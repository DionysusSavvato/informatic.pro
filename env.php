<?php

$dotenv = new \Dotenv\Dotenv(__DIR__);
$dotenv->load();

defined('YII_DEBUG') or define('YII_DEBUG', (bool)trim(getenv('YII_DEBUG')));
defined('YII_ENV') or define('YII_ENV', trim(getenv('YII_ENV')));