Infoman.pro Project
===============================
This is an project of information management software 

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    models/              contains model classes used in both main app and API    
    components/          contains common components    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
app
    assets/              contains application assets such as JavaScript and CSS
    config/              contains application configurations
    controllers/         contains Web controller classes
    models/              contains application-specific model classes
    runtime/             contains files generated during runtime  
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
api
    config/              contains api configurations
    controllers/         contains Web controller classes
    models/              contains api-specific model classes
    runtime/             contains files generated during runtime
vendor/                  contains dependent 3rd-party packages
```
Run next commands for project installing:

1. `./yii migrate --migrationPath=@yii/rbac/migrations`

2. `./yii migrate/up`

3. `./yii install/create-super-administrator`

4. `./yii rbac/init`