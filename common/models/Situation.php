<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\MessageQuery;
use common\queries\SituationQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "situation".
 *
 * @property int       $id
 * @property string    $title
 * @property string    $recomendation
 *
 * @property Message[] $messages
 */
class Situation extends ActiveRecord
{
    /** Несвоевременное информационное обеспечение */
    public const INFORMATION_IS_NOT_TIMELINESS = 1;

    /** Недостаточная полнота информационного обеспечения */
    public const INSTORMAION_IS_NOT_COMPLETENESS = 2;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'situation';
    }

    /**
     * @inheritdoc
     *
     * @return SituationQuery
     *
     * @throws InvalidConfigException
     */
    public static function find(): SituationQuery
    {
        return Yii::createObject(SituationQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['title', 'recomendation'],
                'required',
            ],
            [
                ['recomendation'],
                'string',
            ],
            [
                ['title'],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'recomendation' => 'Recomendation',
        ];
    }

    /**
     * @return MessageQuery|ActiveQuery
     */
    public function getMessages(): MessageQuery
    {
        return $this->hasMany(Message::class, ['situation_id' => 'id']);
    }
}
