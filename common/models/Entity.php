<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\EntityTypeQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\InformationFunctionQuery;
use common\queries\InformationNeedQuery;
use common\queries\MaterialQuery;
use common\queries\NeedQuery;
use Exception;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidValueException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "entity".
 *
 * @property integer             $entity_id
 * @property integer             $entity_type_id
 *
 * @property Consumer            $consumer
 * @property IndustrialFunction  $industrialFunction
 * @property InformationFunction $informationFunction
 * @property InformationNeed     $informationNeed
 * @property InformationProduct  $informationProduct
 * @property InformationResource $informationResource
 * @property Manager             $manager
 * @property Material            $material
 * @property Need                $need
 * @property Parameter[]         $parameters
 * @property Supplier            $supplier
 * @property EntityType          $entityType
 *
 * @property EntityInterface     $instance
 * @property string|null         $typeName
 *
 */
class Entity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'entity';
    }

    /**
     * @inheritdoc
     *
     * @return EntityQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): EntityQuery
    {
        return Yii::createObject(EntityQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['entity_type_id'],
                'required',
            ],
            [
                ['entity_type_id'],
                'integer',
            ],
            [
                ['entity_type_id'],
                'exist',
                'targetClass' => EntityType::class,
                'targetAttribute' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'entity_type_id' => 'Entity Type ID',
        ];
    }

    /**
     * @return EntityInterface
     *
     * @throws Exception
     * @throws InvalidValueException
     */
    public function getInstance(): ?EntityInterface
    {
        switch ($this->entity_type_id) {
            case null:
                return null;
            case EntityType::TYPE_INDUSTRIAL_FUNCTION:
                return $this->industrialFunction;
            case EntityType::TYPE_MATERIAL:
                return $this->material;
            case EntityType::TYPE_CONSUMER:
                return $this->consumer;
            case EntityType::TYPE_SUPPLIER:
                return $this->supplier;
            case EntityType::TYPE_NEED:
                return $this->need;
            case EntityType::TYPE_MANAGER:
                return $this->manager;
            case EntityType::TYPE_INFORMATION_NEED:
                return $this->informationNeed;
            case EntityType::TYPE_INFORMATION_PRODUCT:
                return $this->informationProduct;
            case EntityType::TYPE_INFORMATION_FUNCTION:
                return $this->informationFunction;
            case EntityType::TYPE_INFORMATION_RESOURCE;
                return $this->informationResource;
            default:
                throw new InvalidValueException('Неизвестный тип сущности');
        }
    }

    /**
     * @return ActiveQuery
     */
    public function getConsumer(): ActiveQuery
    {
        return $this->hasOne(Consumer::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getIndustrialFunction(): IndustrialFunctionQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return InformationFunctionQuery|ActiveQuery
     */
    public function getInformationFunction(): InformationFunctionQuery
    {
        return $this->hasOne(InformationFunction::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return InformationNeedQuery|ActiveQuery
     */
    public function getInformationNeed(): InformationNeedQuery
    {
        return $this->hasOne(InformationNeed::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProduct(): ActiveQuery
    {
        return $this->hasOne(InformationProduct::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationResource(): ActiveQuery
    {
        return $this->hasOne(InformationResource::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getManager(): ActiveQuery
    {
        return $this->hasOne(Manager::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getMaterial(): MaterialQuery
    {
        return $this->hasOne(Material::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return NeedQuery|ActiveQuery
     */
    public function getNeed(): NeedQuery
    {
        return $this->hasOne(Need::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSupplier(): ActiveQuery
    {
        return $this->hasOne(Supplier::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return EntityTypeQuery|ActiveQuery
     */
    public function getEntityType(): EntityTypeQuery
    {
        return $this->hasOne(EntityType::class, ['id' => 'entity_type_id']);
    }

    /**
     * Название типа
     *
     * @return null|string
     */
    public function getTypeName(): ?string
    {
        return $this->entityType->name ?? null;
    }
}
