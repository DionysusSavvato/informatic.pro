<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\IndustrialFunctionQuery;
use common\queries\MaterialQuery;
use common\queries\NeedQuery;
use common\queries\ProductQuery;
use common\queries\ResourceQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "product".
 *
 * @property integer             $id
 * @property string              $abbreviation
 * @property integer             $material_id
 * @property integer             $function_id
 *
 * @property IndustrialFunction  $function
 * @property Material            $material
 * @property ProductToNeed[]     $productToNeeds
 * @property Need[]              $needs
 * @property ProductToResource[] $productToResources
 * @property Resource[]          $targetResources
 */
class Product extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'product';
    }

    /**
     * @inheritdoc
     *
     * @return ProductQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): ProductQuery
    {
        return Yii::createObject(ProductQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['material_id', 'function_id', 'abbreviation'],
                'required',
            ],
            [
                ['material_id', 'function_id'],
                'integer',
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['abbreviation'],
                'unique',
            ],
            [
                ['material_id'],
                'unique',
                'targetAttribute' => ['material_id', 'function_id'],
            ],
            [
                ['function_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => IndustrialFunction::class,
                'targetAttribute' => ['function_id' => 'entity_id'],
            ],
            [
                ['material_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Material::class,
                'targetAttribute' => ['material_id' => 'entity_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'abbreviation' => 'Аббревиатура',
            'material_id' => 'Material ID',
            'function_id' => 'Function ID',
        ];
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getFunction(): IndustrialFunctionQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'function_id']);
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getMaterial(): MaterialQuery
    {
        return $this->hasOne(Material::class, ['entity_id' => 'material_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProductToNeeds(): ActiveQuery
    {
        return $this->hasMany(ProductToNeed::class, ['product_id' => 'id']);
    }

    /**
     * @return NeedQuery|ActiveQuery
     */
    public function getNeeds(): NeedQuery
    {
        return $this->hasMany(Need::class, ['entity_id' => 'need_id'])
            ->via('productToNeeds');
    }

    /**
     * @return ActiveQuery
     */
    public function getProductToResources(): ActiveQuery
    {
        return $this->hasMany(ProductToResource::class, ['product_id' => 'id']);
    }

    /**
     * @return ResourceQuery|ActiveQuery
     */
    public function getTargetResources(): ResourceQuery
    {
        return $this->hasMany(Resource::class, ['id' => 'resource_id'])
            ->via('productToResources');
    }

    public function toString(): string
    {
        return "<code class=\"abbreviation-code\">{$this->abbreviation}</code> <code class=\"name-code\">{$this->material->name}</code>";
    }
}
