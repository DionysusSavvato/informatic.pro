<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\EntityQuery;
use common\queries\EntityTypeQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * Типы сущностей
 *
 * @property integer  $id
 * @property string   $name
 *
 * @property Entity[] $entities
 */
class EntityType extends ActiveRecord
{
    /** Функция */
    public const TYPE_INDUSTRIAL_FUNCTION = 1;

    /** Оборотная единица (ресурс или продукт) */
    public const TYPE_MATERIAL = 2;

    /** Потребитель */
    public const TYPE_CONSUMER = 3;

    /** Поставщик */
    public const TYPE_SUPPLIER = 4;

    /** Потребность */
    public const TYPE_NEED = 5;

    /** Ответственное лицо */
    public const TYPE_MANAGER = 6;

    /** Информационная потребность */
    public const TYPE_INFORMATION_NEED = 7;

    /** Информационный продукт */
    public const TYPE_INFORMATION_PRODUCT = 8;

    /** Информационная функции */
    public const TYPE_INFORMATION_FUNCTION = 9;

    /** Информацонный ресурс */
    public const TYPE_INFORMATION_RESOURCE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'entity_type';
    }

    /**
     * @inheritdoc
     *
     * @return EntityTypeQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): EntityTypeQuery
    {
        return Yii::createObject(EntityTypeQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name'],
                'required',
            ],
            [
                ['name'],
                'string',
            ],
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntities(): EntityQuery
    {
        return $this->hasMany(Entity::class, ['entity_type_id' => 'id']);
    }
}