<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\MaterialQuery;
use common\queries\ProductQuery;
use common\queries\ResourceQuery;
use common\traits\EntityRelationTrait;
use common\traits\EntityTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "industrial_function".
 *
 * @property integer              $entity_id
 * @property string               $name
 * @property string               $abbreviation
 * @property integer              $parent_id
 * @property integer              $manager_id
 *
 * @property Entity               $entity
 * @property IndustrialFunction   $parent
 * @property IndustrialFunction[] $subFunctions
 * @property Product[]            $products
 * @property Material[]           $productMaterials
 * @property Resource[]           $resources
 * @property Material[]           $resourceMaterials
 * @property Parameter[]          $parameters
 * @property Manager              $manager
 * @property IndustrialExpense[]  $industrialExpenses
 * @property IndustrialEffect[]   $industrialEffects
 * @property ProductToNeed[]      $productsToNeeds
 * @property IndustrialFunction[] $dependentIndustrialFunctions
 * @property ProductToResource[]  $productToResourceViaProduct
 */
class IndustrialFunction extends ActiveRecord implements EntityInterface
{
    use EntityTrait;
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_INDUSTRIAL_FUNCTION;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'industrial_function';
    }

    /**
     * @inheritdoc
     *
     * @return IndustrialFunctionQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): IndustrialFunctionQuery
    {
        return Yii::createObject(IndustrialFunctionQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     *
     * @return array
     *
     * @throws InvalidConfigException
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'abbreviation'],
                'required',
            ],
            [
                // Указывать родительский процесс обязательно, если уже есть корневой процесс
                ['parent_id'],
                'required',
                'when' => function () {
                    return self::find()->root()->exists();
                },
            ],
            [
                ['parent_id', 'manager_id'],
                'integer',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['name', 'abbreviation'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => IndustrialFunction::class,
                'targetAttribute' => ['parent_id' => 'entity_id'],
            ],
            [
                ['manager_id'],
                'exist',
                'skipOnEmpty' => true,
                'targetClass' => Manager::class,
                'targetAttribute' => ['manager_id' => 'entity_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
            'parent_id' => 'Вышестоящий процесс',
            'manager_id' => 'Менеджер',
            'abbreviation' => 'Аббревиатура',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getParent(): IndustrialFunctionQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'parent_id']);
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getSubFunctions(): IndustrialFunctionQuery
    {
        return $this->hasMany(IndustrialFunction::class, ['parent_id' => 'entity_id']);
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public function getProducts(): ProductQuery
    {
        return $this->hasMany(Product::class, ['function_id' => 'entity_id']);
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getProductMaterials(): MaterialQuery
    {
        return $this->hasMany(Material::class, ['entity_id' => 'material_id'])
            ->via('products');
    }

    /**
     * @return ActiveQuery
     */
    public function getProductsToNeeds(): ActiveQuery
    {
        return $this->hasMany(ProductToNeed::class, ['product_id' => 'id'])
            ->via('products');
    }

    /**
     * @return ActiveQuery
     */
    public function getIndustrialEffects(): ActiveQuery
    {
        return $this->hasMany(IndustrialEffect::class, ['product_to_need_id' => 'id'])
            ->via('productsToNeed');
    }

    /**
     * @return ResourceQuery|ActiveQuery
     */
    public function getResources(): ResourceQuery
    {
        return $this->hasMany(Resource::class, ['function_id' => 'entity_id']);
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getResourceMaterials(): MaterialQuery
    {
        return $this->hasMany(Material::class, ['entity_id' => 'material_id'])
            ->via('resources');
    }

    /**
     * @return ActiveQuery
     */
    public function getIndustrialExpenses(): ActiveQuery
    {
        return $this->hasMany(IndustrialExpense::class, ['resource_id' => 'id'])
            ->via('resources');
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * @return ActiveQuery
     */
    public function getManager(): ActiveQuery
    {
        return $this->hasOne(Manager::class, ['entity_id' => 'manager_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProductToResourceViaProduct(): ActiveQuery
    {
        return $this->hasMany(ProductToResource::class, ['product_id' => 'id'])
            ->via('products');
    }

    /**
     * @return ResourceQuery|ActiveQuery
     */
    public function getResourcesOfDependentFunctions(): ResourceQuery
    {
        return $this->hasMany(Resource::class, ['id' => 'resource_id'])
            ->via('productToResourceViaProduct');
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getDependentIndustrialFunctions(): IndustrialFunctionQuery
    {
        return $this->hasMany(IndustrialFunction::class, ['entity_id' => 'function_id'])
            ->via('resourcesOfDependentFunctions');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
