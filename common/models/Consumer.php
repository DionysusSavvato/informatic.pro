<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\ConsumerQuery;
use common\queries\EntityQuery;
use common\queries\NeedQuery;
use common\traits\EntityRelationTrait;
use common\traits\EntityTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "consumer".
 *
 * @property integer     $entity_id
 * @property string      $name
 * @property string      $abbreviation
 *
 * @property Entity      $entity
 * @property Need[]      $needs
 * @property Parameter[] $parameters
 */
class Consumer extends ActiveRecord implements EntityInterface
{
    use EntityTrait;
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_CONSUMER;

    /**
     * @inheritdoc
     *
     * @return ConsumerQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): ConsumerQuery
    {
        return Yii::createObject(ConsumerQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'consumer';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'abbreviation'],
                'required',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['name', 'abbreviation'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
            'abbreviation' => 'Аббревиатура',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return NeedQuery|ActiveQuery
     */
    public function getNeeds(): NeedQuery
    {
        return $this->hasMany(Need::class, ['consumer_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
