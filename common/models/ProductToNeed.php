<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\IndustrialEffectQuery;
use common\queries\NeedQuery;
use common\queries\ProductQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "product_to_need".
 *
 * @property integer            $id
 * @property integer            $product_id
 * @property integer            $need_id
 * @property string             $effect_name
 *
 * @property Need               $need
 * @property Product            $product
 * @property IndustrialEffect[] $industrialEffects
 */
class ProductToNeed extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'product_to_need';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['product_id', 'need_id'],
                'required',
            ],
            [
                ['product_id', 'need_id'],
                'integer',
            ],
            [
                ['product_id', 'need_id'],
                'unique',
                'targetAttribute' => ['product_id', 'need_id'],
            ],
            [
                ['effect_name'],
                'string',
            ],
            [
                ['need_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Need::class,
                'targetAttribute' => ['need_id' => 'entity_id'],
            ],
            [
                ['product_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Product::class,
                'targetAttribute' => ['product_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'need_id' => 'Need ID',
            'product_id' => 'Продукт',
        ];
    }

    /**
     * @return NeedQuery|ActiveQuery
     */
    public function getNeed(): NeedQuery
    {
        return $this->hasOne(Need::class, ['entity_id' => 'need_id']);
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public function getProduct(): ProductQuery
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return IndustrialEffectQuery|ActiveQuery
     */
    public function getIndustrialEffects(): IndustrialEffectQuery
    {
        return $this->hasMany(IndustrialEffect::class, ['product_to_need_id' => 'id']);
    }
}
