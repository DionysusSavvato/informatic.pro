<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\MaterialQuery;
use common\queries\ResourceQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "resource".
 *
 * @property integer             $id
 * @property string              $abbreviation
 * @property integer             $material_id
 * @property integer             $function_id
 *
 * @property Material            $material
 * @property IndustrialFunction  $function
 * @property IndustrialExpense[] $industrialExpenses
 * @property ProductToResource[] $sourceProducts
 */
class Resource extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'resource';
    }

    /**
     * @inheritdoc
     *
     * @return ResourceQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): ResourceQuery
    {
        return Yii::createObject(ResourceQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['material_id', 'function_id', 'abbreviation'],
                'required',
            ],
            [
                ['material_id', 'function_id'],
                'integer',
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['abbreviation'],
                'unique',
            ],
            [
                ['material_id'],
                'unique',
                'targetAttribute' => ['material_id', 'function_id'],
            ],
            [
                ['function_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => IndustrialFunction::class,
                'targetAttribute' => ['function_id' => 'entity_id'],
            ],
            [
                ['material_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Material::class,
                'targetAttribute' => ['material_id' => 'entity_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'abbreviation' => 'Аббревиатура',
            'material_id' => 'Материал',
            'function_id' => 'Производственная функция',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getFunction(): ActiveQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'function_id']);
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getMaterial(): MaterialQuery
    {
        return $this->hasOne(Material::class, ['entity_id' => 'material_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSourceProducts(): ActiveQuery
    {
        return $this->hasMany(ProductToResource::class, ['resource_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getIndustrialExpenses(): ActiveQuery
    {
        return $this->hasMany(IndustrialExpense::class, ['resource_id' => 'id']);
    }

    public function toString(): string
    {
        return "<code class=\"abbreviation-code\">{$this->abbreviation}</code> <code class=\"name-code\">{$this->material->name}</code>";
    }
}
