<?php

namespace common\models;

use common\behaviors\ScaleTypeBehavior;
use common\components\ActiveRecord;
use common\queries\ScaleQuery;
use common\queries\ScaleValueQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scale".
 *
 * @property integer      $scale_id
 * @property integer      $type_id
 * @property string       $name
 *
 * @property Parameter[]  $parameters
 * @property ScaleType    $scaleType
 * @property ScaleValue[] $scaleValues
 * @property ScaleValue[] $sortedScaleValues
 */
class Scale extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'scale';
    }

    /**
     * @inheritdoc
     *
     * @return ScaleQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): ScaleQuery
    {
        return Yii::createObject(ScaleQuery::class, [static::class]);
    }


    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'type_id'],
                'required',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['name'],
                'unique',
            ],
            [
                ['type_id'],
                'integer',
            ],
            [
                ['name'],
                'unique',
            ],
            [
                ['type_id'],
                'exist',
                'targetClass' => ScaleType::class,
                'targetAttribute' => ['type_id' => 'scale_type_id'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'type' => [
                'class' => ScaleTypeBehavior::class,
            ],
        ]);
    }

    /**
     * @return array
     */
    public function extraFields(): array
    {
        return [
            'values' => 'sortedScaleValues',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'scale_id' => 'ID шкалы',
            'type_id' => 'ID типа шкалы',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['scale_id' => 'scale_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getScaleType(): ActiveQuery
    {
        return $this->hasOne(ScaleType::class, ['scale_type_id' => 'type_id']);
    }

    /**
     * @return ScaleValueQuery
     */
    public function getSortedScaleValues(): ScaleValueQuery
    {
        return $this->getScaleValues()->sortByRank();
    }

    /**
     * @return ScaleValueQuery|ActiveQuery
     */
    public function getScaleValues(): ScaleValueQuery
    {
        return $this->hasMany(ScaleValue::class, ['scale_id' => 'scale_id']);
    }

    /**
     * Должна ли шкала иметь фиксированный набор значений
     *
     * @return bool
     *
     * @throws InvalidParamException
     */
    public function hasFixedValues(): bool
    {
        return ArrayHelper::isIn($this->type_id, [
            ScaleType::TYPE_CLASS,
            ScaleType::TYPE_RANK,
        ]);
    }
}
