<?php

namespace common\models;

use Carbon\Carbon;
use common\components\ActiveRecord;
use common\queries\IndustrialExpenseQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\InnerIndustrialEffectQuery;
use common\queries\ProductQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "inner_industrial_effect".
 *
 * @property integer             $id
 * @property integer             $product_to_resource_id
 * @property double              $value
 * @property string              $date
 *
 * @property ProductToResource   $productToResource
 * @property Product             $product
 * @property IndustrialFunction  $sourceFunction
 * @property Resource[]          $resourcesOfSourceFunction
 * @property IndustrialExpense[] $industrialExpensesOfSourceFunction
 *
 * @property Carbon|null         $carbonDate
 */
class InnerIndustrialEffect extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'inner_industrial_effect';
    }

    /**
     * @inheritdoc
     *
     * @return InnerIndustrialEffectQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): InnerIndustrialEffectQuery
    {
        return Yii::createObject(InnerIndustrialEffectQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['product_to_resource_id', 'value'],
                'required',
            ],
            [
                ['product_to_resource_id'],
                'integer',
            ],
            [
                ['value'],
                'number',
            ],
            [
                ['date'],
                'safe',
            ],
            [
                ['product_to_resource_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ProductToResource::class,
                'targetAttribute' => ['product_to_resource_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'product_to_resource_id' => 'Product To Resource ID',
            'value' => 'Значение',
            'date' => 'Дата',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProductToResource(): ActiveQuery
    {
        return $this->hasOne(ProductToResource::class, ['id' => 'product_to_resource_id']);
    }

    /**
     * @return Carbon|null
     */
    public function getCarbonDate(): ?Carbon
    {
        return $this->date ? Carbon::parse($this->date) : null;
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public function getProduct(): ProductQuery
    {
        return $this->hasOne(Product::class, ['id' => 'product_id'])->via('productToResource');
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getSourceFunction(): IndustrialFunctionQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'function_id'])->via('product');
    }

    /**
     * @return ActiveQuery
     */
    public function getResourcesOfSourceFunction(): ActiveQuery
    {
        return $this->hasMany(Resource::class, ['function_id' => 'entity_id'])->via('sourceFunction');
    }

    /**
     * @return IndustrialExpenseQuery|ActiveQuery
     */
    public function getIndustrialExpensesOfSourceFunction(): IndustrialExpenseQuery
    {
        return $this->hasMany(IndustrialExpense::class, ['resource_id' => 'id'])->via('resourcesOfSourceFunction');
    }
}
