<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\ConsumerQuery;
use common\queries\EntityQuery;
use common\queries\MaterialQuery;
use common\queries\NeedQuery;
use common\queries\ProductQuery;
use common\traits\EntityRelationTrait;
use common\traits\EntityTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "need".
 *
 * @property integer         $entity_id
 * @property string          $name
 * @property string          $abbreviation
 * @property integer         $consumer_id
 *
 * @property Consumer        $consumer
 * @property Entity          $entity
 * @property ProductToNeed[] $productsToNeeds
 * @property Material[]      $products
 * @property Parameter[]     $paramaters
 */
class Need extends ActiveRecord implements EntityInterface
{
    use EntityTrait;
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_NEED;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'need';
    }

    /**
     * @inheritdoc
     *
     * @return NeedQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): NeedQuery
    {
        return Yii::createObject(NeedQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'consumer_id', 'abbreviation'],
                'required',
            ],
            [
                ['consumer_id'],
                'integer',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['abbreviation'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
            [
                ['consumer_id'],
                'exist',
                'targetClass' => Consumer::class,
                'targetAttribute' => 'entity_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
            'abbreviation' => 'Аббревиатура',
            'consumer_id' => 'ID потребителя',
        ];
    }

    /**
     * @return ConsumerQuery|ActiveQuery
     */
    public function getConsumer(): ConsumerQuery
    {
        return $this->hasOne(Consumer::class, ['entity_id' => 'consumer_id']);
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProductsToNeeds(): ActiveQuery
    {
        return $this->hasMany(ProductToNeed::class, ['need_id' => 'entity_id']);
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public function getProducts(): ProductQuery
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])
            ->via('productsToNeeds');
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getMaterials(): MaterialQuery
    {
        return $this->hasMany(Material::class, ['entity_id' => 'material_id'])
            ->via('products');
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
