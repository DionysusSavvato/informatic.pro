<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\MaterialQuery;
use common\traits\EntityRelationTrait;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "supplier".
 *
 * @property integer              $entity_id
 * @property string               $name
 * @property string               $abbreviation
 *
 * @property Entity               $entity
 * @property SupplierToMaterial[] $supplierToMaterials
 * @property Material[]           $materials
 * @property Parameter[]          $parameters
 */
class Supplier extends ActiveRecord implements EntityInterface
{
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_SUPPLIER;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'abbreviation'],
                'required',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['name', 'abbreviation'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => EntityType::TYPE_SUPPLIER,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSupplierToMaterials(): ActiveQuery
    {
        return $this->hasMany(SupplierToMaterial::class, ['supplier_id' => 'entity_id']);
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getMaterials(): MaterialQuery
    {
        return $this->hasMany(Material::class, ['entity_id' => 'material_id'])
            ->via('supplierToMaterials');
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
