<?php

namespace common\models;

use Carbon\Carbon;
use common\components\ActiveRecord;
use common\queries\InformationExpenseQuery;
use Yii;
use yii\base\InvalidConfigException;

/**
 * This is the model class for table "information_expense".
 *
 * @property integer     $id
 * @property double      $sum
 * @property string      $date
 *
 * @property Carbon|null $carbonDate
 */
class InformationExpense extends ActiveRecord
{
    /**
     * @inheritdoc
     *
     * @return InformationExpenseQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): InformationExpenseQuery
    {
        return Yii::createObject(InformationExpenseQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_expense';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['sum', 'date'],
                'required',
            ],
            [
                ['sum'],
                'number',
            ],
            [
                ['date'],
                'date',
                'format' => 'php:Y-m-d',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'sum' => 'Сумма',
            'date' => 'Дата',
        ];
    }

    /**
     * @return Carbon|null
     */
    public function getCarbonDate(): ?Carbon
    {
        return $this->date ? Carbon::parse($this->date) : null;
    }
}
