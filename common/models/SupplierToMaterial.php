<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\MaterialQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "supplier_to_material".
 *
 * @property integer  $supplier_id
 * @property integer  $material_id
 *
 * @property Material $material
 * @property Supplier $supplier
 */
class SupplierToMaterial extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'supplier_to_material';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['supplier_id', 'material_id'],
                'required',
            ],
            [
                ['supplier_id', 'material_id'],
                'integer',
            ],
            [
                ['material_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Material::class,
                'targetAttribute' => ['material_id' => 'entity_id'],
            ],
            [
                ['supplier_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Supplier::class,
                'targetAttribute' => ['supplier_id' => 'entity_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'supplier_id' => 'Supplier ID',
            'material_id' => 'Material ID',
        ];
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getMaterial(): MaterialQuery
    {
        return $this->hasOne(Material::class, ['entity_id' => 'material_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSupplier(): ActiveQuery
    {
        return $this->hasOne(Supplier::class, ['entity_id' => 'supplier_id']);
    }
}
