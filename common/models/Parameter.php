<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\EntityQuery;
use common\queries\ParameterValueQuery;
use common\queries\ScaleQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "parameter".
 *
 * @property integer          $parameter_id
 * @property string           $name
 * @property integer          $scale_id
 * @property integer          $entity_id
 *
 * @property Entity           $entity
 * @property Scale            $scale
 * @property ParameterValue[] $parameterValues
 */
class Parameter extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'parameter';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'scale_id', 'entity_id'],
                'required',
            ],
            [
                ['scale_id', 'entity_id'],
                'integer',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['name'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
            ],
            [
                ['scale_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Scale::class,
                'targetAttribute' => ['scale_id' => 'scale_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'parameter_id' => 'Parameter ID',
            'name' => 'Название',
            'scale_id' => 'ID шкалы',
            'entity_id' => 'Entity ID',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ScaleQuery|ActiveQuery
     */
    public function getScale(): ScaleQuery
    {
        return $this->hasOne(Scale::class, ['scale_id' => 'scale_id']);
    }

    /**
     * @return ParameterValueQuery|ActiveQuery
     */
    public function getParameterValues(): ParameterValueQuery
    {
        return $this->hasMany(ParameterValue::class, ['param_id' => 'parameter_id']);
    }
}
