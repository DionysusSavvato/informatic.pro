<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\InformationFunctionQuery;
use common\traits\EntityRelationTrait;
use common\traits\EntityTrait;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "information_resource".
 *
 * @property integer             $entity_id
 * @property string              $name
 * @property string              $abbreviation
 * @property integer             $function_id
 *
 * @property Entity              $entity
 * @property InformationFunction $informationFunction
 * @property Parameter[]         $parameters
 */
class InformationResource extends ActiveRecord implements EntityInterface
{
    use EntityTrait;
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_INFORMATION_RESOURCE;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_resource';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'function_id', 'abbreviation'],
                'required',
            ],
            [
                ['function_id'],
                'integer',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['name', 'abbreviation'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
            [
                ['function_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InformationFunction::class,
                'targetAttribute' => ['function_id' => 'entity_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
            'abbreviation' => 'Аббревиатура',
            'function_id' => 'Function ID',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return InformationFunctionQuery|ActiveQuery
     */
    public function getInformationFunction(): InformationFunctionQuery
    {
        return $this->hasOne(InformationFunction::class, ['entity_id' => 'function_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
