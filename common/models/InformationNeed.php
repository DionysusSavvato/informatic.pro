<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\InformationNeedQuery;
use common\traits\EntityRelationTrait;
use common\traits\EntityTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "information_need".
 *
 * @property integer                               $entity_id
 * @property string                                $name
 * @property string                                $abbreviation
 * @property integer                               $manager_id
 *
 * @property Entity                                $entity
 * @property Manager                               $manager
 * @property InformationProductToInformationNeed[] $informationProductsToInformationNeeds
 * @property InformationProduct[]                  $informationProducts
 * @property Parameter[]                           $parameters
 */
class InformationNeed extends ActiveRecord implements EntityInterface
{
    use EntityTrait;
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_INFORMATION_NEED;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_need';
    }

    /**
     * @inheritdoc
     *
     * @return InformationNeedQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): InformationNeedQuery
    {
        return Yii::createObject(InformationNeedQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'manager_id', 'abbreviation'],
                'required',
            ],
            [
                ['name', 'abbreviation'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['abbreviation'],
                'unique',
            ],
            [
                ['manager_id'],
                'exist',
                'targetClass' => Manager::class,
                'targetAttribute' => 'entity_id',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
            'abbreviation' => 'Аббревиатура',
            'manager_id' => 'Manager ID',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getManager(): ActiveQuery
    {
        return $this->hasOne(Manager::class, ['entity_id' => 'manager_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProductsToInformationNeeds(): ActiveQuery
    {
        return $this->hasMany(InformationProductToInformationNeed::class,
            ['information_need_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProducts(): ActiveQuery
    {
        return $this->hasMany(InformationProduct::class,
            ['entity_id' => 'information_product_id'])->via('informationProductsToInformationNeeds');
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
