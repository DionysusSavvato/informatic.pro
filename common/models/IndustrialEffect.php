<?php

namespace common\models;

use Carbon\Carbon;
use common\components\ActiveRecord;
use common\queries\IndustrialEffectQuery;
use common\queries\IndustrialExpenseQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\ProductQuery;
use common\queries\ResourceQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "industrial_effect".
 *
 * @property integer            $id
 * @property integer            $product_to_need_id
 * @property double             $value
 * @property string             $date
 *
 * @property ProductToNeed      $productToNeed
 * @property Product            $product
 * @property IndustrialFunction $industrialFunction
 *
 * @property Carbon|null        $carbonDate
 */
class IndustrialEffect extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'industrial_effect';
    }

    /**
     * @inheritdoc
     *
     * @return IndustrialEffectQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): IndustrialEffectQuery
    {
        return Yii::createObject(IndustrialEffectQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['product_to_need_id', 'value', 'date'],
                'required',
            ],
            [
                ['product_to_need_id'],
                'integer',
            ],
            [
                ['value'],
                'number',
                'min' => 0,
                'max' => 1,
            ],
            [
                ['date'],
                'safe',
            ],
            [
                ['product_to_need_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ProductToNeed::class,
                'targetAttribute' => ['product_to_need_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'product_to_need_id' => 'Product To Need ID',
            'value' => 'Значение',
            'date' => 'Дата',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProductToNeed(): ActiveQuery
    {
        return $this->hasOne(ProductToNeed::class, ['id' => 'product_to_need_id']);
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public function getProduct(): ProductQuery
    {
        return $this->hasOne(Product::class, ['id' => 'product_id'])
            ->via('productToNeed');
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getIndustrialFunction(): IndustrialFunctionQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'function_id'])
            ->via('product');
    }

    /**
     * @return ResourceQuery|ActiveQuery
     */
    public function getIndustrialResources(): ResourceQuery
    {
        return $this->hasMany(Resource::class, ['function_id' => 'entity_id'])
            ->via('industrialFunction');
    }

    /**
     * @return IndustrialExpenseQuery|ActiveQuery
     */
    public function getIndustrialExpenses(): IndustrialExpenseQuery
    {
        return $this->hasMany(IndustrialExpense::class, ['resource_id' => 'id'])->via('industrialResources');
    }

    /**
     * @return Carbon|null
     */
    public function getCarbonDate(): ?Carbon
    {
        return $this->date ? Carbon::parse($this->date) : null;
    }
}
