<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\InformationEffectQuery;
use common\queries\InformationNeedQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "information_product_to_information_need".
 *
 * @property integer             $id
 * @property integer             $information_product_id
 * @property integer             $information_need_id
 * @property float               $effect_threshold_value
 *
 * @property InformationNeed     $informationNeed
 * @property InformationProduct  $informationProduct
 * @property InformationEffect[] $informationEffects
 */
class InformationProductToInformationNeed extends ActiveRecord
{
    /** Значение порогового значения по умолчанию */
    public const DEFAULT_THRESHOLD_VALUE = 0.5;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_product_to_information_need';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['effect_threshold_value'],
                'default',
                'value' => self::DEFAULT_THRESHOLD_VALUE,
            ],
            [
                ['information_product_id', 'information_need_id', 'effect_threshold_value'],
                'required',
            ],
            [
                ['information_product_id', 'information_need_id'],
                'integer',
            ],
            [
                ['effect_threshold_value'],
                'double',
                'min' => 0,
                'max' => 1,
            ],
            [
                ['information_need_id'],
                'unique',
                'targetAttribute' => ['information_product_id', 'information_need_id'],
            ],
            [
                ['information_need_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InformationNeed::class,
                'targetAttribute' => ['information_need_id' => 'entity_id'],
            ],
            [
                ['information_product_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InformationProduct::class,
                'targetAttribute' => ['information_product_id' => 'entity_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'information_product_id' => 'Информационный продукт',
            'information_need_id' => 'Информационная потребность',
            'effect_threshold_value' => 'Пороговое значение при оценке эффекта',
        ];
    }

    /**
     * @return InformationNeedQuery|ActiveQuery
     */
    public function getInformationNeed(): InformationNeedQuery
    {
        return $this->hasOne(InformationNeed::class, ['entity_id' => 'information_need_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProduct(): ActiveQuery
    {
        return $this->hasOne(InformationProduct::class, ['entity_id' => 'information_product_id']);
    }

    /**
     * @return InformationEffectQuery|ActiveQuery
     */
    public function getInformationEffects(): InformationEffectQuery
    {
        return $this->hasMany(InformationEffect::class, ['information_product_to_information_need_id' => 'id']);
    }
}
