<?php

namespace common\models;

use Carbon\Carbon;
use common\components\ActiveRecord;
use common\queries\InformationChangeQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\AttributeTypecastBehavior;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "information_change".
 *
 * @property integer                         $id
 * @property string                          $title
 * @property string                          $description
 * @property string                          $date
 * @property boolean                         $flag_model_changes_needed
 *
 * @property InformationChangeLocalization[] $informationChangeLocalizations
 *
 * @property Carbon|null                     $carbonDate
 */
class InformationChange extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_change';
    }

    /**
     * @inheritdoc
     *
     * @return InformationChangeQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): InformationChangeQuery
    {
        return Yii::createObject(InformationChangeQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['date'],
                'default',
                'value' => Carbon::now()->toDateTimeString(),
            ],
            [
                ['flag_model_changes_needed'],
                'default',
                'value' => false,
            ],
            [
                ['title', 'date'],
                'required',
            ],
            [
                ['description'],
                'string',
            ],
            [
                ['date'],
                'date',
                'format' => 'php:Y-m-d',
            ],
            [
                ['title'],
                'string',
                'max' => 255,
            ],
            [
                ['flag_model_changes_needed'],
                'boolean',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
            'date' => 'Дата',
            'flag_model_changes_needed' => 'Требуются изменения в модели?',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationChangeLocalizations(): ActiveQuery
    {
        return $this->hasMany(InformationChangeLocalization::class, ['information_change_id' => 'id']);
    }

    /**
     * @return Carbon|null
     */
    public function getCarbonDate(): ?Carbon
    {
        return $this->date ? Carbon::parse($this->date) : null;
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'typecast' => [
                'class' => AttributeTypecastBehavior::class,
                'typecastAfterFind' => true,
                'typecastAfterValidate' => true,
            ],
        ]);
    }
}
