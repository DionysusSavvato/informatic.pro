<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\InformationEffectQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "information_effect_type".
 *
 * @property int                 $id
 * @property string              $name
 *
 * @property InformationEffect[] $informationEffects
 */
class InformationEffectType extends ActiveRecord
{
    /** Своевременность */
    public const TIMELINESS = 1;

    /** Полнота */
    public const COMPLETENESS = 2;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_effect_type';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return InformationEffectQuery|ActiveQuery
     */
    public function getInformationEffects(): InformationEffectQuery
    {
        return $this->hasMany(InformationEffect::class, ['information_effect_type_id' => 'id']);
    }
}
