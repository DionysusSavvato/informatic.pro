<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\ScaleQuery;
use common\queries\ScaleValueQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "scale_value".
 *
 * @property integer   $scale_value_id
 * @property integer   $scale_id
 * @property string    $name
 * @property integer   $rank
 *
 * @property Parameter $parameter
 */
class ScaleValue extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'scale_value';
    }

    /**
     * @inheritdoc
     *
     * @return ScaleValueQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): ScaleValueQuery
    {
        return Yii::createObject(ScaleValueQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['scale_id', 'name'], 'required'],
            [['scale_id', 'rank'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [
                ['scale_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Scale::class,
                'targetAttribute' => ['scale_id' => 'scale_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'scale_value_id' => 'Scale Value ID',
            'scale_id' => 'Scale ID',
            'name' => 'Name',
            'rank' => 'Rank',
        ];
    }

    /**
     * @return ScaleQuery|ActiveQuery
     */
    public function getScale(): ScaleQuery
    {
        return $this->hasOne(Scale::class, ['scale_id' => 'scale_id']);
    }
}
