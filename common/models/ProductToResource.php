<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\InnerIndustrialEffectQuery;
use common\queries\ProductQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "product_to_resource".
 *
 * @property integer                 $id
 * @property integer                 $product_id
 * @property integer                 $resource_id
 * @property string                  $effect_name
 *
 * @property InnerIndustrialEffect[] $innerIndustrialEffects
 * @property Product                 $product
 * @property Resource                $resource
 */
class ProductToResource extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'product_to_resource';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['product_id', 'resource_id'],
                'required',
            ],
            [
                ['product_id', 'resource_id'],
                'integer',
            ],
            [
                ['effect_name'],
                'string',
            ],
            [
                ['product_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Product::class,
                'targetAttribute' => ['product_id' => 'id'],
            ],
            [
                ['resource_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Resource::class,
                'targetAttribute' => ['resource_id' => 'id'],
            ],
            [
                ['product_id'],
                'unique',
                'targetAttribute' => ['product_id', 'resource_id'],
            ],
            [
                ['product_id'],
                function (string $attribute) {
                    if ($this->product->material_id !== $this->resource->material_id) {
                        $this->addError($attribute, 'Ошибка связи');
                    }
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'resource_id' => 'Resource ID',
        ];
    }

    /**
     * @return InnerIndustrialEffectQuery|ActiveQuery
     */
    public function getInnerIndustrialEffects(): InnerIndustrialEffectQuery
    {
        return $this->hasMany(InnerIndustrialEffect::class, ['product_to_resource_id' => 'id']);
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public function getProduct(): ProductQuery
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getResource(): ActiveQuery
    {
        return $this->hasOne(Resource::class, ['id' => 'resource_id']);
    }
}
