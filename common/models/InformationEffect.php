<?php

namespace common\models;

use Carbon\Carbon;
use common\components\ActiveRecord;
use common\queries\InformationEffectQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\AttributeTypecastBehavior;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "information_effect".
 *
 * @property integer                             $id
 * @property integer                             $information_product_to_information_need_id
 * @property integer                             $information_effect_type_id
 * @property double                              $value
 * @property string                              $date
 *
 * @property InformationProductToInformationNeed $informationProductToInformationNeed
 * @property InformationProduct                  $informationProduct
 * @property InformationEffectType               $informationEffectType
 *
 * @property Carbon|null                         $carbonDate
 * @property InformationExpense                  $informationExpense|null
 */
class InformationEffect extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_effect';
    }

    /**
     * @inheritdoc
     *
     * @return InformationEffectQuery|ActiveQuery
     *
     * @throws InvalidConfigException
     */
    public static function find(): InformationEffectQuery
    {
        return Yii::createObject(InformationEffectQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['information_product_to_information_need_id', 'value', 'information_effect_type_id'],
                'required',
            ],
            [
                ['information_product_to_information_need_id', 'information_effect_type_id'],
                'integer',
            ],
            [
                ['information_effect_type_id'],
                'exist',
                'targetClass' => InformationEffectType::class,
                'targetAttribute' => 'id',
            ],
            [
                ['value'],
                'number',
            ],
            [
                ['value'],
                'filter',
                'filter' => function ($value) {
                    return round($value);
                },
                'when' => function (self $model) {
                    return $model->information_effect_type_id == InformationEffectType::TIMELINESS;
                },
            ],
            [
                ['date'],
                'safe',
            ],
            [
                ['information_product_to_information_need_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InformationProductToInformationNeed::class,
                'targetAttribute' => ['information_product_to_information_need_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'information_product_to_information_need_id' => 'Information Product To Information Need ID',
            'value' => 'Значение',
            'date' => 'Дата',
        ];
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'typecast' => [
                'class' => AttributeTypecastBehavior::class,
                'attributeTypes' => [
                    'id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'information_product_to_information_need_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'information_effect_type_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                    'value' => AttributeTypecastBehavior::TYPE_FLOAT,
                ],
                'typecastAfterFind' => true,
                'typecastAfterValidate' => true,
            ],
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProductToInformationNeed(): ActiveQuery
    {
        return $this->hasOne(InformationProductToInformationNeed::class,
            ['id' => 'information_product_to_information_need_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProduct(): ActiveQuery
    {
        return $this->hasOne(InformationProduct::class, ['entity_id' => 'information_product_id'])
            ->via('informationProductToInformationNeed');
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationNeed(): ActiveQuery
    {
        return $this->hasOne(InformationNeed::class, ['entity_id' => 'information_need_id'])
            ->via('informationProductToInformationNeed');
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationEffectType(): ActiveQuery
    {
        return $this->hasOne(InformationEffectType::class, ['id' => 'information_effect_type_id']);
    }

    /**
     * @return InformationExpense
     *
     * @throws InvalidConfigException
     */
    public function getInformationExpense(): ?InformationExpense
    {
        return InformationExpense::find()
            ->where([
                '<=',
                'date',
                $this->carbonDate->endOfDay()->toDateTimeString(),
            ])
            ->sortByDateDesc()
            ->limit(1)
            ->one();
    }

    /**
     * @return Carbon|null
     */
    public function getCarbonDate(): ?Carbon
    {
        return $this->date ? Carbon::parse($this->date) : null;
    }
}
