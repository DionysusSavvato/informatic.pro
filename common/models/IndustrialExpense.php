<?php

namespace common\models;

use Carbon\Carbon;
use common\components\ActiveRecord;
use common\queries\IndustrialExpenseQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\MaterialQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "industrial_expense".
 *
 * @property integer            $id
 * @property integer            $resource_id
 * @property double             $price
 * @property double             $count
 * @property string             $date
 *
 * @property Resource           $resource
 * @property Material           $material
 * @property IndustrialFunction $industrialFunction
 *
 * @property Carbon|null        $carbonDate
 * @property float              $sum
 */
class IndustrialExpense extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'industrial_expense';
    }

    /**
     * @inheritdoc
     *
     * @return IndustrialFunctionQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): IndustrialExpenseQuery
    {
        return Yii::createObject(IndustrialExpenseQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['resource_id', 'price', 'count'],
                'required',
            ],
            [
                ['resource_id'],
                'integer',
            ],
            [
                ['price', 'count'],
                'number',
            ],
            [
                ['date'],
                'safe',
            ],
            [
                ['resource_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Resource::class,
                'targetAttribute' => ['resource_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'resource_id' => 'Resource ID',
            'price' => 'Цена',
            'count' => 'Количество',
            'date' => 'Дата',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getResource(): ActiveQuery
    {
        return $this->hasOne(Resource::class, ['id' => 'resource_id']);
    }

    /**
     * @return MaterialQuery|ActiveQuery
     */
    public function getMaterial(): MaterialQuery
    {
        return $this->hasOne(Material::class, ['entity_id' => 'material_id'])
            ->via('resource');
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getIndustrialFunction(): IndustrialFunctionQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'function_id'])
            ->via('resource');
    }

    /**
     * @return Carbon|null
     */
    public function getCarbonDate(): ?Carbon
    {
        return $this->date ? Carbon::parse($this->date) : null;
    }

    /**
     * @return float
     */
    public function getSum(): float
    {
        return $this->count * $this->price;
    }
}
