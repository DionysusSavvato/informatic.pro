<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\ScaleQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "scale_type".
 *
 * @property integer $scale_type_id
 * @property string  $name
 *
 * @property Scale[] $scales
 */
class ScaleType extends ActiveRecord
{
    /** Классификационная шкала */
    public const TYPE_CLASS = 1;

    /** Ранговая шкала */
    public const TYPE_RANK = 2;

    /** Относительная шкала */
    public const TYPE_RELATIVE = 3;

    /** Абсолютная шкала */
    public const TYPE_ABSOLUTE = 4;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'scale_type';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'scale_type_id' => 'Scale Type ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return ScaleQuery|ActiveQuery
     */
    public function getScales(): ScaleQuery
    {
        return $this->hasMany(Scale::class, ['type_id' => 'scale_type_id']);
    }
}
