<?php

namespace common\models;

use Carbon\Carbon;
use common\components\ActiveRecord;
use common\queries\ParameterValueQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "parameter_value".
 *
 * @property integer   $value_id
 * @property integer   $param_id
 * @property string    $value
 * @property string    $date
 *
 * @property Parameter $param
 *
 * @property Carbon    $carbonDate
 */
class ParameterValue extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'parameter_value';
    }

    /**
     * @inheritdoc
     *
     * @return ParameterValueQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): ParameterValueQuery
    {
        return Yii::createObject(ParameterValueQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['param_id', 'value'],
                'required',
            ],
            [
                ['param_id'],
                'integer',
            ],
            [
                ['date'],
                'safe',
            ],
            [
                ['value'],
                'string',
                'max' => 255,
            ],
            [
                ['param_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Parameter::class,
                'targetAttribute' => ['param_id' => 'parameter_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'value_id' => 'Value ID',
            'param_id' => 'Param ID',
            'value' => 'Значение',
            'date' => 'Дата',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getParam(): ActiveQuery
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'param_id']);
    }

    /**
     * @return Carbon|null
     */
    public function getCarbonDate(): ?Carbon
    {
        if (!$this->date) {
            return null;
        }
        return Carbon::parse($this->date);
    }
}
