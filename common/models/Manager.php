<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\InformationNeedQuery;
use common\traits\EntityRelationTrait;
use common\traits\EntityTrait;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "manager".
 *
 * @property integer                               $entity_id
 * @property string                                $name
 * @property string                                $abbreviation
 *
 * @property InformationNeed[]                     $informationNeeds
 * @property Entity                                $entity
 * @property Parameter[]                           $parameters
 * @property IndustrialFunction[]                  $industrialFunctions
 * @property InformationProductToInformationNeed[] $informationProductsToInformationNeeds
 */
class Manager extends ActiveRecord implements EntityInterface
{
    use EntityTrait;
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_MANAGER;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'manager';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'abbreviation'],
                'required',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['name', 'abbreviation'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
            'abbreviation' => 'Аббревиатура',
        ];
    }

    /**
     * @return InformationNeedQuery|ActiveQuery
     */
    public function getInformationNeeds(): InformationNeedQuery
    {
        return $this->hasMany(InformationNeed::class, ['manager_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProductsToInformationNeeds(): ActiveQuery
    {
        return $this->hasMany(InformationProductToInformationNeed::class,
            ['information_need_id' => 'entity_id'])->via('informationNeeds');
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getIndustrialFunctions(): IndustrialFunctionQuery
    {
        return $this->hasMany(IndustrialFunction::class, ['manager_id' => 'entity_id']);
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
