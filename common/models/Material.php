<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\MaterialQuery;
use common\queries\NeedQuery;
use common\queries\ProductQuery;
use common\queries\ResourceQuery;
use common\traits\EntityRelationTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "material".
 *
 * @property integer              $entity_id
 * @property string               $name
 *
 * @property Entity               $entity
 * @property Product[]            $products
 * @property IndustrialFunction[] $functionsViaProducts
 * @property IndustrialFunction[] $functionsViaResources
 * @property ProductToNeed[]      $productsToNeeds
 * @property Need[]               $needs
 * @property Resource[]           $resources
 * @property SupplierToMaterial[] $suppliersToMaterials
 * @property Supplier[]           $suppliers
 * @property Parameter[]          $parameters
 */
class Material extends ActiveRecord implements EntityInterface
{
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_MATERIAL;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'material';
    }

    /**
     * @inheritdoc
     *
     * @return MaterialQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): MaterialQuery
    {
        return Yii::createObject(MaterialQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name'],
                'required',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['name'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ProductQuery|ActiveQuery
     */
    public function getProducts(): ProductQuery
    {
        return $this->hasMany(Product::class, ['material_id' => 'entity_id']);
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getFunctionsViaProducts(): IndustrialFunctionQuery
    {
        return $this->hasMany(IndustrialFunction::class, ['entity_id' => 'function_id'])
            ->via('products');
    }

    /**
     * @return ActiveQuery
     */
    public function getProductsToNeeds(): ActiveQuery
    {
        return $this->hasMany(ProductToNeed::class, ['product_id' => 'id'])
            ->via('products');
    }

    /**
     * @return NeedQuery|ActiveQuery
     */
    public function getNeeds(): NeedQuery
    {
        return $this->hasMany(Need::class, ['entity_id' => 'need_id'])
            ->via('productsToNeeds');
    }

    /**
     * @return ResourceQuery|ActiveQuery
     */
    public function getResources(): ResourceQuery
    {
        return $this->hasMany(Resource::class, ['material_id' => 'entity_id']);
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getFunctionsViaResources(): IndustrialFunctionQuery
    {
        return $this->hasMany(IndustrialFunction::class, ['entity_id' => 'function_id'])
            ->via('resources');
    }

    /**
     * @return ActiveQuery
     */
    public function getSuppliersToMaterials(): ActiveQuery
    {
        return $this->hasMany(SupplierToMaterial::class, ['material_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSuppliers(): ActiveQuery
    {
        return $this->hasMany(Supplier::class, ['entity_id' => 'supplier_id'])
            ->via('suppliersToMaterials');
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
