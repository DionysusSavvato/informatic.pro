<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\MessageQuery;
use common\queries\SituationQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "message".
 *
 * @property int                $id
 * @property int                $situation_id
 * @property int                $industrial_function_id
 * @property int                $place_id
 * @property string             $date
 *
 * @property IndustrialFunction $industrialFunction
 * @property Situation          $situation
 */
class Message extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'message';
    }

    /**
     * @inheritdoc
     *
     * @return MessageQuery
     *
     * @throws InvalidConfigException
     */
    public static function find(): MessageQuery
    {
        return Yii::createObject(MessageQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['situation_id', 'industrial_function_id', 'date'],
                'required',
            ],
            [
                ['situation_id', 'industrial_function_id', 'place_id'],
                'integer',
            ],
            [
                ['date'],
                'safe',
            ],
            [
                ['industrial_function_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => IndustrialFunction::class,
                'targetAttribute' => ['industrial_function_id' => 'entity_id'],
            ],
            [
                ['situation_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Situation::class,
                'targetAttribute' => ['situation_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'situation_id' => 'Situation ID',
            'industrial_function_id' => 'Industrial Function ID',
            'place_id' => 'Place ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getIndustrialFunction(): ActiveQuery
    {
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'industrial_function_id']);
    }

    /**
     * @return SituationQuery|ActiveQuery
     */
    public function getSituation(): SituationQuery
    {
        return $this->hasOne(Situation::class, ['id' => 'situation_id']);
    }
}
