<?php

namespace common\models;

use common\components\ActiveRecord;
use common\queries\EntityQuery;
use common\queries\IndustrialFunctionQuery;
use common\queries\InformationChangeLocalizationQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "information_change_localization".
 *
 * @property integer            $id
 * @property integer            $information_change_id
 * @property integer            $entity_id
 * @property integer            $function_id
 *
 * @property Entity             $entity
 * @property InformationChange  $informationChange
 * @property IndustrialFunction $industrialFunction
 */
class InformationChangeLocalization extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_change_localization';
    }

    /**
     * @inheritdoc
     *
     * @return InformationChangeLocalizationQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): InformationChangeLocalizationQuery
    {
        return Yii::createObject(InformationChangeLocalizationQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['information_change_id', 'entity_id', 'function_id'],
                'required',
            ],
            [
                ['information_change_id', 'entity_id', 'function_id'],
                'integer',
            ],
            [
                ['information_change_id', 'entity_id'],
                'unique',
                'targetAttribute' => ['information_change_id', 'entity_id'],
                'message' => 'The combination of Information Change ID and Entity ID has already been taken.',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
            ],
            [
                ['information_change_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InformationChange::class,
                'targetAttribute' => ['information_change_id' => 'id'],
            ],
            [
                ['function_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => IndustrialFunction::class,
                'targetAttribute' => 'entity_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'information_change_id' => 'Information Change ID',
            'entity_id' => 'Entity ID',
            'function_id' => 'Function ID',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationChange(): ActiveQuery
    {
        return $this->hasOne(InformationChange::class, ['id' => 'information_change_id']);
    }

    /**
     * @return IndustrialFunctionQuery
     */
    public function getIndustrialFunction(): IndustrialFunctionQuery
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->hasOne(IndustrialFunction::class, ['entity_id' => 'function_id']);
    }
}
