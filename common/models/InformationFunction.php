<?php

namespace common\models;

use common\components\ActiveRecord;
use common\interfaces\EntityInterface;
use common\queries\EntityQuery;
use common\queries\InformationFunctionQuery;
use common\traits\EntityRelationTrait;
use common\traits\EntityTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "information_function".
 *
 * @property integer               $entity_id
 * @property string                $name
 * @property string                $abbreviation
 *
 * @property Entity                $entity
 * @property InformationProduct[]  $informationProducts
 * @property InformationResource[] $informationResources
 * @property Parameter[]           $parameters
 */
class InformationFunction extends ActiveRecord implements EntityInterface
{
    use EntityTrait;
    use EntityRelationTrait;

    public const ENTITY_TYPE_ID = EntityType::TYPE_INFORMATION_FUNCTION;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'information_function';
    }

    /**
     * @inheritdoc
     *
     * @return InformationFunctionQuery|object
     *
     * @throws InvalidConfigException
     */
    public static function find(): InformationFunctionQuery
    {
        return Yii::createObject(InformationFunctionQuery::class, [static::class]);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'abbreviation'],
                'required',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
            ],
            [
                ['abbreviation'],
                'string',
                'max' => 20,
            ],
            [
                ['name', 'abbreviation'],
                'unique',
            ],
            [
                ['entity_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::class,
                'targetAttribute' => ['entity_id' => 'entity_id'],
                'filter' => [
                    'entity_type_id' => self::ENTITY_TYPE_ID,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'entity_id' => 'Entity ID',
            'name' => 'Название',
            'abbreviation' => 'Аббревиатура',
        ];
    }

    /**
     * @return EntityQuery|ActiveQuery
     */
    public function getEntity(): EntityQuery
    {
        return $this->hasOne(Entity::class, ['entity_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationProducts(): ActiveQuery
    {
        return $this->hasMany(InformationProduct::class, ['function_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInformationResources(): ActiveQuery
    {
        return $this->hasMany(InformationResource::class, ['function_id' => 'entity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery
    {
        return $this->hasMany(Parameter::class, ['entity_id' => 'entity_id'])
            ->via('entity');
    }

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int
    {
        return self::ENTITY_TYPE_ID;
    }
}
