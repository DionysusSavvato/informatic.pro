<?php
    return [
        'adminEmail' => 'd.savvato@gmail.com',
        'supportEmail' => 'd.savvato@gmail.com',
        'user.passwordResetTokenExpire' => 3600,
    ];
