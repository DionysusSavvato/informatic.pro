<?php

use yii\caching\FileCache;
use yii\db\Connection;
use yii\rbac\DbManager;

$dbHost = getenv('DB_HOST');
$dbName = getenv('DB_NAME');
$dbUsername = getenv('DB_USERNAME');
$dbPassword = getenv('DB_PASSWORD');

return [
    'name' => 'INFOMAN.PRO',
    'version' => 'Prototype 0.6',
    'language' => 'ru-RU',
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'db' => [
            'class' => Connection::class,
            'dsn' => "mysql:host={$dbHost};dbname={$dbName}",
            'username' => $dbUsername,
            'password' => $dbPassword,
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => FileCache::class,
        ],
        'authManager' => [
            'class' => DbManager::class,
        ],
    ],
];
