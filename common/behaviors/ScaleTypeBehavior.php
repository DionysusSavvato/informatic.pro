<?php

namespace common\behaviors;

use common\models\Scale;
use common\models\ScaleValue;
use yii\base\Behavior;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;

/**
 * Поведение реагирующее на смену типа шкалы
 *
 * @property Scale $scale
 *
 * @package common\behaviors
 */
class ScaleTypeBehavior extends Behavior
{
    /** @var Scale */
    protected $_scale;

    protected $_flagTypeChanged = false;

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'checkTypeChanged',
            ActiveRecord::EVENT_AFTER_UPDATE => 'handleScaleType',
        ];
    }

    public function init(): void
    {
        parent::init();
    }

    /**
     * Проверяем, изменился ли тип шкалы
     */
    public function checkTypeChanged(): void
    {
        $this->_scale = $this->owner;

        $this->_flagTypeChanged = $this->scale->isAttributeChanged('type_id');
    }

    /**
     * Обработчик изменения типа шкалы
     *
     * @throws InvalidParamException
     */
    public function handleScaleType(): void
    {
        $this->_scale = $this->owner;

        if ($this->_flagTypeChanged
            &&
            !$this->scale->hasFixedValues()
        ) {
            $this->deleteScaleValues();
        }
    }

    /**
     * Удаление всех возможных значений шкалы
     */
    public function deleteScaleValues(): void
    {
        ScaleValue::deleteAll([
            'scale_value_id' => $this->scale->getScaleValues()->select(['scale_value_id'])->column(),
        ]);
    }

    /**
     * @return Scale
     */
    public function getScale(): Scale
    {
        return $this->_scale;
    }


}