<?php

namespace common\services\recomendation;

use common\models\InformationEffect;
use common\models\Message;
use common\models\Situation;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Сервис для рекомендационной обработки эффекта полноты информационного обеспечения
 */
class CompletenessInformatonEffectAnalysisService extends InformationEffectAnalysisService
{
    /**
     * Проверка значения эффекта
     *
     * @param InformationEffect $effect
     *
     * @return bool
     */
    protected function isEffectOk(InformationEffect $effect): bool
    {
        return $effect->value >= $effect->informationProductToInformationNeed->effect_threshold_value;
    }

    /**
     * Создание рекомендационного сообщения
     *
     * @param InformationEffect $effect
     *
     * @return Message
     *
     * @throws BadRequestHttpException
     * @throws ServerErrorHttpException
     */
    protected function generateMessage(InformationEffect $effect): Message
    {
        $message = new Message([
            'industrial_function_id' => $this->industrialFunction->entity_id,
            'place_id' => $effect->id,
            'situation_id' => Situation::INSTORMAION_IS_NOT_COMPLETENESS,
            'date' => $effect->date,
        ]);
        $message->saveOrThrow();

        return $message;
    }
}