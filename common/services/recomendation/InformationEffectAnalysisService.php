<?php

namespace common\services\recomendation;

use common\models\IndustrialFunction;
use common\models\InformationEffect;
use common\models\Message;
use yii\base\BaseObject;

/**
 * Сервис для оценки эффекта информационного обеспечения
 *
 * @property IndustrialFunction $industrialFunction
 */
abstract class InformationEffectAnalysisService extends BaseObject
{
    /** @var IndustrialFunction */
    private $_industrialFunction;

    /**
     * Анализ значения эффекта
     *
     * @param InformationEffect $effect
     *
     * @return Message|null
     */
    public function analysis(InformationEffect $effect): ?Message
    {
        if ($this->isEffectOk($effect)) {
            return null;
        }

        return $this->generateMessage($effect);
    }

    /**
     * Проверка значения эффекта
     *
     * @param InformationEffect $effect
     *
     * @return bool
     */
    abstract protected function isEffectOk(InformationEffect $effect): bool;

    /**
     * Создание рекомендационного сообщения
     *
     * @param InformationEffect $effect
     *
     * @return Message
     */
    abstract protected function generateMessage(InformationEffect $effect): Message;


    /**
     * @return IndustrialFunction
     */
    public function getIndustrialFunction(): IndustrialFunction
    {
        return $this->_industrialFunction;
    }

    /**
     * @param IndustrialFunction $industrialFunction
     */
    public function setIndustrialFunction(IndustrialFunction $industrialFunction): void
    {
        $this->_industrialFunction = $industrialFunction;
    }

}