<?php

namespace common\services\recomendation;

use common\models\InformationEffect;
use common\models\Message;
use common\models\Situation;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Сервис для рекомендационной обработки эффекта своевременности информационного обеспечения
 */
class TimelinessInformationEffectAnalysisService extends InformationEffectAnalysisService
{
    /**
     * Проверка значения эффекта
     *
     * @param InformationEffect $effect
     *
     * @return bool
     */
    protected function isEffectOk(InformationEffect $effect): bool
    {
        return $effect->value == 1;
    }

    /**
     * Создание рекомендационного сообщения
     *
     * @param InformationEffect $effect
     *
     * @return Message
     *
     * @throws ServerErrorHttpException
     * @throws BadRequestHttpException
     */
    protected function generateMessage(InformationEffect $effect): Message
    {
        $message = new Message([
            'industrial_function_id' => $this->industrialFunction->entity_id,
            'place_id' => $effect->id,
            'situation_id' => Situation::INFORMATION_IS_NOT_TIMELINESS,
            'date' => $effect->date,
        ]);
        $message->saveOrThrow();

        return $message;
    }
}