<?php

namespace common\queries;

use Carbon\Carbon;
use common\components\ActiveQuery;
use common\models\InnerIndustrialEffect;

/**
 * Class InnerIndustrialEffectQuery
 *
 * @see InnerIndustrialEffect
 *
 * @method InnerIndustrialEffect|array|null one($db = null)
 * @method InnerIndustrialEffect[]|array all($db = null)
 */
class InnerIndustrialEffectQuery extends ActiveQuery
{
    /**
     * Сортировка по дате
     *
     * @param string|null $alias
     *
     * @return InnerIndustrialEffectQuery
     */
    public function sortByDate(string $alias = null): self
    {
        return $this->orderBy([
            static::fieldAlias('date', $alias) => SORT_ASC,
        ]);
    }

    /**
     * Фильтр по дате (от)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return InnerIndustrialEffectQuery
     */
    public function fromDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '>=',
            static::fieldAlias('date', $alias),
            $date->startOfDay()->toDateTimeString(),
        ]);
    }

    /**
     * Фильтр по дате (до)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return InnerIndustrialEffectQuery
     */
    public function toDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '<=',
            static::fieldAlias('date', $alias),
            $date->endOfDay()->toDateTimeString(),
        ]);
    }
}