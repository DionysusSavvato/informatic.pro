<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Resource;

/**
 * Class ResourceQuery
 *
 * @see Resource
 *
 * @method Resource|array|null one($db = null)
 * @method Resource[]|array all($db = null)
 */
class ResourceQuery extends ActiveQuery
{

}