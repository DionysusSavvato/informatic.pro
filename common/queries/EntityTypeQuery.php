<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\EntityType;

/**
 * Class EntityTypeQuery
 *
 * @see EntityType
 *
 * @method EntityType|array|null one($db = null)
 * @method EntityType[]|array all($db = null)
 */
class EntityTypeQuery extends ActiveQuery
{

}