<?php

namespace common\queries;

use Carbon\Carbon;
use common\components\ActiveQuery;
use common\models\InformationEffect;
use common\models\InformationEffectType;

/**
 * This is the ActiveQuery class for [[common\models\InformationEffect]].
 *
 * @see InformationEffect
 *
 * @method InformationEffect|array|null one($db = null)
 * @method InformationEffect[]|array all($db = null)
 */
class InformationEffectQuery extends ActiveQuery
{
    /**
     * Отбор измерений своевременности
     *
     * @param string|null $alias
     *
     * @return InformationEffectQuery
     */
    public function timeliness(string $alias = null): self
    {
        return $this->andWhere([
            static::fieldAlias('information_effect_type_id', $alias) => InformationEffectType::TIMELINESS,
        ]);
    }

    /**
     * Отбор измерений полноты
     *
     * @param string|null $alias
     *
     * @return InformationEffectQuery
     */
    public function completeness(string $alias = null): self
    {
        return $this->andWhere([
            static::fieldAlias('information_effect_type_id', $alias) => InformationEffectType::COMPLETENESS,
        ]);
    }

    /**
     * Фильтр по дате (от)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return InformationEffectQuery
     */
    public function fromDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '>=',
            static::fieldAlias('date', $alias),
            $date->startOfDay()->toDateTimeString(),
        ]);
    }

    /**
     * Фильтр по дате (до)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return InformationEffectQuery
     */
    public function toDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '<=',
            static::fieldAlias('date', $alias),
            $date->endOfDay()->toDateTimeString(),
        ]);
    }

    /**
     * Сортировка по дате
     *
     * @param string|null $alias
     *
     * @return InformationEffectQuery
     */
    public function sortByDate(string $alias = null): self
    {
        return $this->orderBy([
            static::fieldAlias('date', $alias) => SORT_ASC
        ]);
    }
}