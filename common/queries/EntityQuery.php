<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Entity;

/**
 * Class EntityQuery
 *
 * @see Entity
 *
 * @method Entity|array|null one($db = null)
 * @method Entity[]|array all($db = null)
 */
class EntityQuery extends ActiveQuery
{

}