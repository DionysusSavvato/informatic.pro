<?php

namespace common\queries;

use Carbon\Carbon;
use common\components\ActiveQuery;
use common\models\IndustrialEffect;

/**
 * Class IndustrialEffectQuery
 *
 * @see IndustrialEffect
 *
 * @method IndustrialEffect|array|null one($db = null)
 * @method IndustrialEffect[]|array all($db = null)
 */
class IndustrialEffectQuery extends ActiveQuery
{
    /**
     * Сортировка по дате
     *
     * @param string|null $alias
     *
     * @return IndustrialEffectQuery
     */
    public function sortByDate(string $alias = null): self
    {
        return $this->orderBy([
            static::fieldAlias('date', $alias) => SORT_ASC,
        ]);
    }

    /**
     * Фильтр по дате (от)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return IndustrialEffectQuery
     */
    public function fromDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '>=',
            static::fieldAlias('date', $alias),
            $date->startOfDay()->toDateTimeString(),
        ]);
    }

    /**
     * Фильтр по дате (до)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return IndustrialEffectQuery
     */
    public function toDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '<=',
            static::fieldAlias('date', $alias),
            $date->endOfDay()->toDateTimeString(),
        ]);
    }
}