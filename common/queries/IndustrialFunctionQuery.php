<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\IndustrialFunction;

/**
 * This is the ActiveQuery class for [[common\models\IndustrialFunction]].
 *
 * @see InformationEffect
 *
 * @method IndustrialFunction|array|null one($db = null)
 * @method IndustrialFunction[]|array all($db = null)
 */
class IndustrialFunctionQuery extends ActiveQuery
{
    /**
     * Корневые функции
     *
     * @param string|null $alias
     *
     * @return IndustrialFunctionQuery
     */
    public function root(string $alias = null): self
    {
        return $this->andOnCondition([
            static::fieldAlias('parent_id', $alias) => null
        ]);
    }
}