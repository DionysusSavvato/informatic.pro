<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\IndustrialFunction;
use common\models\InformationChangeLocalization;

/**
 * Class InformationChangeLocalizationQuery
 *
 * @see InformationChangeLocalization
 *
 * @method InformationChangeLocalization|array|null one($db = null)
 * @method InformationChangeLocalization[]|array all($db = null)
 */
class InformationChangeLocalizationQuery extends ActiveQuery
{
    /**
     * @param IndustrialFunction $function
     * @param string|null        $alias
     *
     * @return InformationChangeLocalizationQuery
     */
    public function byProcess(IndustrialFunction $function, string $alias = null): self
    {
        return $this->andWhere([
            static::fieldAlias('function_id', $alias) => $function->entity_id,
        ]);
    }
}