<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Material;

/**
 * Class MaterialQuery
 *
 * @see Material
 *
 * @method Material|array|null one($db = null)
 * @method Material[]|array all($db = null)
 */
class MaterialQuery extends ActiveQuery
{

}