<?php


namespace common\queries;

use common\components\ActiveQuery;
use common\models\ParameterValue;

/**
 * This is the ActiveQuery class for [[\common\models\ParameterValue]].
 *
 * @see ParameterValue
 *
 * @method ParameterValue|array|null one($db = null)
 * @method ParameterValue[]|array all($db = null)
 */
class ParameterValueQuery extends ActiveQuery
{
    /**
     * Сортировка по дате
     *
     * @param string|null $alias
     *
     * @return ParameterValueQuery
     */
    public function sortByDate(string $alias = null): self
    {
        return $this->orderBy([
            static::fieldAlias('date', $alias) => SORT_ASC,
        ]);
    }
}