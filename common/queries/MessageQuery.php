<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Message;

/**
 * This is the ActiveQuery class for [[\common\models\Message]].
 *
 * @see Message
 *
 * @method Message|array|null one($db = null)
 * @method Message[]|array all($db = null)
 */
class MessageQuery extends ActiveQuery
{

}
