<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Consumer;

/**
 * Class ConsumerQuery
 *
 * @see Consumer
 *
 * @method Consumer|array|null one($db = null)
 * @method Consumer[]|array all($db = null)
 */
class ConsumerQuery extends ActiveQuery
{

}