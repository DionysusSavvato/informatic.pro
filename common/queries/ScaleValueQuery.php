<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\ScaleValue;

/**
 * Class ScaleValueQuery
 *
 * @see ScaleValue
 *
 * @method ScaleValue|array|null one($db = null)
 * @method ScaleValue[]|array all($db = null)
 */
class ScaleValueQuery extends ActiveQuery
{
    /**
     * Сортировка ранговой шкалы
     *
     * @param string|null $alias
     *
     * @return ScaleValueQuery
     */
    public function sortByRank(string $alias = null): self
    {
        return $this->orderBy([static::fieldAlias('rank', $alias) => SORT_ASC]);
    }
}