<?php

namespace common\queries;

use Carbon\Carbon;
use common\components\ActiveQuery;
use common\models\InformationChange;

/**
 * Class InformationChangeQuery
 *
 * @see InformationChange
 *
 * @method InformationChange|array|null one($db = null)
 * @method InformationChange[]|array all($db = null)
 */
class InformationChangeQuery extends ActiveQuery
{
    /**
     * Сортировка по дате
     *
     * @param string|null $alias
     *
     * @return InformationChangeQuery
     */
    public function sortByDate(string $alias = null): self
    {
        return $this->orderBy([
            static::fieldAlias('date', $alias) => SORT_ASC,
        ]);
    }

    /**
     * Фильтр по дате (от)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return InformationChangeQuery
     */
    public function fromDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '>=',
            static::fieldAlias('date', $alias),
            $date->startOfDay()->toDateTimeString(),
        ]);
    }

    /**
     * Фильтр по дате (до)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return InformationChangeQuery
     */
    public function toDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '<=',
            static::fieldAlias('date', $alias),
            $date->endOfDay()->toDateTimeString(),
        ]);
    }

    public function modelChangesNeeded(): self
    {
        return $this->andWhere([
            'flag_model_changes_needed' => true,
        ]);
    }
}