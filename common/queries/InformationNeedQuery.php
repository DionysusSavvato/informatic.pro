<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\InformationNeed;

/**
 * Class InformationNeedQuery
 *
 * @see InformationNeed
 *
 * @method InformationNeed|array|null one($db = null)
 * @method InformationNeed[]|array all($db = null)
 */
class InformationNeedQuery extends ActiveQuery
{

}