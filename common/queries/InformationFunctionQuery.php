<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\InformationFunction;

/**
 * Class InformationFunctionQuery
 *
 * @see InformationFunction
 *
 * @method InformationFunction|array|null one($db = null)
 * @method InformationFunction[]|array all($db = null)
 */
class InformationFunctionQuery extends ActiveQuery
{

}