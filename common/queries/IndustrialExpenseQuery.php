<?php

namespace common\queries;

use Carbon\Carbon;
use common\components\ActiveQuery;
use common\models\IndustrialExpense;

/**
 * This is the ActiveQuery class for [[common\models\IndustrialExpense]].
 *
 * @see IndustrialExpense
 *
 * @method IndustrialExpense|array|null one($db = null)
 * @method IndustrialExpense[]|array all($db = null)
 */
class IndustrialExpenseQuery extends ActiveQuery
{
    /**
     * Сортировка по дате
     *
     * @param string|null $alias
     *
     * @return IndustrialExpenseQuery
     */
    public function sortByDate(string $alias = null): self
    {
        return $this->orderBy([
            static::fieldAlias('date', $alias) => SORT_ASC,
        ]);
    }

    /**
     * Сортировка по дате, по убыванию
     *
     * @param string|null $alias
     *
     * @return IndustrialExpenseQuery
     */
    public function sortByDateDesc(string $alias = null): self
    {
        return $this->orderBy([
            static::fieldAlias('date', $alias) => SORT_DESC,
        ]);
    }

    /**
     * Фильтр по дате (от)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return IndustrialExpenseQuery
     */
    public function fromDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '>=',
            static::fieldAlias('date', $alias),
            $date->startOfDay()->toDateTimeString(),
        ]);
    }

    /**
     * Фильтр по дате (до)
     *
     * @param Carbon|null $date
     * @param string|null $alias
     *
     * @return IndustrialExpenseQuery
     */
    public function toDate(Carbon $date = null, string $alias = null): self
    {
        if ($date === null) {
            return $this;
        }

        return $this->andWhere([
            '<=',
            static::fieldAlias('date', $alias),
            $date->endOfDay()->toDateTimeString(),
        ]);
    }
}