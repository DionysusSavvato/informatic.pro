<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Product;

/**
 * Class ProductQuery
 *
 * @see Product
 *
 * @method Product|array|null one($db = null)
 * @method Product[]|array all($db = null)
 */
class ProductQuery extends ActiveQuery
{

}