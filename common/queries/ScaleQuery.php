<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Scale;

/**
 * Class ScaleQuery
 *
 * @see Scale
 *
 * @method Scale|array|null one($db = null)
 * @method Scale[]|array all($db = null)
 */
class ScaleQuery extends ActiveQuery
{

}