<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Situation;

/**
 * This is the ActiveQuery class for [[\common\models\Situation]].
 *
 * @see Situation
 *
 * @method Situation|array|null one($db = null)
 * @method Situation[]|array all($db = null)
 */
class SituationQuery extends ActiveQuery
{

}
