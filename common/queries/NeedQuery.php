<?php

namespace common\queries;

use common\components\ActiveQuery;
use common\models\Need;

/**
 * Class NeedQuery
 *
 * @see Need
 *
 * @method Need|array|null one($db = null)
 * @method Need[]|array all($db = null)
 */
class NeedQuery extends ActiveQuery
{

}