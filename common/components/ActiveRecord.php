<?php

namespace common\components;

use Throwable;
use yii\db\ActiveRecord as YiiActiveRecord;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Переопределенный класс ActiveRecord
 */
class ActiveRecord extends YiiActiveRecord
{
    /**
     * Поиск записи по условию или выкидывание исключения, если ничего не найдено
     *
     * @param $condition
     *
     * @return static
     *
     * @throws NotFoundHttpException
     */
    public static function findOrThrow($condition): self
    {
        $result = static::findOne($condition);
        if ($result === null) {
            throw new NotFoundHttpException('Not found');
        }
        return $result;
    }

    /**
     * Сохранение модели, выкидывание исключения в случае неудачи
     *
     * @param bool       $runValidation
     * @param array|null $attributeNames
     *
     * @return bool
     *
     * @throws BadRequestHttpException
     * @throws ServerErrorHttpException
     */
    public function saveOrThrow(bool $runValidation = true, array $attributeNames = null): bool
    {
        if ($this->save($runValidation, $attributeNames) === false) {
            if ($this->hasErrors()) {
                throw new BadRequestHttpException('Ошибка валидации');
            }
            throw new ServerErrorHttpException('Ошибка при сохранении');
        }

        return true;
    }

    /**
     * Удаление сущности или выкидывание исключения в случае неудачи
     *
     * @return bool
     *
     * @throws Exception
     * @throws \Exception
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function deleteOrThrow(): bool
    {
        if ($result = ($this->delete() === false)) {
            throw new Exception('Ошибка при попытке удаления');
        }
        return $result;
    }
}