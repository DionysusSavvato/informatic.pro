<?php

namespace common\components;

use yii\db\ActiveQuery as BaseActiveQuery;

class ActiveQuery extends BaseActiveQuery
{
    /**
     * Добавление алиаса к полю
     *
     * @param string      $fieldName
     * @param string|null $alias
     *
     * @return string
     */
    public static function fieldAlias(string $fieldName, string $alias = null): string
    {
        if ($alias === null) {
            return $fieldName;
        }
        return "{$alias}.{$fieldName}";
    }
}