<?php

namespace common\traits;

use common\interfaces\EntityInterface;

/**
 * Трейт общих функций сущностей
 *
 * @property EntityInterface $this
 */
trait EntityTrait
{
    /**
     * @return string
     */
    public function toString(): string
    {
        return "<code class=\"abbreviation-code\">{$this->abbreviation}</code> <code class=\"name-code\">{$this->name}</code>";
    }
}