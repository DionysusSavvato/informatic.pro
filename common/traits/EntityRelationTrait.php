<?php

namespace common\traits;

use common\interfaces\EntityInterface;
use common\models\Entity;
use yii\web\ServerErrorHttpException;

/**
 * Трейт инициализации сущностей
 *
 * @property EntityInterface $this
 */
trait EntityRelationTrait
{
    /**
     * {@inheritDoc}
     *
     * @throws ServerErrorHttpException
     */
    public function beforeSave($insert): bool
    {
        if ($insert) {
            $this->initEntity();
        }
        return parent::beforeSave($insert);
    }

    /**
     * Инициализация классификационной сущности
     *
     * @throws ServerErrorHttpException
     */
    protected function initEntity(): void
    {
        if ($this->getEntityID() !== null) {
            return;
        }

        $entity = new Entity();
        $entity->entity_type_id = $this->getEntityTypeID();
        if ($entity->save() === false) {
            throw new ServerErrorHttpException('Не удалось инициализировать базовую сущность');
        }
        $this->setEntityID($entity->entity_id);
    }

    /**
     * Получение идентификатора сущности
     *
     * @return int
     */
    public function getEntityID(): ?int
    {
        return $this->entity_id;
    }

    /**
     * Установка идентификатора сущности
     *
     * @param int $entityID
     */
    public function setEntityID(int $entityID): void
    {
        $this->entity_id = $entityID;
    }
}