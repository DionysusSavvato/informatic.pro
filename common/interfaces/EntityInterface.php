<?php
/**
 * Created by PhpStorm.
 * User: Denis
 * Date: 08.10.2017
 * Time: 16:20
 */

namespace common\interfaces;


use common\models\Entity;
use common\models\Parameter;
use common\queries\EntityQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecordInterface;

/**
 * Интерфейс сущностей
 *
 * @property integer     $entity_id
 *
 * @property Entity      $entity
 * @property Parameter[] $parameters
 * @property string      $abbreviation
 * @property string      $name
 */
interface EntityInterface extends ActiveRecordInterface
{
    /**
     * Получение идентификатора сущности
     *
     * @return int
     */
    public function getEntityID(): ?int;

    /**
     * Установка идентификатора сущности
     *
     * @param int $entityID
     */
    public function setEntityID(int $entityID): void;

    /**
     * Получение типа сущности
     *
     * @return int
     */
    public function getEntityTypeID(): int;

    /**
     * @return EntityQuery
     */
    public function getEntity(): EntityQuery;

    /**
     * @return ActiveQuery
     */
    public function getParameters(): ActiveQuery;
}