<?php

namespace app\modules\recomendation\controllers;

use common\models\Message;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Базовый контроллер модуля рекомендаций
 */
class DefaultController extends Controller
{
    /**
     * `/recomendation`
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     */
    public function actionIndex(): string
    {
        $messagesQuery = Message::find()
            ->orderBy(['date' => SORT_DESC])
            ->with([
                'situation',
                'industrialFunction',
            ]);

        return $this->render('index', [
            'messagesProvider' => new ActiveDataProvider([
                'query' => $messagesQuery,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
        ]);
    }
}