<?php

namespace app\modules\recomendation;

use Yii;
use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль вывода рекомендаций по информационному менеджменту
 */
class RecomendationModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        parent::init();

        $this->controllerNamespace = 'app\modules\recomendation\controllers';

        $this->loadRules();
    }

    /**
     * Загрузка правил роутинга
     */
    private function loadRules(): void
    {
        Yii::$app->urlManager->addRules(
            require __DIR__ . '/config/rules.php'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}