<?php

use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\ListView;

/**
 * @var View               $this
 * @var ActiveDataProvider $messagesProvider
 */

$this->title = 'Рекомендации по информационному менеджменту';

?>

<style>
    hr {
        margin-top: 10px;
        margin-bottom: 10px;
    }
</style>

<div class="recomendation">
    <div class="container-fluid">
        <h3>
            <?= $this->title; ?>
        </h3>
        <?= ListView::widget([
            'view' => $this,
            'dataProvider' => $messagesProvider,
            'itemView' => 'index/recomendation',
        ]); ?>
    </div>
</div>