<?php

use common\models\InformationEffect;
use yii\web\View;

/**
 * @var View              $this
 * @var InformationEffect $informationEffect
 */

?>

<p style="font-size: 15px;">
    <strong>Информационная потребность: </strong>
    <?= $informationEffect->informationProductToInformationNeed->informationNeed->toString(); ?>
</p>
<p style="font-size: 15px;">
    <strong>Информационный продукт: </strong>
    <?= $informationEffect->informationProductToInformationNeed->informationProduct->toString(); ?>
</p>
<p style="font-size: 15px;">
    <strong>Информационная функция: </strong>
    <?= $informationEffect->informationProductToInformationNeed->informationProduct->informationFunction->toString(); ?>
</p>