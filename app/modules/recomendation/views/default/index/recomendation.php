<?php

use Carbon\Carbon;
use common\models\Message;
use yii\web\View;

/**
 * @var View    $this
 * @var Message $model
 */


?>
<div class="col-md-6">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $model->situation->title; ?>
            </h3>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <p style="font-size: 15px;">
                    <strong>Процесс:</strong>
                    <?= $model->industrialFunction->toString(); ?>
                </p>
                <hr>
                <p style="font-size: 15px;">
                    <strong>Ответственное лицо:</strong>
                    <?= $model->industrialFunction->manager->toString(); ?>
                </p>
                <hr>
                <p style="font-size: 15px;">
                    <strong>Дата:</strong>
                    <?= Carbon::parse($model->date)->format('d.m.Y'); ?>
                </p>
                <hr>
                <p style="font-size: 15px;">
                    <strong>Рекомендация:</strong>
                    <?= $model->situation->recomendation; ?>
                </p>
                <hr>

                <?= $this->render('detalization', ['model' => $model]); ?>
            </div>
        </div>
    </div>
</div>
