<?php

use common\models\InformationEffect;
use common\models\Message;
use common\models\Situation;
use yii\web\View;

/**
 * @var View    $this
 * @var Message $model
 */

if ($model->place_id === null) {
    return;
}

$detalizationView = '';

switch ($model->situation_id) {
    case Situation::INFORMATION_IS_NOT_TIMELINESS:
    case Situation::INSTORMAION_IS_NOT_COMPLETENESS:
        $informationEffect = InformationEffect::findOrThrow($model->place_id);
        $detalizationView = $this->render('information-effect-detalization',
            ['informationEffect' => $informationEffect]);

}
?>

    <p style="font-size: 15px; text-align: center"><strong>Детализация:</strong></p>
<?= $detalizationView; ?>