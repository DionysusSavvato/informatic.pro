<?php

namespace app\modules\admin;

use app\modules\admin\controllers\AdminController;
use app\modules\admin\controllers\RoleController;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль управления учетными записями пользователей.
 *
 * @see AdminController::actionIndex() Просмотр списка учетных записей
 * @see AdminController::actionView() Просмотр детализированной информации по учетной записи
 * @see AdminController::actionCreate() Создание учетной записи пользователя
 * @see AdminController::actionDelete() Удаление учетной записи пользователя
 *
 * @see RoleController::actionView() Список ролей пользователя
 * @see RoleController::actionAttachRole() Добавление роли пользователю
 * @see RoleController::actionDetachRole() Удаление роли пользователя
 */
class AdminModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        parent::init();
        Yii::$app->urlManager->addRules(require __DIR__ . '/config/rules.php');
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'controllers' => ['admin/admin', 'admin/role'],
                        'allow' => true,
                        'roles' => ['userManagement'],
                    ],
                ],
            ],
        ];
    }
}
