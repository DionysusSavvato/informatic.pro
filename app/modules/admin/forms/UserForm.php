<?php

namespace app\modules\admin\forms;

use common\models\User;
use yii\base\Model;

/**
 * Class UserForm
 *
 * @package app\modules\admin\forms
 */
class UserForm extends Model
{
    /** @var string */
    public $username;

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /**
     * {@inheritDoc}
     */
    public function rules(): array
    {
        return [
            [
                ['username', 'email', 'password'],
                'required',
            ],
            [
                ['email'],
                'email',
            ],
            [
                ['username'],
                'string',
                'length' => [4, 255],
            ],
            [
                ['username'],
                'unique',
                'targetClass' => User::class,
                'targetAttribute' => 'username',
            ],
            [
                ['email'],
                'unique',
                'targetClass' => User::class,
                'targetAttribute' => 'email',
            ],
            ['username', 'match', 'pattern' => '/^[a-z]\w*$/i'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels(): array
    {
        return [
            'username' => 'Логин:',
            'email' => 'E-mail:',
            'password' => 'Пароль:',
        ];
    }
}