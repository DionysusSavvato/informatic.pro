<?php
use app\modules\admin\models\User;
use yii\data\ArrayDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\View;

/** @var View $this */
/** @var User $user */
/** @var Role[] $userRoles */
/** @var array $availableRoles */

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-users"></i>Пользователи',
        'url' => ['/admin'],
    ],
    [
        'label' => "<i class='fa fa-id-card'></i> $user->username",
        'url' => ['/admin/admin/view', 'id' => $user->id],
    ],
    [
        'label' => "<i class='fa fa-user-secret'></i> Роли",
        'url' => ['/admin/role/view', 'id' => $user->id],
    ],
];
$this->title = 'Роли';
?>

<div class="roles">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Роли пользователя <?= $user->username ?></h3>
        </div>
        <div class="box-body">
            <?php
            echo GridView::widget([
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => $userRoles,
                ]),
                'columns' => [
                    [
                        'class' => SerialColumn::class,
                    ],
                    [
                        'label' => 'Наименование роли',
                        'attribute' => 'name',
                    ],
                    [
                        'label' => 'Описание',
                        'attribute' => 'description',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{delete}',
                        'urlCreator' => function (string $action, Role $model, string $key, int $index) use ($user) {
                            if ($action === 'delete') {
                                return Url::toRoute(['/admin/role/detach-role', 'id' => $user->id, 'roleName' => $model->name]);
                            }
                        },
                    ],
                ],
                'options' => [
                    'class' => 'box-body',
                ],
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover',
                ],
            ]);
            ?>
        </div>
        <div class="box-footer">
            <?=
            Html::beginForm(['/admin/role/attach-role', 'id' => $user->id], 'post');
            ?>
            <div class='form-group'>
                <?= Html::label('Добавить роль:', ['class' => 'control-label']); ?>
                <?= Html::dropDownList(
                    'roleName',
                    null,
                    $availableRoles,
                    ['class' => 'form-control']
                ); ?>
            </div class='form-group'>
            <?= Html::submitButton(
                '<i class="fa fa-plus"></i> Добавить',
                ['class' => 'btn btn-success']);
            ?>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>
