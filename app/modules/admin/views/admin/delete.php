<?php
use app\modules\admin\models\User;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var User $user */

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-users"></i>Пользователи',
        'url' => ['/admin'],
    ],
    [
        'label' => '<i class="fa fa-trash"></i>Удалить пользователя',
        'url' => ['/admin/admin/delete'],
    ],
];

$this->title = 'Удалить пользователя';
?>
<div class="admin">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $this->title ?></h3>
        </div>
        <div class="box-body">
            <p>Вы действительно хотите удалить <?= $user->username ?>?</p>
            <?= Html::a(
                '<i class="fa fa-trash"></i>  Удалить',
                ['/admin/admin/delete', 'id' => $user->id, 'confirm' => true],
                ['encode' => false, 'class' => 'btn btn-danger']) ?>
            <br><br>
            <?= Html::a(
                '<i class="fa fa-arrow-left"></i>  Отмена',
                ['/admin/admin/index',],
                ['encode' => false, 'class' => 'btn btn-success']) ?>
        </div>

    </div>
</div>

