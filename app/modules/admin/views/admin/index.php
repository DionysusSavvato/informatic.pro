<?php
use app\modules\admin\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/** @var ActiveDataProvider $dataProvider */
/** @var View $this */

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-users"></i>Пользователи',
        'url' => ['/admin'],
    ],
];
$this->title = 'Пользователи';
?>
<div class="admin">
    <div class="box">
        <div class="box-header with-border">
            <h2>Пользователи</h2>
            <?= Html::a(
                '<i class="fa fa-user-plus"></i> Добавить пользователя',
                ['/admin/admin/create'],
                [
                    'encode' => false,
                    'class' => 'btn btn-success',
                ]) ?>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => new User(),
            'columns' => [
                'id',
                'username',
                'email',
                [
                    'class' => ActionColumn::class,
                    'template' => '{view}',
                ],
            ],
            'options' => [
                'class' => 'box-body',
            ],
            'tableOptions' => [
                'class' => 'table table-bordered table-hover',
            ],
        ]);
        ?>
    </div>
</div>
