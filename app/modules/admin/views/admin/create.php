<?php

use app\modules\admin\forms\UserForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var View $this */
/* @var UserForm $model */
/* @var $form ActiveForm */

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-users"></i>Пользователи',
        'url' => ['/admin'],
    ],
    [
        'label' => '<i class="fa fa-user-plus"></i>Создать пользователя',
        'url' => ['/admin/admin/create'],
    ],
];

$this->title = 'Создать пользователя';
?>
<div class="admin">
    <div class="box">
        <div class="box-header with-border">
            <h2 class="box-title"><?= $this->title ?></h2>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <?= $form->field($model, 'email')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model, 'username')->textInput(['autocomplete' => 'off']) ?>
            <?= $form->field($model, 'password')->passwordInput(['autocomplete' => 'off']) ?>
            <div class="form-group">
                <?= Html::submitButton('<i class="fa fa-user-plus"></i>  Создать', ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>