<?php
use app\modules\admin\models\User;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

/** @var User $user */
/** @var View $this */

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-users"></i>Пользователи',
        'url' => ['/admin'],
    ],
    [
        'label' => "<i class='fa fa-id-card'></i> $user->username",
        'url' => ['/admin/admin/view', 'id' => $user->id],
    ],
];

$this->title = $user->username;

?>
<div class="admin">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $user->username ?></h3>
        </div>
        <div class="box-body">
            <?php
            echo DetailView::widget([
                'model' => $user,
                'attributes' => [
                    'id',
                    'username',
                    'email',
                    [
                        'attribute' => 'status',
                        'value' => Html::tag(
                            'span',
                            $user->status == User::STATUS_ACTIVE ? 'Активный' : 'Удаленный',
                            [
                                'class' => $user->status == User::STATUS_ACTIVE ? 'badge bg-green' : 'badge bg-red',
                            ]),
                        'format' => 'html',
                    ],
                    'created_at',
                    'updated_at',
                ],
            ]);
            ?>
        </div>
        <div class="box-footer">
            <?= Html::a(
                '<i class="fa fa-user-secret"></i> Роли',
                ['/admin/role/view', 'id' => $user->id],
                [
                    'class' => 'btn btn-app bg-blue',
                    'style' => [
                        'background-color' => '#',
                    ],
                ]) ?>
            <?= Html::a(
                '<i class="fa fa-trash-o"></i> Удалить',
                ['/admin/admin/delete', 'id' => $user->id],
                [
                    'class' => 'btn btn-app bg-red',
                    'style' => [
                        'background-color' => '#',
                    ],
                ]) ?>
        </div>
    </div>
</div>
