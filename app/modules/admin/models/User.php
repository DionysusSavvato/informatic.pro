<?php

namespace app\modules\admin\models;

use common\models\User as CommonUser;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;

/**
 * Class User
 * @package app\modules\admin\models
 */
class User extends CommonUser
{
    /**
     * Метод-фабрика для создания учетной записи пользователя
     *
     * @param $username
     * @param $email
     * @param $password
     *
     * @return bool
     *
     * @throws Exception
     */
    public static function create($username, $email, $password): bool
    {
        /** @var User $user */
        $user = new static();
        $user->username = $username;
        $user->email = $email;
        $user->password = $password;
        $user->generateAuthKey();
        return $user->save();
    }

    /**
     * Поиск учетной записи пользователя
     *
     * @param $params
     *
     * @return ActiveDataProvider
     *
     * @throws InvalidParamException
     */
    public function search($params): ActiveDataProvider
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        // загружаем данные формы поиска и производим валидацию
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // изменяем запрос добавляя в его фильтрацию
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere([
            'like',
            'username',
            $this->username,
        ]);
        $query->andFilterWhere([
            'like',
            'email',
            $this->email,
        ]);

        return $dataProvider;
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'email' => 'E-mail',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата последнего обновления',
        ];
    }
}