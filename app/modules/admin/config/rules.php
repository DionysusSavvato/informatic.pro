<?php

return [
    'admin' => 'admin/admin/index',
    'admin/<action>' => 'admin/admin/<action>',
    'admin/<id>/roles' => 'admin/role/view',
    'admin/<id>/roles/<action>' => 'admin/role/<action>',
];