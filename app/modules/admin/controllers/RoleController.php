<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\User;
use Exception;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

/**
 * Контроллер для управления ролями пользователей
 *
 * @package app\modules\admin\controllers
 */
class RoleController extends Controller
{
    /**
     * Вывод списка ролей пользователя
     *
     * @param integer $id Идентификатор пользователя
     *
     * @return string
     * @throws InvalidParamException
     */
    public function actionView(int $id): string
    {
        $user = User::findOne($id);
        if ($user) {
            $userRoles = Yii::$app->authManager->getRolesByUser($user->id);
            $availableRoles = ArrayHelper::getColumn(Yii::$app->authManager->getRoles(), 'name');
            foreach ($userRoles as $userRole) {
                ArrayHelper::remove($availableRoles, $userRole->name);
            }

            return $this->render('roles', [
                'user' => $user,
                'userRoles' => $userRoles,
                'availableRoles' => $availableRoles,
            ]);
        }

        return $this->redirect(['admin/admin/index']);
    }

    /**
     * Прикрепление роли к пользователю
     *
     * @param integer $id Идентификатор пользователя
     *
     * @return Response
     *
     * @throws Exception
     */
    public function actionAttachRole(int $id): Response
    {
        $user = User::findOne($id);
        $roleName = Yii::$app->request->post('roleName');
        $role = Yii::$app->authManager->getRole($roleName);
        Yii::$app->authManager->assign($role, $user->id);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Открепление роли от пользователя
     *
     * @param integer $id       Идентификатор пользователя
     * @param string  $roleName Название роли
     *
     * @return Response
     */
    public function actionDetachRole(int $id, string $roleName): Response
    {
        $user = User::findOne($id);
        $role = Yii::$app->authManager->getRole($roleName);
        Yii::$app->authManager->revoke($role, $user->id);

        return $this->redirect(['view', 'id' => $id]);
    }

}