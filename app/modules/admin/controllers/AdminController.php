<?php

namespace app\modules\admin\controllers;

use app\modules\admin\forms\UserForm;
use app\modules\admin\models\User;
use Exception;
use Yii;
use yii\base\Exception as BaseException;
use yii\base\InvalidParamException;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Базовый контроллер для модуля Admin
 *
 * @package app\modules\admin\controllers
 */
class AdminController extends Controller
{
    /**
     * Рендеринг страницы со списком пользователей.
     *
     * `/admin`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        $dataProvider = (new User())->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Рендеринг страницы управления пользователями.
     *
     * `/admin/view?id={$id}`
     *
     * @param integer $id ID пользователя
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionView($id): string
    {
        /** @var User $user */
        $user = User::findOne($id);

        return $this->render('view', [
            'user' => $user,
        ]);
    }

    /**
     * Создание учетной записи пользователя.
     *
     * `/admin/create`
     *
     * @return array|string|Response
     *
     * @throws BaseException
     * @throws InvalidParamException
     */
    public function actionCreate()
    {
        $userForm = new UserForm();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $userForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($userForm);
        }

        /**
         * Если POST, то создаем учетную запись пользователя.
         */
        if (Yii::$app->request->isPost && $userForm->load(Yii::$app->request->post())) {
            if (User::create($userForm->username, $userForm->email, $userForm->password)) {
                Yii::$app->getSession()->setFlash('success', 'Учетная запись пользователя успешно создана');

                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'model' => $userForm,
        ]);
    }

    /**
     * Удаление учетной записи пользователя.
     *
     * `/admin/delete?id={$id}` - Запрос подтверждения удаления
     *
     * `/admin/delete?id={$id}&confirm={$confirm}` - Удаление
     *
     * @param integer $id      ID пользователя
     * @param bool    $confirm Переменная для подтверждения
     *
     * @return string|Response
     *
     * @throws StaleObjectException
     * @throws Exception
     * @throws InvalidParamException
     */
    public function actionDelete(int $id, bool $confirm = false)
    {
        /** @var User $user */
        $user = User::findOne($id);
        if ($confirm) {
            if (User::find()->count() > 1) {
                $user->delete();
                Yii::$app->getSession()->setFlash('success', 'Учетная запись пользователя успешно удалена');
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Нельзя удалить единственного пользователя');
            }

            return $this->redirect('index');
        }

        return $this->render('delete', [
            'user' => $user,
        ]);
    }
}
