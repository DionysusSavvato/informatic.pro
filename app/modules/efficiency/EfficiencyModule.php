<?php

namespace app\modules\efficiency;

use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * Модуль для оценки эффективности бизнес-процесов
 */
class EfficiencyModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        parent::init();

        $this->controllerNamespace = 'app\modules\efficiency\controllers';

        $this->loadRules();
    }

    /**
     * Загрузка правил роутинга
     */
    private function loadRules(): void
    {
        Yii::$app->urlManager->addRules(
            ArrayHelper::merge(
                require __DIR__ . '/config/rules.php',
                []
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}