<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use yii\web\View;

/**
 * @var View $this
 */

$this->title = 'Эффективность информационного обеспечения';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-line-chart"></i>Эффективность ИО',
        'url' => ['/efficiency'],
    ],
];

?>

<?=
/** @noinspection PhpUnhandledExceptionInspection */
IndustrialFunctionHierarchyWidget::widget([
    'baseRoute' => ['/efficiency/default/efficiency']
]);
?>

