<?php

use app\assets\ChartJsPluginAnnotationAsset;
use app\modules\efficiency\widgets\industrial_card\IndustrialCardWidget;
use app\modules\efficiency\widgets\informtion_card\InformationCardWidget;
use app\modules\efficiency\widgets\inner_industrial_card\InnerIndustrialCardWidget;
use Carbon\Carbon;
use common\models\IndustrialFunction;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var Carbon|null        $dateFrom
 * @var Carbon|null        $dateTo
 */

$this->title = 'Эффективность информационного обеспечения';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-line-chart"></i>Эффективность ИО',
        'url' => ['/efficiency'],
    ],
    [
        'label' => $function->name,
        'url' => ['/efficiency/default/efficiency', 'id' => $function->entity_id],
    ],
];

ChartJsPluginAnnotationAsset::register($this);

?>

<div class="container-fluid">
    <div style="text-align: center; background-color: white; padding: 10px; border-radius: 10px; margin: 10px;">
        <h3><?= $function->manager->toString(); ?> - <?= $function->toString(); ?></h3>
        <form action="<?= Url::current(); ?>" method="post">
            <input id="form-token" type="hidden" name="<?= Yii::$app->request->csrfParam; ?>"
                   value="<?= Yii::$app->request->csrfToken; ?>"/>
            <input name="dateFrom" type="date"
                   value="<?= $dateFrom ? $dateFrom->toDateString() : Carbon::now()->subMonth()->toDateString() ?>"
                   class="form-control"
                   style="display: inline-block; width: auto;">
            -
            <input name="dateTo" type="date"
                   value="<?= $dateTo ? $dateTo->toDateString() : Carbon::now()->toDateString() ?>" class="form-control"
                   style="display: inline-block; width: auto;">
            <input type="submit" value="Установить" class="btn btn-success">
            <a class="btn btn-default"
               href="<?= Url::to(['/efficiency/default/parameters', 'id' => $function->entity_id]) ?>">
                Параметры
            </a>
        </form>
    </div>
    <h4 style="text-align: center;">Информационное обеспечение</h4>
    <div class="row">
        <?php foreach ($function->manager->informationProductsToInformationNeeds as $informationDirection): ?>
            <div class="col-md-6">
                <?= InformationCardWidget::widget([
                    'industrialFunction' => $function,
                    'informationDirection' => $informationDirection,
                    'dateFrom' => $dateFrom,
                    'dateTo' => $dateTo,
                ]); ?>
            </div>
        <?php endforeach; ?>
    </div>
    <hr style="border-color: #D2D6DE;">
    <h4 style="text-align: center;">Производство</h4>
    <div class="row">
        <?php foreach ($function->productsToNeeds as $productToNeed): ?>
            <div class="col-md-6">
                <?= IndustrialCardWidget::widget([
                    'industrialFunction' => $function,
                    'productToNeed' => $productToNeed,
                    'dateFrom' => $dateFrom,
                    'dateTo' => $dateTo,
                ]) ?>
            </div>
        <?php endforeach; ?>
        <?php foreach ($function->productToResourceViaProduct as $productToResource): ?>
            <div class="col-md-6">
                <?= InnerIndustrialCardWidget::widget([
                    'industrialFunction' => $function,
                    'productToResource' => $productToResource,
                    'dateFrom' => $dateFrom,
                    'dateTo' => $dateTo,
                ]) ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>



