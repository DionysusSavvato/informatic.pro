<?php

use common\models\Parameter;
use yii\data\ActiveDataProvider;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\web\View;

/**
 * @var View      $this
 * @var Parameter $parameter
 */

?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= $parameter->name; ?>
        </h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <?=
                GridView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => $parameter->getParameterValues()->sortByDate(),
                        'pagination' => [
                            'pageSize' => 20,
                        ],
                    ]),
                    'columns' => [
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'value',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'date',
                        ],
                    ],
                    'options' => [
                        'class' => 'box-body',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>