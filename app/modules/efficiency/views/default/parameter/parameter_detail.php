<?php

use app\assets\ChartJsPluginAnnotationAsset;
use common\models\Parameter;
use yii\web\View;

/**
 * @var View        $this
 * @var Parameter[] $parameters
 */

ChartJsPluginAnnotationAsset::register($this);

$cards = [];

foreach ($parameters as $parameter) {
    if (!$parameter->scale->hasFixedValues()) {
        $cards[] = $this->render('chart-card', ['parameter' => $parameter]);
        continue;
    }
    $cards[] = $this->render('table-card', ['parameter' => $parameter]);
}

?>

<div class="container-fluid">
    <div class="row-fluid">
        <?php foreach ($cards as $card): ?>
            <div class="col-md-6"><?= $card; ?></div>
        <?php endforeach; ?>
    </div>
</div>


