<?php

use common\models\Parameter;
use common\models\ParameterValue;
use yii\helpers\Json;
use yii\web\View;

/**
 * @var View      $this
 * @var Parameter $parameter
 */

$parameterData = [];
$labels = [];

$valuesQuery = $parameter->getParameterValues()->sortByDate();

/** @var ParameterValue $parameterValue */
foreach ($valuesQuery->each() as $parameterValue) {
    $labels[] = $parameterValue->carbonDate->toDateString();

    $parameterData[] = [
        't' => $parameterValue->carbonDate->toDateString(),
        'y' => (float)$parameterValue->value,
    ];
}

?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= $parameter->name; ?>
        </h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <?php if (!empty($parameterData)): ?>
                    <canvas id="canvas-parameter-<?= $parameter->parameter_id; ?>"
                            height="100px" style="width: 100%;"></canvas>
                <?php else: ?>
                    Нет данных
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        let parameterCanvas = document.getElementById("canvas-parameter-<?= $parameter->parameter_id; ?>");
        new Chart(parameterCanvas, {
            type: 'line',
            data: {
                fill: false,
                labels: <?= Json::encode($labels); ?>,
                datasets: [
                    {
                        label: "<?= $parameter->name; ?>",
                        data: <?= Json::encode($parameterData); ?>,
                        fill: false,
                        borderColor: "rgb(0, 255, 0)",
                        lineTension: 0.1,
                        steppedLine: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        bounds: 'data',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD.MM.YYYY'
                            }
                        },
                        ticks: {
                            source: 'labels'
                        }
                    }],
                }
            }
        });
    });
</script>

