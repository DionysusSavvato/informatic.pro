<?php

use app\widgets\structure\StructureWidget;
use common\models\IndustrialFunction;
use common\models\Parameter;
use yii\bootstrap\Tabs;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var Parameter[]        $parameters
 */

$this->title = 'Параметры';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-line-chart"></i>Эффективность ИО',
        'url' => ['/efficiency'],
    ],
    [
        'label' => 'Параметры',
        'url' => ['/efficiency/default/parameters', 'id' => $function->entity_id],
    ],
];

$tabs = [
    [
        'label' => 'Струкутра процесса',
        'content' => StructureWidget::widget([
            'industrialFunction' => $function,
            'openInOtherWindow' => false,
            'urlConfig' => [
                '/efficiency/default/parameters',
                'id' => $function->entity_id,
            ],
        ]),
        'active' => true,
    ],
];

if (!empty($parameters)) {
    $tabs[0]['active'] = false;
    $tabs[] = [
        'label' => 'Параметры',
        'content' => $this->render('parameter/parameter_detail', ['parameters' => $parameters]),
        'active' => true,
    ];
}

?>

<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => $tabs,
    ]); ?>
</div>
