<?php

namespace app\modules\efficiency\controllers;

use Carbon\Carbon;
use common\models\IndustrialFunction;
use common\models\Parameter;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Базовый контроллер для оценки эффективности процессов
 */
class DefaultController extends Controller
{
    /**
     * Базовая страница данных
     *
     * `efficiency`
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * Вывод данных по эффективности
     *
     * `/efficiency/<id>`
     *
     * @param int $id
     *
     * @body_param string $dateFrom
     * @body_param string $dateTo
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionEfficiency(int $id): string
    {
        $function = IndustrialFunction::findOrThrow($id);

        $dateFrom = Yii::$app->request->post('dateFrom') ? Carbon::parse(Yii::$app->request->post('dateFrom')) : null;
        $dateTo = Yii::$app->request->post('dateTo') ? Carbon::parse(Yii::$app->request->post('dateTo')) : null;


        return $this->render('efficiency', [
            'function' => $function,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
        ]);
    }

    /**
     * Обзор динамики параметров
     *
     * @param int $id
     *
     * @param int $entityID
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionParameters(int $id, int $entityID = null): string
    {
        $function = IndustrialFunction::findOrThrow($id);
        $parameters = Parameter::findAll(['entity_id' => $entityID]);

        if ($entityID !== null && empty($parameters)) {
            Yii::$app->session->setFlash('danger', 'Параметры не найдены');
        }

        return $this->render('parameter', [
            'function' => $function,
            'parameters' => $parameters,
        ]);
    }
}