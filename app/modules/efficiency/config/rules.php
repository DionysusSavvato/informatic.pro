<?php

return [
    'efficiency' => 'efficiency/default/index',
    'efficiency/<id:\\d+>/parameters' => 'efficiency/default/parameters',
    'efficiency/<id:\\d+>' => 'efficiency/default/efficiency',
];
