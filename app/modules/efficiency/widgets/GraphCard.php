<?php

namespace app\modules\efficiency\widgets;

use Carbon\Carbon;
use common\models\IndustrialFunction;
use common\models\InformationChange;
use common\queries\InformationChangeLocalizationQuery;
use yii\base\InvalidConfigException;
use yii\base\Widget;

/**
 * Карточка графиков
 *
 * @property IndustrialFunction $industrialFunction
 * @property Carbon|null        $dateFrom
 * @property Carbon|null        $dateTo
 */
abstract class GraphCard extends Widget
{
    /** @var string[] */
    protected $_labels = [];

    /** @var array */
    protected $_informationChangesData = [];

    /** @var IndustrialFunction */
    private $_industrialFunction;

    /** @var Carbon|null */
    private $_dateFrom;

    /** @var Carbon|null */
    private $_dateTo;

    /**
     * @return Carbon|null
     */
    public function getDateFrom(): ?Carbon
    {
        return $this->_dateFrom;
    }

    /**
     * @param Carbon|null $dateFrom
     */
    public function setDateFrom(?Carbon $dateFrom): void
    {
        $this->_dateFrom = $dateFrom;
    }

    /**
     * @return Carbon|null
     */
    public function getDateTo(): ?Carbon
    {
        return $this->_dateTo;
    }

    /**
     * @param Carbon|null $dateTo
     */
    public function setDateTo(?Carbon $dateTo): void
    {
        $this->_dateTo = $dateTo;
    }

    /**
     * @return IndustrialFunction
     */
    public function getIndustrialFunction(): IndustrialFunction
    {
        return $this->_industrialFunction;
    }

    /**
     * @param IndustrialFunction $industrialFunction
     */
    public function setIndustrialFunction(IndustrialFunction $industrialFunction): void
    {
        $this->_industrialFunction = $industrialFunction;
    }

    /**
     * @throws InvalidConfigException
     */
    protected function initInformationChanges(): void
    {
        $informationChanges = InformationChange::find()
            ->joinWith([
                'informationChangeLocalizations icl' => function (InformationChangeLocalizationQuery $query) {
                    $query->byProcess($this->industrialFunction);
                },
            ], false)
            ->fromDate($this->dateFrom)
            ->toDate($this->dateTo)
            ->sortByDate()
            ->all();
        foreach ($informationChanges as $informationChange) {
            $this->_labels[] = $informationChange->carbonDate->toDateString();
            $this->_informationChangesData[] = [
                'id' => $informationChange->id,
                'label' => $informationChange->title,
                'value' => $informationChange->carbonDate->toDateString(),
            ];
        }
    }

    /**
     * Подготовка подписей
     */
    protected function prepareLabels(): void
    {
        asort($this->_labels);
        $this->_labels = array_values($this->_labels);
    }
}