<?php

namespace app\modules\efficiency\widgets\informtion_card;

use app\modules\efficiency\widgets\GraphCard;
use common\models\InformationEffect;
use common\models\InformationExpense;
use common\models\InformationProductToInformationNeed;
use common\queries\InformationEffectQuery;
use common\queries\InformationExpenseQuery;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii2mod\helpers\ArrayHelper;

/**
 * Class InformationCardWidget
 *
 * @property InformationProductToInformationNeed $informationDirection
 */
class InformationCardWidget extends GraphCard
{
    /** @var InformationProductToInformationNeed */
    private $_informationDirection;

    /** @var array */
    private $_timelinessEffectsData = [];

    /** @var array */
    private $_timelinessEfficiencyData = [];

    /** @var array */
    private $_completenessEffectsData = [];

    /** @var array */
    private $_completenessEfficiencyData = [];

    /** @var array */
    private $_expensesData = [];

    /**
     * @return string
     *
     * @throws InvalidConfigException
     * @throws InvalidArgumentException
     */
    public function run(): string
    {
        $timelinessEffects = $this->getTimelinessEffectsQuery()->all();
        $completenessEffects = $this->getCompletenessEffectsQuery()->all();
        $informationExpenses = $this->getInformationExpensesQuery()->all();

        $this->initTimelinessData($timelinessEffects);
        $this->initCompletenessData($completenessEffects);
        $this->initExpensesData($informationExpenses);
        $this->initInformationChanges();

        $timelinessEffectDelta = null;
        $timelinessEfficiencyDelta = null;
        $completenessEffectDelta = null;
        $completenessEfficiencyDelta = null;

        $effectsExists = !empty($this->_timelinessEffectsData) || !empty($this->_completenessEffectsData);

        if ($effectsExists) {
            $timelinessEffectDelta = $timelinessEffects ?
                ArrayHelper::last($timelinessEffects)->value - ArrayHelper::first($timelinessEffects)->value
                :
                0;
            $completenessEffectDelta = $completenessEffects ?
                ArrayHelper::last($completenessEffects)->value - ArrayHelper::first($completenessEffects)->value
                :
                0;
            $timelinessEfficiencyDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_timelinessEfficiencyData,
                    'y'))
                -
                ArrayHelper::first(ArrayHelper::getColumn($this->_timelinessEfficiencyData, 'y'));
            $completenessEfficiencyDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_completenessEfficiencyData,
                    'y'))
                -
                ArrayHelper::first(ArrayHelper::getColumn($this->_completenessEfficiencyData, 'y'));
        }
        $expensesDelta = null;
        if (!empty($this->_expensesData)) {
            $expensesDelta = ArrayHelper::last($informationExpenses)->sum - ArrayHelper::first($informationExpenses)->sum;
        }

        $this->prepareLabels();

        return $this->render('card', [
            'informationDirection' => $this->informationDirection,
            'informationNeed' => $this->informationDirection->informationNeed,
            'informationProduct' => $this->informationDirection->informationProduct,
            'labels' => $this->_labels,
            'effectsExists' => $effectsExists,
            'expensesExists' => !empty($this->_expensesData),
            'timelinessEffectsData' => $this->_timelinessEffectsData,
            'timelinessEfficiencyData' => $this->_timelinessEfficiencyData,
            'completenessEffectsData' => $this->_completenessEffectsData,
            'completenessEfficiencyData' => $this->_completenessEfficiencyData,
            'expensesData' => $this->_expensesData,
            'informationChangesData' => $this->_informationChangesData,
            'timelinessEffectDelta' => $timelinessEffectDelta,
            'completenessEfficiencyDelta' => $completenessEfficiencyDelta,
            'timelinessEfficiencyDelta' => $timelinessEfficiencyDelta,
            'expensesDelta' => $expensesDelta,
            'completenessEffectDelta' => $completenessEffectDelta,
        ]);
    }

    /**
     * Запрос на эффекты своевременности
     *
     * @return InformationEffectQuery
     */
    protected function getTimelinessEffectsQuery(): InformationEffectQuery
    {
        return $this->informationDirection->getInformationEffects()
            ->timeliness()
            ->fromDate($this->dateFrom)
            ->toDate($this->dateTo)
            ->sortByDate();
    }

    /**
     * Запрос на эффекты полноты
     *
     * @return InformationEffectQuery
     */
    protected function getCompletenessEffectsQuery(): InformationEffectQuery
    {
        return $this->informationDirection->getInformationEffects()
            ->completeness()
            ->fromDate($this->dateFrom)
            ->toDate($this->dateTo)
            ->sortByDate();
    }

    /**
     * Запрос на затраты
     *
     * @return InformationExpenseQuery
     *
     * @throws InvalidConfigException
     */
    protected function getInformationExpensesQuery(): InformationExpenseQuery
    {
        return InformationExpense::find()
            ->fromDate($this->dateFrom)
            ->toDate($this->dateTo)
            ->sortByDate();
    }

    /**
     * Инициализация данных по своевременности
     *
     * @param InformationEffect[] $timelinessEffects
     */
    protected function initTimelinessData(array $timelinessEffects): void
    {
        foreach ($timelinessEffects as $timelinessEffect) {
            $this->_labels[] = $timelinessEffect->carbonDate->toDateString();
            $this->_timelinessEffectsData[] = [
                't' => $timelinessEffect->carbonDate->toDateString(),
                'y' => $timelinessEffect->value,
            ];
            $expense = $timelinessEffect->informationExpense;
            if ($expense) {
                $this->_timelinessEfficiencyData[] = [
                    't' => $timelinessEffect->carbonDate->toDateString(),
                    'y' => $timelinessEffect->value / $expense->sum,
                ];
            }
        }
    }

    /**
     * Инициализация данных по полноте
     *
     * @param InformationEffect[] $completenessEffects
     */
    protected function initCompletenessData(array $completenessEffects): void
    {
        foreach ($completenessEffects as $completenessEffect) {
            $this->_labels[] = $completenessEffect->carbonDate->toDateString();
            $this->_completenessEffectsData[] = [
                't' => $completenessEffect->carbonDate->toDateString(),
                'y' => $completenessEffect->value,
            ];
            $expense = $completenessEffect->informationExpense;
            if ($expense) {
                $this->_completenessEfficiencyData[] = [
                    't' => $completenessEffect->carbonDate->toDateString(),
                    'y' => $completenessEffect->value / $expense->sum,
                ];
            }
        }
    }

    /**
     * Инициализация расчетов затрат
     *
     * @param InformationExpense[] $informationExpenses
     */
    protected function initExpensesData(array $informationExpenses): void
    {
        foreach ($informationExpenses as $informationExpense) {
            $this->_expensesData[] = [
                't' => $informationExpense->carbonDate->toDateString(),
                'y' => $informationExpense->sum,
            ];
            $this->_labels[] = $informationExpense->carbonDate->toDateString();
        }
    }


    /**
     * @return InformationProductToInformationNeed
     */
    public function getInformationDirection(): InformationProductToInformationNeed
    {
        return $this->_informationDirection;
    }

    /**
     * @param InformationProductToInformationNeed $informationDirection
     */
    public function setInformationDirection(InformationProductToInformationNeed $informationDirection): void
    {
        $this->_informationDirection = $informationDirection;
    }
}