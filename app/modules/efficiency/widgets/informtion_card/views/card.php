<?php

use common\models\InformationNeed;
use common\models\InformationProduct;
use common\models\InformationProductToInformationNeed;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;


/**
 * @var View                                $this
 * @var InformationProductToInformationNeed $informationDirection
 * @var InformationProduct                  $informationProduct
 * @var InformationNeed                     $informationNeed
 * @var array                               $informationChangesData
 * @var boolean                             $effectsExists
 * @var float                               $timelinessEfficiencyDelta
 * @var float                               $completenessEfficiencyDelta
 * @var float                               $timelinessEffectDelta
 * @var float                               $completenessEffectDelta
 * @var boolean                             $expensesExists
 * @var float                               $expensesDelta
 * @var array                               $timelinessEfficiencyData
 * @var array                               $completenessEfficiencyData
 * @var array                               $timelinessEffectsData
 * @var array                               $completenessEffectsData
 * @var array                               $expensesData
 */
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= $informationProduct->toString(); ?>
            <br><br>
            <?= $informationNeed->toString(); ?>
        </h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <h4 style="text-align: center">Эффективность</h4>
                <?php if ($effectsExists): ?>
                    <div class="col-md-9">
                        <canvas id="canvas-efficiency-<?= $informationDirection->id; ?>" height="250px"
                                style="width: 100%;">
                        </canvas>
                    </div>
                    <div class="col-md-3">
                        <h5 style="text-align: center">Изменение</h5>
                        <small>Своевременность: <?= $timelinessEfficiencyDelta; ?></small>
                        <br>
                        <small>Полнота: <?= $completenessEfficiencyDelta; ?></small>
                    </div>
                <?php else: ?>
                    Нет данных
                <?php endif; ?>
            </div>
            <div class="row">
                <h4 style="text-align: center">Эффекты</h4>
                <?php if ($effectsExists): ?>
                    <div class="col-md-9">
                        <canvas id="canvas-effects-<?= $informationDirection->id; ?>" height="250px"
                                style="width: 100%;">
                        </canvas>
                    </div>
                    <div class="col-md-3">
                        <h5 style="text-align: center">Изменение</h5>
                        <small>Своевременность: <?= $timelinessEffectDelta ?></small>
                        <br>
                        <small>Полнота: <?= $completenessEffectDelta ?></small>
                    </div>
                <?php else: ?>
                    Нет данных
                <?php endif; ?>
            </div>
            <div class="row">
                <h4 style="text-align: center">Затраты</h4>
                <?php if ($expensesExists): ?>
                    <div class="col-md-9">
                        <canvas id="canvas-expenses-<?= $informationDirection->id; ?>" height="250px"
                                style="width: 100%;">
                        </canvas>
                    </div>
                    <div class="col-md-3">
                        <h5 style="text-align: center">Изменение</h5>
                        <small>Затраты: <?= $expensesDelta; ?></small>
                    </div>
                <?php else: ?>
                    Нет данных
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        let efficiencyCanvas = document.getElementById("canvas-efficiency-<?= $informationDirection->id; ?>");

        <?php if ($effectsExists): ?>
        new Chart(efficiencyCanvas, {
            type: 'line',
            data: {
                fill: false,
                labels: <?= Json::encode($labels); ?>,
                datasets: [
                    {
                        label: 'Своевременность',
                        data: <?= Json::encode($timelinessEfficiencyData); ?>,
                        fill: false,
                        borderColor: "rgb(0, 128, 128)",
                        lineTension: 0.1,
                        steppedLine: false,
                    },
                    {
                        label: 'Полнота',
                        data: <?= Json::encode($completenessEfficiencyData); ?>,
                        fill: false,
                        borderColor: "rgb(0, 255, 0)",
                        lineTension: 0.1,
                        steppedLine: false,
                    },
                ]
            },

            options: {
                events: ['mouseenter', 'mouseleave', 'click'],
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        bounds: 'data',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD.MM.YYYY'
                            }
                        },
                        ticks: {
                            source: 'labels'
                        }
                    }],
                },
                annotation: {
                    drawTime: 'afterDatasetsDraw', // (default)
                    events: ['mouseenter', 'mouseleave', 'click'],
                    annotations: [
                        <?php foreach ($informationChangesData as $informationChangeData): ?>
                        {
                            drawTime: 'afterDraw',
                            type: 'line',
                            mode: 'vertical',
                            scaleID: "x-axis-0",
                            value: "<?= $informationChangeData['value']; ?>",
                            borderColor: 'blue',
                            borderWidth: 3,
                            label: {
                                backgroundColor: "blue",
                                fontSize: 8,
                                content: "<?= $informationChangeData['label']; ?>",
                                enabled: false,
                                position: "bottom",
                                yPadding: 1,
                            },
                            onMouseenter() {
                                this._model.labelEnabled = true;
                                this.chartInstance.render();
                            },
                            onMouseleave() {
                                this._model.labelEnabled = false;
                                this.chartInstance.render();
                            },
                            onClick() {
                                window.open("<?=
                                    Url::to([
                                        '/monitoring/change-registrator/default/view',
                                        'id' => $informationChangeData['id'],
                                    ]);
                                    ?>");
                            }
                        },
                        <?php endforeach; ?>
                    ]
                }
            }
        });

        let effectsCanvas = document.getElementById("canvas-effects-<?= $informationDirection->id; ?>");
        new Chart(effectsCanvas, {
            type: 'line',
            data: {
                fill: false,
                labels: <?= Json::encode($labels); ?>,
                datasets: [
                    {
                        //xAxisID: "x-effects-axe",
                        label: 'Своевременность',
                        data: <?= Json::encode($timelinessEffectsData); ?>,
                        fill: false,
                        borderColor: "rgb(0, 128, 128)",
                        lineTension: 0.1,
                        steppedLine: false,
                    },
                    {
                        label: 'Полнота',
                        data: <?= Json::encode($completenessEffectsData); ?>,
                        fill: false,
                        borderColor: "rgb(0, 255, 0)",
                        lineTension: 0.1,
                        steppedLine: false,
                    },
                ]
            },

            options: {
                events: ['mouseenter', 'mouseleave', 'click'],
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        bounds: 'data',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD.MM.YYYY'
                            }
                        },
                        ticks: {
                            source: 'labels'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            max: 1.2,
                            min: 0,
                            stepSize: 0.2
                        }
                    }]
                },
                annotation: {
                    drawTime: 'afterDatasetsDraw', // (default)
                    events: ['mouseenter', 'mouseleave', 'click'],
                    annotations: [
                        <?php foreach ($informationChangesData as $informationChangeData): ?>
                        {
                            drawTime: 'afterDraw',
                            type: 'line',
                            mode: 'vertical',
                            scaleID: "x-axis-0",
                            value: "<?= $informationChangeData['value']; ?>",
                            borderColor: 'blue',
                            borderWidth: 3,
                            label: {
                                backgroundColor: "blue",
                                fontSize: 8,
                                content: "<?= $informationChangeData['label']; ?>",
                                enabled: false,
                                position: "bottom",
                                yPadding: 1,
                            },
                            onMouseenter() {
                                this._model.labelEnabled = true;
                                this.chartInstance.render();
                            },
                            onMouseleave() {
                                this._model.labelEnabled = false;
                                this.chartInstance.render();
                            },
                            onClick() {
                                window.open("<?=
                                    Url::to([
                                        '/monitoring/change-registrator/default/view',
                                        'id' => $informationChangeData['id'],
                                    ]);
                                    ?>");
                            }
                        },
                        <?php endforeach; ?>
                    ]
                }
            }
        });
        <?php endif; ?>

        <?php if ($expensesExists): ?>
        let expensesCanvas = document.getElementById("canvas-expenses-<?= $informationDirection->id; ?>");
        new Chart(expensesCanvas, {
            type: 'line',
            data: {
                fill: false,
                labels: <?= Json::encode($labels); ?>,
                datasets: [{
                    label: 'Затраты',
                    data: <?= Json::encode($expensesData); ?>,
                    fill: false,
                    borderColor: "rgb(255, 0, 0)",
                    lineTension: 0.1,
                    steppedLine: false,
                }]
            },
            options: {
                events: ['mouseenter', 'mouseleave', 'click'],
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        bounds: 'data',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD.MM.YYYY'
                            }
                        },
                        ticks: {
                            source: 'labels'
                        }
                    }],
                },
                annotation: {
                    drawTime: 'afterDatasetsDraw', // (default)
                    events: ['mouseenter', 'mouseleave', 'click'],
                    annotations: [
                        <?php foreach ($informationChangesData as $informationChangeData): ?>
                        {
                            drawTime: 'afterDraw',
                            type: 'line',
                            mode: 'vertical',
                            scaleID: "x-axis-0",
                            value: "<?= $informationChangeData['value']; ?>",
                            borderColor: 'blue',
                            borderWidth: 3,
                            label: {
                                backgroundColor: "blue",
                                fontSize: 8,
                                content: "<?= $informationChangeData['label']; ?>",
                                enabled: false,
                                position: "bottom",
                                yPadding: 1,
                            },
                            onMouseenter() {
                                this._model.labelEnabled = true;
                                this.chartInstance.render();
                            },
                            onMouseleave() {
                                this._model.labelEnabled = false;
                                this.chartInstance.render();
                            },
                            onClick() {
                                window.open("<?= Url::to([
                                    '/monitoring/change-registrator/default/view',
                                    'id' => $informationChangeData['id'],
                                ]);
                                    ?>");
                            }
                        },
                        <?php endforeach; ?>
                    ]
                }
            }
        });
        <?php endif; ?>
    });
</script>
