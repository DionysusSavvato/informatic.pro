<?php

namespace app\modules\efficiency\widgets\industrial_card;

use app\modules\efficiency\widgets\GraphCard;
use common\models\IndustrialEffect;
use common\models\IndustrialExpense;
use common\models\ProductToNeed;
use common\queries\IndustrialEffectQuery;
use common\queries\IndustrialExpenseQuery;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii2mod\helpers\ArrayHelper;

/**
 * Class IndustrialCardWidget
 *
 * @property ProductToNeed $productToNeed
 */
class IndustrialCardWidget extends GraphCard
{
    /** @var ProductToNeed */
    private $_productToNeed;

    /** @var array */
    private $_efficiencyData = [];

    /** @var array */
    private $_effectData = [];

    /** @var array */
    private $_expensesData = [];

    /**
     * @return string
     *
     * @throws InvalidConfigException
     * @throws InvalidArgumentException
     */
    public function run(): string
    {
        $this->initData();
        $this->initInformationChanges();

        $efficiencyDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_efficiencyData,
                'y'))
            -
            ArrayHelper::first(ArrayHelper::getColumn($this->_efficiencyData, 'y'));

        $effectsDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_effectData,
                'y'))
            -
            ArrayHelper::first(ArrayHelper::getColumn($this->_effectData, 'y'));

        $expensesDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_expensesData,
                'y'))
            -
            ArrayHelper::first(ArrayHelper::getColumn($this->_expensesData, 'y'));

        return $this->render('card', [
            'productToNeed' => $this->productToNeed,
            'product' => $this->productToNeed->product,
            'need' => $this->productToNeed->need,
            'expensesData' => $this->_expensesData,
            'efficiencyData' => $this->_efficiencyData,
            'effectsData' => $this->_effectData,
            'labels' => $this->_labels,
            'efficiencyDelta' => $efficiencyDelta,
            'effectsDelta' => $effectsDelta,
            'expensesDelta' => $expensesDelta,
            'informationChangesData' => $this->_informationChangesData,
        ]);
    }

    /**
     * @return ProductToNeed
     */
    public function getProductToNeed(): ProductToNeed
    {
        return $this->_productToNeed;
    }

    /**
     * @param ProductToNeed $productToNeed
     */
    public function setProductToNeed(ProductToNeed $productToNeed): void
    {
        $this->_productToNeed = $productToNeed;
    }

    /**
     * @throws InvalidConfigException
     */
    protected function initData(): void
    {
        $query = $this->getIndustrialEffectsQuery();

        /** @var IndustrialEffect $effect */
        foreach ($query->each() as $effect) {
            $this->_effectData[] = [
                't' => $effect->carbonDate->toDateString(),
                'y' => $effect->value,
            ];
            $this->_labels[] = $effect->carbonDate->toDateString();

            /** @var IndustrialExpenseQuery $expensesQuery */
            $expensesQuery =
                $effect->getIndustrialExpenses()
                    ->fromDate($this->dateFrom)
                    ->toDate($this->dateTo)
                    ->andWhere([
                        'id' => IndustrialExpense::find()
                            ->select([
                                'MAX(id)',
                            ])
                            ->toDate($effect->carbonDate)
                            ->groupBy(['resource_id']),
                    ]);

            if ($expensesQuery->exists()) {
                $expensesSum = 0;

                foreach ($expensesQuery->each() as $expense) {
                    $expensesSum += $expense->price * $expense->count;
                }

                $this->_expensesData[] = [
                    't' => $effect->carbonDate->toDateString(),
                    'y' => $expensesSum,
                ];

                if ($expensesSum !== 0) {
                    $this->_efficiencyData[] = [
                        't' => $effect->carbonDate->toDateString(),
                        'y' => $effect->value / $expensesSum,
                    ];
                }
            }
        }
    }

    /**
     * Запрос на получение эффектов
     *
     * @return IndustrialEffectQuery
     */
    protected function getIndustrialEffectsQuery(): IndustrialEffectQuery
    {
        return $this->productToNeed
            ->getIndustrialEffects()
            ->fromDate($this->dateFrom)
            ->toDate($this->dateTo)
            ->sortByDate();
    }
}