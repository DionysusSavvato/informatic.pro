<?php

namespace app\modules\efficiency\widgets\inner_industrial_card;

use app\modules\efficiency\widgets\GraphCard;
use common\models\IndustrialExpense;
use common\models\ProductToResource;
use common\queries\IndustrialExpenseQuery;
use common\queries\InnerIndustrialEffectQuery;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii2mod\helpers\ArrayHelper;

/**
 * Class InnerIndustrialCardWidget
 *
 * @property ProductToResource $productToResource
 */
class InnerIndustrialCardWidget extends GraphCard
{
    /** @var ProductToResource */
    private $_productToResource;

    /** @var array */
    private $_innerIndustrialEfficiencyData = [];

    /** @var array */
    private $_innerIndustrialEffectsData = [];

    /** @var array */
    private $_innerIndustrialExpensesData = [];

    /**
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     */
    public function run(): string
    {
        $resource = $this->productToResource->resource;

        $this->initData();
        $this->initInformationChanges();

        $efficiencyDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_innerIndustrialEfficiencyData,
                'y'))
            -
            ArrayHelper::first(ArrayHelper::getColumn($this->_innerIndustrialEfficiencyData, 'y'));

        $effectsDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_innerIndustrialEffectsData,
                'y'))
            -
            ArrayHelper::first(ArrayHelper::getColumn($this->_innerIndustrialEffectsData, 'y'));;

        $expensesDelta = ArrayHelper::last(ArrayHelper::getColumn($this->_innerIndustrialExpensesData,
                'y'))
            -
            ArrayHelper::first(ArrayHelper::getColumn($this->_innerIndustrialExpensesData, 'y'));

        return $this->render('card', [
            'resource' => $resource,
            'targetFunction' => $resource->function,
            'efficiencyData' => $this->_innerIndustrialEfficiencyData,
            'expensesData' => $this->_innerIndustrialExpensesData,
            'effectsData' => $this->_innerIndustrialEffectsData,
            'efficiencyDelta' => $efficiencyDelta,
            'effectsDelta' => $effectsDelta,
            'expensesDelta' => $expensesDelta,
            'labels' => $this->_labels,
            'productToResource' => $this->productToResource,
            'informationChangesData' => $this->_informationChangesData,
        ]);
    }

    /**
     * Инициализация расчетов
     *
     * @throws InvalidConfigException
     */
    protected function initData(): void
    {
        $query = $this->getInnerEffectsQuery();
        foreach ($query->each() as $effect) {
            $this->_innerIndustrialEffectsData[] = [
                't' => $effect->carbonDate->toDateString(),
                'y' => $effect->value,
            ];

            $this->_labels[] = $effect->carbonDate->toDateString();


            /** @var IndustrialExpenseQuery $expensesQuery */
            $expensesQuery =
                $effect->getIndustrialExpensesOfSourceFunction()
                    ->fromDate($this->dateFrom)
                    ->toDate($this->dateTo)
                    ->andWhere([
                        'id' => IndustrialExpense::find()
                            ->select([
                                'MAX(id)',
                            ])
                            ->toDate($effect->carbonDate)
                            ->groupBy(['resource_id']),
                    ]);

            if ($expensesQuery->exists()) {
                $expensesSum = 0;

                foreach ($expensesQuery->each() as $expense) {
                    $expensesSum += $expense->price * $expense->count;
                }

                $this->_innerIndustrialExpensesData[] = [
                    't' => $effect->carbonDate->toDateString(),
                    'y' => $expensesSum,
                ];

                if ($expensesSum !== 0) {
                    $this->_innerIndustrialEfficiencyData[] = [
                        't' => $effect->carbonDate->toDateString(),
                        'y' => $effect->value / $expensesSum,
                    ];
                }
            }
        }
    }

    /**
     * @return ProductToResource
     */
    public function getProductToResource(): ProductToResource
    {
        return $this->_productToResource;
    }

    /**
     * @param ProductToResource $productToResource
     */
    public function setProductToResource(ProductToResource $productToResource): void
    {
        $this->_productToResource = $productToResource;
    }

    /**
     * Запрос на измерения внутренних эффектов
     *
     * @return InnerIndustrialEffectQuery
     */
    protected function getInnerEffectsQuery(): InnerIndustrialEffectQuery
    {
        return $this->productToResource
            ->getInnerIndustrialEffects()
            ->fromDate($this->dateFrom)
            ->toDate($this->dateTo)
            ->sortByDate();
    }
}