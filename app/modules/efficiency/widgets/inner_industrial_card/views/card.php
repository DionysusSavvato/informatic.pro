<?php

use common\models\IndustrialFunction;
use common\models\ProductToResource;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var array              $expensesData
 * @var IndustrialFunction $targetFunction
 * @var array              $efficiencyData
 * @var float              $expensesDelta
 * @var array              $effectsData
 * @var Resource           $resource
 * @var ProductToResource  $productToResource
 * @var float              $efficiencyDelta
 * @var float              $effectsDelta
 * @var string[]           $labels
 * @var array              $informationChangesData
 */
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= $resource->toString(); ?>
            <br><br>
            <?= $targetFunction->toString(); ?>
        </h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <h4 style="text-align: center">Эффективность</h4>
                <?php if (!empty($efficiencyData)): ?>
                    <div class="col-md-9">
                        <canvas id="canvas-inner-industrial-efficiency-<?= $productToResource->id; ?>"
                                height="250px" style="width: 100%;"></canvas>
                    </div>
                    <div class="col-md-3">
                        <h5 style="text-align: center">Изменение</h5>
                        <small>Эффективность: <?= $efficiencyDelta; ?></small>
                    </div>
                <?php else: ?>
                    Нет данных
                <?php endif; ?>
            </div>
            <div class="row">
                <h4 style="text-align: center">Эффекты</h4>
                <?php if (!empty($effectsData)): ?>
                    <div class="col-md-9">
                        <canvas id="canvas-inner-industrial-effects-<?= $productToResource->id; ?>" height="250px"
                                style="width: 100%;">
                        </canvas>
                    </div>
                    <div class="col-md-3">
                        <h5 style="text-align: center">Изменение</h5>
                        <small>Эффект: <?= $effectsDelta; ?></small>
                    </div>
                <?php else: ?>
                    Нет данных
                <?php endif; ?>
            </div>
            <div class="row">
                <h4 style="text-align: center">Затраты</h4>
                <?php if (!empty($expensesData)): ?>
                    <div class="col-md-9">
                        <canvas id="canvas-inner-industrial-expenses-<?= $productToResource->id; ?>"
                                height="250px" style="width: 100%;">
                        </canvas>
                    </div>
                    <div class="col-md-3">
                        <h5 style="text-align: center">Изменение</h5>
                        <small>Затраты: <?= $expensesDelta; ?></small>
                    </div>
                <?php else: ?>
                    Нет данных
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        <?php if (!empty($efficiencyData)): ?>
        let efficiencyCanvas = document.getElementById("canvas-inner-industrial-efficiency-<?= $productToResource->id; ?>");
        new Chart(efficiencyCanvas, {
            type: 'line',
            data: {
                fill: false,
                labels: <?= Json::encode($labels); ?>,
                datasets: [
                    {
                        label: 'Эффективность',
                        data: <?= Json::encode($efficiencyData); ?>,
                        fill: false,
                        borderColor: "rgb(0, 255, 0)",
                        lineTension: 0.1,
                        steppedLine: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        bounds: 'data',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD.MM.YYYY'
                            }
                        },
                        ticks: {
                            source: 'labels'
                        }
                    }],
                },
                annotation: {
                    drawTime: 'afterDatasetsDraw', // (default)
                    events: ['mouseenter', 'mouseleave', 'click'],
                    annotations: [
                        <?php foreach ($informationChangesData as $informationChangeData): ?>
                        {
                            drawTime: 'afterDraw',
                            type: 'line',
                            mode: 'vertical',
                            scaleID: "x-axis-0",
                            value: "<?= $informationChangeData['value']; ?>",
                            borderColor: 'blue',
                            borderWidth: 3,
                            label: {
                                backgroundColor: "blue",
                                fontSize: 8,
                                content: "<?= $informationChangeData['label']; ?>",
                                enabled: false,
                                position: "bottom",
                                yPadding: 1,
                            },
                            onMouseenter() {
                                this._model.labelEnabled = true;
                                this.chartInstance.render();
                            },
                            onMouseleave() {
                                this._model.labelEnabled = false;
                                this.chartInstance.render();
                            },
                            onClick() {
                                window.open("<?= Url::to([
                                    '/monitoring/change-registrator/default/view',
                                    'id' => $informationChangeData['id'],
                                ]);
                                    ?>");
                            }
                        },
                        <?php endforeach; ?>
                    ]
                }
            }
        });
        <?php endif; ?>

        <?php if (!empty($effectsData)): ?>
        let effectsCanvas = document.getElementById("canvas-inner-industrial-effects-<?= $productToResource->id; ?>");
        new Chart(effectsCanvas, {
            type: 'line',
            data: {
                fill: false,
                labels: <?= Json::encode($labels); ?>,
                datasets: [
                    {
                        label: 'Эффекты',
                        data: <?= Json::encode($effectsData); ?>,
                        fill: false,
                        borderColor: "rgb(0, 128, 128)",
                        lineTension: 0.1,
                        steppedLine: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        bounds: 'data',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD.MM.YYYY'
                            }
                        },
                        ticks: {
                            source: 'labels'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            max: 1.2,
                            min: 0,
                            stepSize: 0.2
                        }
                    }]
                },
                annotation: {
                    drawTime: 'afterDatasetsDraw', // (default)
                    events: ['mouseenter', 'mouseleave', 'click'],
                    annotations: [
                        <?php foreach ($informationChangesData as $informationChangeData): ?>
                        {
                            drawTime: 'afterDraw',
                            type: 'line',
                            mode: 'vertical',
                            scaleID: "x-axis-0",
                            value: "<?= $informationChangeData['value']; ?>",
                            borderColor: 'blue',
                            borderWidth: 3,
                            label: {
                                backgroundColor: "blue",
                                fontSize: 8,
                                content: "<?= $informationChangeData['label']; ?>",
                                enabled: false,
                                position: "bottom",
                                yPadding: 1,
                            },
                            onMouseenter() {
                                this._model.labelEnabled = true;
                                this.chartInstance.render();
                            },
                            onMouseleave() {
                                this._model.labelEnabled = false;
                                this.chartInstance.render();
                            },
                            onClick() {
                                window.open("<?= Url::to([
                                    '/monitoring/change-registrator/default/view',
                                    'id' => $informationChangeData['id'],
                                ]);
                                    ?>");
                            }
                        },
                        <?php endforeach; ?>
                    ]
                }
            }
        });
        <?php endif; ?>

        <?php if (!empty($expensesData)): ?>
        let expensesCanvas = document.getElementById("canvas-inner-industrial-expenses-<?= $productToResource->id; ?>");
        new Chart(expensesCanvas, {
            type: 'line',
            data: {
                fill: false,
                labels: <?= Json::encode($labels); ?>,
                datasets: [{
                    label: 'Затраты',
                    data: <?= Json::encode($expensesData); ?>,
                    fill: false,
                    borderColor: "rgb(255, 0, 0)",
                    lineTension: 0.1,
                    steppedLine: false,
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        bounds: 'data',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD.MM.YYYY'
                            }
                        },
                        ticks: {
                            source: 'labels'
                        }
                    }],
                },
                annotation: {
                    drawTime: 'afterDatasetsDraw', // (default)
                    events: ['mouseenter', 'mouseleave', 'click'],
                    annotations: [
                        <?php foreach ($informationChangesData as $informationChangeData): ?>
                        {
                            drawTime: 'afterDraw',
                            type: 'line',
                            mode: 'vertical',
                            scaleID: "x-axis-0",
                            value: "<?= $informationChangeData['value']; ?>",
                            borderColor: 'blue',
                            borderWidth: 3,
                            label: {
                                backgroundColor: "blue",
                                fontSize: 8,
                                content: "<?= $informationChangeData['label']; ?>",
                                enabled: false,
                                position: "bottom",
                                yPadding: 1,
                            },
                            onMouseenter() {
                                this._model.labelEnabled = true;
                                this.chartInstance.render();
                            },
                            onMouseleave() {
                                this._model.labelEnabled = false;
                                this.chartInstance.render();
                            },
                            onClick() {
                                window.open("<?= Url::to([
                                    '/monitoring/change-registrator/default/view',
                                    'id' => $informationChangeData['id'],
                                ]);
                                    ?>");
                            }
                        },
                        <?php endforeach; ?>
                    ]
                }
            }
        });
        <?php endif; ?>
    });
</script>
