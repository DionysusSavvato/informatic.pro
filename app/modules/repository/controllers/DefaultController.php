<?php

namespace app\modules\repository\controllers;

use common\models\IndustrialFunction;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Базовый контроллер модуля
 */
class DefaultController extends Controller
{
    /**
     * Стартовая страница модуля
     *
     * `/repository`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function actionStructure(): string
    {
        return $this->render('structure');
    }

    /**
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionStructureView(int $id): string
    {
        return $this->render('view-structure', [
            'function' => IndustrialFunction::findOrThrow($id),
        ]);
    }
}