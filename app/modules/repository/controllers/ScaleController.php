<?php

namespace app\modules\repository\controllers;

use app\modules\repository\models\Scale;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для управления измерительными шкалами
 *
 * @package app\modules\repository\controllers
 */
class ScaleController extends Controller
{
    /**
     * Просмотр списка шкал
     *
     * `/repository/scale`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        $dataProvider = (new Scale())->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Создание шкалы
     *
     * `/repository/scale/create`
     *
     * @return array|string|Response
     *
     * @throws InvalidParamException
     */
    public function actionCreate()
    {
        $scale = new Scale();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $scale->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($scale);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $scale->load(Yii::$app->request->post())) {
            if ($scale->save()) {
                Yii::$app->getSession()->setFlash('success', 'Шкала успешно создана');

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $scale,
        ]);
    }

    /**
     * Просмотр и редактирование шкалы
     *
     * `/repository/scale/{id}`
     *
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     * @throws InvalidParamException
     */
    public function actionView(int $id): string
    {
        $scale = Scale::findOrThrow($id);

        if (Yii::$app->request->post()) {
            $scale->setAttributes(Yii::$app->request->post('Scale'));
            if ($scale->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $scale->refresh();
            }
        }

        return $this->render('view', [
            'scale' => $scale,
        ]);
    }

    /**
     * Удаление шкалы
     *
     * `/repository/scale/{id}/delete`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws Exception
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        $scale = Scale::findOrThrow($id);

        if ($scale->delete()) {
            Yii::$app->session->setFlash('success', "Шкала \"{$scale->name}\" успешно удалена.");
        } else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении шкалы.');
        }

        return $this->redirect(['index']);
    }
}