<?php

namespace app\modules\repository\controllers;

use common\models\Parameter;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для управления параметрами сущностей
 */
class ParameterController extends Controller
{
    /**
     * Редактирование параметра
     *
     * `/repository/parameter/{id}`
     *
     * @param int $id
     *
     * @return string|array
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        $parameter = Parameter::findOrThrow($id);

        if (Yii::$app->request->isAjax && $parameter->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($parameter);
        }

        if (Yii::$app->request->isPost) {
            $parameter->setAttributes(Yii::$app->request->post('Parameter'));
            if ($parameter->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $parameter->refresh();
            }
        }

        return $this->render('view', [
            'parameter' => $parameter,
        ]);
    }

    /**
     * Создание параметра
     *
     * `/repository/parameter/create`
     *
     * @return Response|array
     */
    public function actionCreate()
    {
        $parameter = new Parameter();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $parameter->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($parameter);
        }

        if (Yii::$app->request->isPost) {
            $parameter->setAttributes(Yii::$app->request->post('Parameter'));
            if ($parameter->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
            }
        }

        return $this->goBack();
    }

    /**
     * Удаление параметра
     *
     * `/repository/parameter/{id}/delete`
     *
     * @param int $id Идентификатор параметра
     *
     * @return Response
     *
     * @throws StaleObjectException
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        $parameter = Parameter::findOrThrow($id);

        if ($parameter->delete()) {
            Yii::$app->session->setFlash('success', "Параметр \"{$parameter->name}\" успешно удален.");
        }
        else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении параметра.');
        }

        return $this->goBack();
    }
}