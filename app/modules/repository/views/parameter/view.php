<?php

use common\models\Parameter;
use common\models\Scale;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View      $this
 * @var Parameter $parameter
 */


$this->title = $parameter->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i>Репозиторий бизнес-процессов',
        'url' => ['/repository'],
    ],
];

?>

<div class="parameter">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $this->title ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($parameter, 'name')
                    ->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($parameter, 'scale_id')
                    ->dropDownList(
                        Scale::find()->indexBy('scale_id')->select(['name'])->column()
                    )->label('Шкала') ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

