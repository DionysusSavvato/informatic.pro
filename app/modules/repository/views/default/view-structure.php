<?php

use app\widgets\structure\StructureWidget;
use common\models\IndustrialFunction;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 */

$this->title = "Структура {$function->abbreviation}";

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i>Репозиторий бизнес-процессов',
        'url' => ['/repository'],
    ],
];

?>

<?= StructureWidget::widget([
    'industrialFunction' => $function,
]) ?>