<?php

use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 */

$this->title = 'Репозиторий бизнес-процессов';
$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий бизнес-процессов',
        'url' => ['/repository'],
    ],
];

$this->registerCss($this->render('style.css'));

?>
<div class="repository">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2>
                    <?= $this->title; ?>
                </h2>
            </div>
            <div class="box-tools pull-right" style="margin-right: 20px; margin-top: 10px;">
                <a class="btn btn-app"
                   href="<?= Url::to(['/repository/scale']) ?>">
                    <i class="fa fa-arrows-v"></i> Шкалы
                </a>
                <a class="btn btn-app"
                   href="<?= Url::to(['/repository/structure']) ?>">
                    <i class="fa fa-connectdevelop"></i> Структура
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <strong>Выберите страту, с которой вы хотите работать:</strong>
                <div class="list-group">
                    <a href="<?= Url::to(['/repository/consumer']); ?>" class="list-group-item">
                        <h2 class="pull-left abbreviation">S<sub>c</sub> &nbsp;</h2>
                        <h4 class="list-group-item-heading">Потребители</h4>
                        <p class="list-group-item-text">Управление списком потребителей и их потребностями</p>
                    </a>
                    <a href="<?= Url::to(['/repository/industrial-function/default/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">S<sub>пр</sub></h2>
                        <h4 class="list-group-item-heading">Производственные процессы</h4>
                        <p class="list-group-item-text">Управление структурой производственных процессов, ресурсами,
                            продуктами</p>
                    </a>
                    <a href="<?= Url::to(['/repository/management/default/index']); ?>" class="list-group-item">
                        <h2 class="pull-left abbreviation">S<sub>уо</sub></h2>
                        <h4 class="list-group-item-heading">Менеджмент</h4>
                        <p class="list-group-item-text">Управление списком ответственных лиц и их информационными
                            потребностями</p>
                    </a>
                    <a href="<?= Url::to(['/repository/information-system/information-function']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">S<sub>и</sub> &nbsp;</h2>
                        <h4 class="list-group-item-heading">Информационная система</h4>
                        <p class="list-group-item-text">Управление списком информационных функций, ресурсов и
                            продуктов</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
