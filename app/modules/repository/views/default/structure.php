<?php

use app\assets\JsPlumbAsset;
use app\widgets\industrial_function_structure\IndustrialFunctionStructureWidget;
use common\models\IndustrialFunction;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 */

JsPlumbAsset::register($this);

$this->title = 'Структура бизнес-процессов';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i>Репозиторий бизнес-процессов',
        'url' => ['/repository'],
    ],
];
?>
<?= IndustrialFunctionStructureWidget::widget(); ?>

