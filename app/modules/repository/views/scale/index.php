<?php

use app\modules\repository\models\Scale;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var ActiveDataProvider $dataProvider */

$this->title = 'Шкалы';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-arrows-v"></i> ' . $this->title,
        'url' => ['/repository/scale'],
    ],
];
?>
<div class="scale">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2>
                    <?= $this->title ?>
                </h2>
                <?= Html::a(
                    '<i class="fa fa-plus"></i> Добавить шкалу',
                    ['/repository/scale/create'],
                    [
                        'encode' => false,
                        'class' => 'btn btn-success',
                    ]) ?>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => new Scale(),
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'name',
                            'filterInputOptions' => [
                                'name' => 'name',
                                'class' => 'form-control',
                            ],
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'scaleType.name',
                            'enableSorting' => false,
                        ],
                        [
                            'class' => ActionColumn::class,
                            'controller' => 'scale',
                            'template' => '{view}',
                            'contentOptions' => [
                                'align' => 'center',
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'box-body',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
