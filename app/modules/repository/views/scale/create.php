<?php

use app\modules\repository\models\Scale;
use common\models\ScaleType;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var View $this */
/* @var Scale $model */
/* @var $form ActiveForm */

$this->title = 'Создать шкалу';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-arrows-v"></i>Шкалы',
        'url' => ['/repository/scale'],
    ],
    [
        'label' => $this->title,
        'url' => ['/repository/scale/create'],
    ],
];

?>
<div class="admin">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $this->title ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($model, 'name')->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($model, 'type_id')->dropDownList(
                    ScaleType::find()->indexBy('scale_type_id')->select(['name'])->column()
                )->label('Тип шкалы') ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-plus"></i>  Создать',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>