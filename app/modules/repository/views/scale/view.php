<?php

use app\modules\repository\models\Scale;
use app\modules\repository\widgets\scale_values_editor\ScaleValuesEditorWidget;
use common\models\ScaleType;
use kartik\detail\DetailView;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var Scale $scale */

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-arrows-v"></i>Шкалы',
        'url' => ['/repository/scale'],
    ],
    [
        'label' => $scale->name,
        'url' => ['/repository/scale/view', 'id' => $scale->scale_id],
    ],
];

$this->title = $scale->name;
?>

<div class="scale">
    <div class="container-fluid">
        <?=
        DetailView::widget([
            'model' => $scale,
            'mode' => DetailView::MODE_VIEW,
            'panel' => [
                'heading' => $this->title,
                'type' => DetailView::TYPE_SUCCESS,
            ],
            'attributes' => [
                'name',
                [
                    'attribute' => 'type_id',
                    'type' => DetailView::INPUT_DROPDOWN_LIST,
                    'items' => ScaleType::find()->indexBy('scale_type_id')->select(['name'])->column(),
                    'label' => 'Тип шкалы',
                    'value' => $scale->scaleType->name,
                ],
            ],
            'deleteOptions' => [
                'confirm' => "Вы действительно хотите удалить шкалу \"{$scale->name}\"",
                'url' => Url::to(['/repository/scale/delete', 'id' => $scale->scale_id]),
            ],
        ]);
        ?>
    </div>
</div>

<div class="scale-values">
    <?= ScaleValuesEditorWidget::widget([
        'scale' => $scale,
    ]) ?>
</div>