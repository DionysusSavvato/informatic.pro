<?php

namespace app\modules\repository\models;


use common\models\Scale as CommonScale;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class Scale
 *
 * @package app\modules\repository\models
 */
class Scale extends CommonScale
{
    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     *
     * @throws InvalidParamException
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 10,
            ],
        ]);

        $this->setAttributes($params);

        $query->filterWhere([
            'like',
            'name',
            $this->name,
        ]);

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->with([
            'scaleType',
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributes(): array
    {
        return ArrayHelper::merge(parent::attributes(), [
            'scaleType.name',
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function attributeLabels(): array
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'scaleType.name' => 'Тип шкалы',
        ]);
    }
}