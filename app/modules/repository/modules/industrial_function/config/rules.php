<?php

return [
    'repository/industrial-function' => 'repository/industrial-function/default/index',
    'repository/industrial-function/<id:\\d+>' => 'repository/industrial-function/default/view',
    'repository/industrial-function/create' => 'repository/industrial-function/default/create',

    /** @see ResourceController */
    'repository/industrial-function/resource/<id:\\d+>' => 'repository/industrial-function/resource/view',
    'repository/industrial-function/resource/<id:\\d+>/attach/product' => 'repository/industrial-function/resource/attach-product',
    'repository/industrial-function/resource/<id:\\d+>/detach/product/<productID:\\d+>' => 'repository/industrial-function/resource/detach-product',
    'repository/industrial-function/<id:\\d+>/resource/create' => 'repository/industrial-function/resource/create',
    'repository/industrial-function/<id:\\d+>/resource/attach' => 'repository/industrial-function/resource/attach',
    'repository/industrial-function/resource/<id:\\d+>/delete' => 'repository/industrial-function/resource/delete',

    /** @see ProductController */
    'repository/industrial-function/product/<id:\\d+>' => 'repository/industrial-function/product/view',
    'repository/industrial-function/<id:\\d+>/product/create' => 'repository/industrial-function/product/create',
    'repository/industrial-function/<id:\\d+>/product/attach' => 'repository/industrial-function/product/attach',
    'repository/industrial-function/product/<id:\\d+>/delete' => 'repository/industrial-function/product/delete',
];