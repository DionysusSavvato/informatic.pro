<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use yii\web\View;

/**
 * @var View $this
 */

$this->title = $resource->material->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">F</span> Производственные функции',
        'url' => ['/repository/industrial-function'],
    ],
    [
        'label' => $resource->function->name,
        'url' => ['/repository/industrial-function/default/view', 'id' => $resource->function->entity_id],
    ],
];
?>

<?= $this->render('view/material_form', [
    'resource' => $resource,
]); ?>

<?= ParametersEditorWidget::widget(['entity' => $resource->material]); ?>

<?= $this->render('view/sources', [
    'resource' => $resource,
    'sourcesProvider' => $sourcesProvider,
    'productsAsSource' => $productsAsSource,
]); ?>