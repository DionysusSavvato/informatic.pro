<?php

use common\models\ProductToResource;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 */

$productToResource = new ProductToResource();
?>

<div class="sources">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2 class="box-title">Функции - источники ресурса</h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php
                echo GridView::widget([
                    'dataProvider' => $sourcesProvider,
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'product.function.name',
                        ],
                        [
                            'class' => ActionColumn::class,
                            'controller' => 'default',
                            'template' => '{view} {delete}',
                            'urlCreator' => function (string $action, ProductToResource $productToResource) {
                                switch ($action) {
                                    case 'view' :
                                        break;
                                    case 'delete':
                                        return Url::to([
                                            '/repository/industrial-function/resource/detach-product',
                                            'id' => $productToResource->resource_id,
                                            'productID' => $productToResource->product_id,
                                        ]);
                                }
                            },
                            'contentOptions' => [
                                'align' => 'center',
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'box-body',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="box-footer">
            <div class="container-fluid">
                <button type="button"
                        class="btn btn-default"
                        data-toggle="modal"
                        data-target="#modal-resource-sources">
                    <i class="fa fa-plus"></i> Добавить источник ресурса
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-resource-sources" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => ['/repository/industrial-function/resource/attach-product', 'id' => $resource->id],
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Добавить источник ресурса</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group">
                        <?= $form->field($productToResource, 'product_id')
                            ->dropDownList($productsAsSource)
                            ->label('Функция'); ?>
                        <?= $form->field($productToResource, 'resource_id')
                            ->hiddenInput(['value' => $resource->id])
                            ->label(false); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                    ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

