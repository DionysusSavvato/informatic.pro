<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 */

?>

<div class="material">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $resource->material->name ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($resource, 'abbreviation')
                    ->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($resource->material, 'name')
                    ->textInput(['autocomplete' => 'off']) ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
