<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use yii\web\View;

/**
 * @var View $this
 */

$this->title = $product->material->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">F</span> Производственные функции',
        'url' => ['/repository/industrial-function'],
    ],
    [
        'label' => $product->function->name,
        'url' => ['/repository/industrial-function/default/view', 'id' => $product->function->entity_id],
    ],
];
?>

<?= $this->render('view/material_form', [
    'product' => $product,
]); ?>

<?= ParametersEditorWidget::widget(['entity' => $product->material]); ?>