<?php

use common\models\IndustrialFunction;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var IndustrialFunction $model
 * @var array              $parentFunctions
 * @var array              $managers
 */

?>
<div class="industrial-function">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $this->title ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($model, 'abbreviation')
                    ->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($model, 'name')
                    ->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($model, 'parent_id')
                    ->dropDownList($parentFunctions); ?>
                <?= $form->field($model, 'manager_id')
                    ->dropDownList($managers); ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
