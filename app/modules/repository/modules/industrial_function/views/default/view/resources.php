<?php

use common\models\IndustrialFunction;
use common\models\Material;
use common\models\Resource;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var IndustrialFunction $model
 * @var ActiveDataProvider $resourcesProvider
 * @var array              $materialsForResource
 */

?>

<div class="resources">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2 class="box-title">Ресурсы</h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php
                echo GridView::widget([
                    'dataProvider' => $resourcesProvider,
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'abbreviation',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'material.name',
                        ],
                        [
                            'class' => ActionColumn::class,
                            'controller' => 'default',
                            'template' => '{view} {delete}',
                            'urlCreator' => function (string $action, Resource $resource) {
                                switch ($action) {
                                    case 'view' :
                                        return Url::to([
                                            '/repository/industrial-function/resource/view',
                                            'id' => $resource->id,
                                        ]);
                                    case 'delete':
                                        return Url::to([
                                            '/repository/industrial-function/resource/delete',
                                            'id' => $resource->id,
                                        ]);
                                }
                            },
                            'contentOptions' => [
                                'align' => 'center',
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'box-body',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="box-footer">
            <div class="container-fluid">
                <button type="button"
                        class="btn btn-default"
                        data-toggle="modal"
                        data-target="#modal-resource-add">
                    <i class="fa fa-plus"></i> Добавить ресурс
                </button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-resource-add" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Добавить ресурс</h4>
                </div>
                <div class="modal-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_r1" data-toggle="tab" aria-expanded="false">Создать
                                    ресурс</a></li>
                            <li><a href="#tab_r2" data-toggle="tab" aria-expanded="true">Указать ресурс</a></li>
                        </ul>
                        <div class="tab-content">
                            <!--Добавление ресурса-->
                            <div class="tab-pane active" id="tab_r1">
                                <?php
                                $material = new Material();
                                $resource = new Resource();
                                $form = ActiveForm::begin([
                                    'enableAjaxValidation' => true,
                                    'action' => [
                                        '/repository/industrial-function/resource/create',
                                        'id' => $model->entity_id,
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                    ],
                                ]); ?>
                                <?= $form->field($resource, 'abbreviation')
                                    ->textInput(['autocomplete' => 'off']); ?>
                                <?= $form->field($material, 'name')
                                    ->textInput(['autocomplete' => 'off']) ?>
                                <div class="form-group">
                                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                            <!--Указание ресурса-->
                            <div class="tab-pane" id="tab_r2">
                                <?php
                                $resource = new Resource();
                                $form = ActiveForm::begin([
                                    'enableAjaxValidation' => true,
                                    'action' => [
                                        '/repository/industrial-function/resource/attach',
                                        'id' => $model->entity_id,
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                    ],
                                ]); ?>
                                <?= $form->field($resource, 'abbreviation')
                                    ->textInput(['autocomplete' => 'off']); ?>
                                <?= $form->field($resource, 'material_id')
                                    ->dropDownList($materialsForResource)
                                    ->label('Название') ?>
                                <?= $form->field($resource, 'function_id')
                                    ->hiddenInput(['value' => $model->entity_id])
                                    ->label(false) ?>
                                <div class="form-group">
                                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
