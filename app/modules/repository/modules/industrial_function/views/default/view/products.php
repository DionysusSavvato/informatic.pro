<?php


use common\models\IndustrialFunction;
use common\models\Material;
use common\models\Product;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var IndustrialFunction $model
 * @var ActiveDataProvider $productsProvider
 * @var array              $materialsForProduct
 */
?>

<div class="products">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2 class="box-title">Продукты</h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php
                echo GridView::widget([
                    'dataProvider' => $productsProvider,
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'abbreviation',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'material.name',
                        ],
                        [
                            'class' => ActionColumn::class,
                            'controller' => 'default',
                            'template' => '{view} {delete}',
                            'urlCreator' => function (string $action, Product $product) {
                                switch ($action) {
                                    case 'view' :
                                        return Url::to([
                                            '/repository/industrial-function/product/view',
                                            'id' => $product->id,
                                        ]);
                                    case 'delete':
                                        return Url::to([
                                            '/repository/industrial-function/product/delete',
                                            'id' => $product->id,
                                        ]);
                                }
                            },
                            'contentOptions' => [
                                'align' => 'center',
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'box-body',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="box-footer">
            <div class="container-fluid">
                <button type="button"
                        class="btn btn-default"
                        data-toggle="modal"
                        data-target="#modal-product-add">
                    <i class="fa fa-plus"></i> Добавить продукт
                </button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-product-add" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Добавить продукт</h4>
                </div>
                <div class="modal-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_p1" data-toggle="tab" aria-expanded="false">Создать
                                    продукт</a></li>
                            <li><a href="#tab_p2" data-toggle="tab" aria-expanded="true">Указать продукт</a></li>
                        </ul>
                        <div class="tab-content">
                            <!--Добавление продукта-->
                            <div class="tab-pane active" id="tab_p1">
                                <?php
                                $material = new Material();
                                $product = new Product();
                                $form = ActiveForm::begin([
                                    'enableAjaxValidation' => true,
                                    'action' => [
                                        '/repository/industrial-function/product/create',
                                        'id' => $model->entity_id,
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                    ],
                                ]); ?>
                                <?= $form->field($product, 'abbreviation')
                                    ->textInput(['autocomplete' => 'off']); ?>
                                <?= $form->field($material, 'name')
                                    ->textInput(['autocomplete' => 'off']) ?>
                                <div class="form-group">
                                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                            <!--Указание продукта-->
                            <div class="tab-pane" id="tab_p2">
                                <?php
                                $product = new Product();
                                $form = ActiveForm::begin([
                                    'enableAjaxValidation' => true,
                                    'action' => [
                                        '/repository/industrial-function/product/attach',
                                        'id' => $model->entity_id,
                                    ],
                                    'options' => [
                                        'autocomplete' => 'off',
                                    ],
                                ]); ?>
                                <?= $form->field($product, 'abbreviation')
                                    ->textInput(['autocomplete' => 'off']); ?>
                                <?= $form->field($product, 'material_id')
                                    ->dropDownList($materialsForProduct)
                                    ->label('Название') ?>
                                <?= $form->field($product, 'function_id')
                                    ->hiddenInput(['value' => $model->entity_id])
                                    ->label(false) ?>
                                <div class="form-group">
                                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
