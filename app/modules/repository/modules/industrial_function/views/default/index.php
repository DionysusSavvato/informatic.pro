<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use common\models\IndustrialFunction;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View                 $this
 * @var IndustrialFunction[] $rootFunctions
 */

$this->title = 'Иерархия процессов';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
];
?>
<div class="box box-default">
    <div class="box-header with-border">
        <div class="container-fluid">
            <h2><?= $this->title; ?></h2>
            <a class="btn btn-success" href="<?= Url::to(['/repository/industrial-function/default/create']) ?>">
                <i class="fa fa-plus"></i> Создать производственный процесс
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div style="margin-left: 20px;">
                <?= IndustrialFunctionHierarchyWidget::widget([
                    'baseRoute' => ['/repository/industrial-function/default/view'],
                ]) ?>
            </div>

        </div>
    </div>
</div>
