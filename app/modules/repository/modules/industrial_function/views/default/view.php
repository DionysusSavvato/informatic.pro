<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\IndustrialFunction;
use yii\data\ActiveDataProvider;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $model
 * @var array              $parentFunctions
 * @var array              $managers
 * @var ActiveDataProvider $resourcesProvider
 * @var array              $materialsForResource
 * @var ActiveDataProvider $productsProvider
 * @var array              $materialsForProduct
 */

$this->title = $model->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">F</span> Производственные функции',
        'url' => ['/repository/industrial-function'],
    ],
];

?>

<?=
$this->render('view/edit-form', [
    'model' => $model,
    'parentFunctions' => $parentFunctions,
    'managers' => $managers,
])
?>

<?= ParametersEditorWidget::widget([
    'entity' => $model,
]) ?>

<?=
$this->render('view/resources', [
    'model' => $model,
    'resourcesProvider' => $resourcesProvider,
    'materialsForResource' => $materialsForResource,
]);
?>

<?=
$this->render('view/products', [
    'model' => $model,
    'productsProvider' => $productsProvider,
    'materialsForProduct' => $materialsForProduct,
])
?>



