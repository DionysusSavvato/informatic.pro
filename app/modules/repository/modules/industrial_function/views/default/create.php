<?php

use common\models\IndustrialFunction;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var IndustrialFunction $model
 */

$this->title = 'Создать производственную функцию';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">F</span> Производственные функции',
        'url' => ['/repository/industrial-function'],
    ],
];

?>

<div class="industrial-function">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $this->title ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($model, 'abbreviation')
                    ->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($model, 'name')
                    ->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($model, 'parent_id')
                    ->dropDownList($parentFunctions); ?>
                <?= $form->field($model, 'manager_id')
                    ->dropDownList($managers); ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-plus"></i>  Создать',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
