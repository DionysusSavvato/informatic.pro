<?php

namespace app\modules\repository\modules\industrial_function;

use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль для управления производственными функциями, ресурсами, продуктами
 */
class IndustrialFunctionModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}