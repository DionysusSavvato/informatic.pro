<?php

namespace app\modules\repository\modules\industrial_function\controllers;

use common\models\IndustrialFunction;
use common\models\Manager;
use common\models\Material;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Основной контроллер для управления производственными функциями
 */
class DefaultController extends Controller
{
    /**
     * Обзор иерархии производственных процессов
     *
     *  `/repository/industrial-function`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * Создание производстенной функции
     *
     * `/repository/industrial-function/create`
     *
     * @return array|string|Response
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws InvalidConfigException
     */
    public function actionCreate()
    {
        $industrialFunction = new IndustrialFunction();
        $industrialFunction->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($industrialFunction);
        }

        if (Yii::$app->request->isPost) {
            if ($industrialFunction->save()) {
                Yii::$app->session->setFlash('success', 'Производственная функция успешно создана');

                return $this->redirect(['view', 'id' => $industrialFunction->entity_id]);
            }

            Yii::$app->session->setFlash('danger', 'Производственная функция не создана');

        }

        return $this->render('create', [
            'model' => $industrialFunction,
            'parentFunctions' => ArrayHelper::merge(
                [null => null],
                IndustrialFunction::find()->select(['name'])->indexBy('entity_id')->column()
            ),
            'managers' => ArrayHelper::merge(
                [null => null],
                Manager::find()->select(['name'])->indexBy('entity_id')->column()
            ),
        ]);
    }

    /**
     * Редактирование производственной функции
     *
     * `/repository/industrial-function/{id}`
     *
     * @param int $id
     *
     * @return array|Response|string
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionView(int $id)
    {
        $industrialFunction = IndustrialFunction::findOrThrow($id);

        $industrialFunction->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($industrialFunction);
        }
        if (Yii::$app->request->isPost) {
            if ($industrialFunction->save()) {
                Yii::$app->session->setFlash('success', 'Производственная функция успешно изменена');
            }
            else {
                Yii::$app->session->setFlash('danger', 'Производственная функция не изменена');
            }
        }

        return $this->render('view', [
            'model' => $industrialFunction,
            'parentFunctions' => ArrayHelper::merge(
                [null => null],
                IndustrialFunction::find()
                    ->select(['name'])
                    ->where([
                        'not in',
                        'entity_id',
                        [$industrialFunction->entity_id],
                    ])
                    ->indexBy('entity_id')
                    ->column()
            ),
            'managers' => ArrayHelper::merge(
                [null => null],
                Manager::find()->select(['name'])->indexBy('entity_id')->column()
            ),
            'resourcesProvider' => new ActiveDataProvider([
                'query' => $industrialFunction->getResources()
                    ->with(['material'])
                    ->orderBy(['abbreviation' => SORT_ASC]),
            ]),
            'materialsForResource' => ArrayHelper::map(
                Material::find()
                    ->andWhere([
                        'not in',
                        'entity_id',
                        $industrialFunction->getResources()->select(['material_id'])->column(),
                    ])->all(),
                'entity_id',
                'name'
            ),
            'productsProvider' => new ActiveDataProvider([
                'query' => $industrialFunction->getProducts()
                    ->with(['material'])
                    ->orderBy(['abbreviation' => SORT_ASC]),
            ]),
            'materialsForProduct' => ArrayHelper::map(
                Material::find()
                    ->andWhere([
                        'not in',
                        'entity_id',
                        $industrialFunction->getProducts()->select(['material_id'])->column(),
                    ])->all(),
                'entity_id',
                'name'
            ),
        ]);
    }
}