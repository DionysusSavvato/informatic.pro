<?php

namespace app\modules\repository\modules\industrial_function\controllers;

use common\models\IndustrialFunction;
use common\models\Material;
use common\models\Product;
use common\models\ProductToResource;
use common\models\Resource;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\Exception as DbException;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для управления производственными ресурсами
 */
class ResourceController extends Controller
{
    /**
     * Просмотр и редактирование ресурса
     *
     * `/repository/industrial-function/resource/{id}`
     *
     * @param int $id
     *
     * @return array|string|Response
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        $resource = Resource::findOrThrow($id);

        $resource->setAttributes(Yii::$app->request->post('Resource'));
        $resource->material->setAttributes(Yii::$app->request->post('Material'));

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($resource->material);
        }
        if (Yii::$app->request->isPost) {
            if ($resource->material->save() && $resource->save()) {
                Yii::$app->session->setFlash('success', 'Ресурс успешно изменен');
            }
        }

        return $this->render('view', [
            'resource' => $resource,
            'sourcesProvider' => new ActiveDataProvider([
                'query' => $resource
                    ->getSourceProducts()
                    ->with([
                        'product.function',
                    ]),
            ]),
            'productsAsSource' => ArrayHelper::map(
                Product::find()
                    ->alias('p')
                    ->select(['p.id', 'f.name'])
                    ->innerJoinWith('function f', false)
                    ->where([
                        'p.material_id' => $resource->material_id,
                    ])
                    ->andWhere([
                        '<>',
                        'f.entity_id',
                        $resource->function->entity_id,
                    ])
                    ->asArray()
                    ->all(),
                'id', 'name'),
        ]);
    }

    /**
     * Указание источника - продукта другого процесса
     *
     * `/repository/industrial-function/resource/{id}/attach/product`
     *
     * @param int $id Идентификатор ресурса
     *
     * @return array|Response
     */
    public function actionAttachProduct(int $id)
    {
        $productToResource = new ProductToResource();
        $productToResource->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($productToResource);
        }
        if (Yii::$app->request->isPost) {
            $productToResource->save();
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Открепление источника ресурса - продукта другого процесса
     *
     * `/repository/industrial-function/resource/{id}/detach/product/{productID}`
     *
     * @param int $id
     * @param int $productID
     *
     * @return Response
     *
     * @throws DbException
     * @throws Throwable
     * @throws Exception
     * @throws StaleObjectException
     */
    public function actionDetachProduct(int $id, int $productID): Response
    {
        $productToResource = ProductToResource::findOne([
            'product_id' => $productID,
            'resource_id' => $id,
        ]);

        if ($productToResource !== null) {
            $productToResource->deleteOrThrow();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Создание ресурса
     *
     * `/repository/industrial-function/{id}/resource/create`
     *
     * @param int $id Идентификатор производственного процесса
     *
     * @return Response|array
     * @throws NotFoundHttpException
     */
    public function actionCreate(int $id)
    {
        $industrialFunction = IndustrialFunction::findOrThrow($id);

        $material = new Material();

        $material->setAttributes(Yii::$app->request->post('Material'));
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($industrialFunction);
        }

        if (Yii::$app->request->isPost) {
            if ($material->save()) {
                $resource = new Resource([
                    'abbreviation' => ArrayHelper::getValue(Yii::$app->request->post('Resource'), 'abbreviation'),
                    'material_id' => $material->entity_id,
                    'function_id' => $industrialFunction->entity_id,
                ]);
                if ($resource->save()) {
                    Yii::$app->session->setFlash('success', 'Ресурс успешно добавлен');
                }
            }
        }

        return $this->redirect([
            '/repository/industrial-function/default/view',
            'id' => $industrialFunction->entity_id,
        ]);
    }

    /**
     * Указание ресурса
     *
     * `/repository/industrial-function/{id}/resource/attach`
     *
     * @param int $id Идентификатор производственного процесса
     *
     * @return Response|array
     * @throws NotFoundHttpException
     */
    public function actionAttach(int $id)
    {
        $industrialFunction = IndustrialFunction::findOrThrow($id);

        $resource = new Resource();
        $resource->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($industrialFunction);
        }

        if (Yii::$app->request->isPost) {
            if ($resource->save()) {
                Yii::$app->session->setFlash('success', 'Ресурс успешно добавлен');
            }
        }

        return $this->redirect([
            '/repository/industrial-function/default/view',
            'id' => $industrialFunction->entity_id,
        ]);
    }

    /**
     * Удаление ресурса
     *
     * `repository/industrial-function/resource/{id}/delete`
     *
     * @param int $id Идентификатор ресурса
     *
     * @return Response
     *
     * @throws Throwable
     * @throws Exception
     * @throws StaleObjectException
     * @throws NotFoundHttpException
     */
    public function actionDelete(int $id): Response
    {
        $resource = Resource::findOrThrow($id);
        $industrialFunction = $resource->function;
        if ($resource->delete()) {
            Yii::$app->session->setFlash('success', 'Ресурс успешно удален');
        }

        return $this->redirect([
            '/repository/industrial-function/default/view',
            'id' => $industrialFunction->entity_id,
        ]);
    }
}