<?php

namespace app\modules\repository\modules\industrial_function\controllers;

use common\models\IndustrialFunction;
use common\models\Material;
use common\models\Product;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для работы с производственными продуктами
 */
class ProductController extends Controller
{
    /**
     * Просмотр и редактирование продукта
     *
     * `/repository/industrial-function/product/{id}`
     *
     * @param int $id
     *
     * @return array|string|Response
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        $product = Product::findOrThrow($id);
        $product->setAttributes(Yii::$app->request->post('Product'));
        $product->material->setAttributes(Yii::$app->request->post('Material'));
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($product->material);
        }
        if (Yii::$app->request->isPost) {
            if ($product->material->save() && $product->save()) {
                Yii::$app->session->setFlash('success', 'Продукт успешно изменен');
            }
        }

        return $this->render('view', [
            'product' => $product,
        ]);
    }

    /**
     * Создание продукта
     *
     * `/repository/industrial-function/{id}/product/create`
     *
     * @param int $id Идентификатор производственного процесса
     *
     * @return Response|array
     * @throws NotFoundHttpException
     */
    public function actionCreate(int $id)
    {
        $industrialFunction = IndustrialFunction::findOrThrow($id);

        $material = new Material();

        $material->setAttributes(Yii::$app->request->post('Material'));
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($industrialFunction);
        }

        if (Yii::$app->request->isPost) {
            if ($material->save()) {
                $product = new Product([
                    'abbreviation' => ArrayHelper::getValue(Yii::$app->request->post('Product'), 'abbreviation'),
                    'material_id' => $material->entity_id,
                    'function_id' => $industrialFunction->entity_id,
                ]);
                if ($product->save()) {
                    Yii::$app->session->setFlash('success', 'Продукт успешно добавлен');
                }
            }
        }

        return $this->redirect([
            '/repository/industrial-function/default/view',
            'id' => $industrialFunction->entity_id,
        ]);
    }

    /**
     * Указание продукта
     *
     * `/repository/industrial-function/{id}/product/attach`
     *
     * @param int $id Идентификатор производственного процесса
     *
     * @return Response|array
     * @throws NotFoundHttpException
     */
    public function actionAttach(int $id)
    {
        $industrialFunction = IndustrialFunction::findOrThrow($id);

        $product = new Product();
        $product->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($industrialFunction);
        }

        if (Yii::$app->request->isPost) {
            if ($product->save()) {
                Yii::$app->session->setFlash('success', 'Продукт успешно добавлен');
            }
            else {
                Yii::$app->session->setFlash('danger', 'Продукт не добавлен');
            }
        }

        return $this->redirect([
            '/repository/industrial-function/default/view',
            'id' => $industrialFunction->entity_id,
        ]);
    }

    /**
     * Удаление продукта
     *
     * `repository/industrial-function/product/{id}/delete`
     *
     * @param int $id Идентификатор продукта
     *
     * @return Response
     *
     * @throws Throwable
     * @throws StaleObjectException
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDelete(int $id): Response
    {
        $product = Product::findOrThrow($id);
        $industrialFunction = $product->function;
        if ($product->delete()) {
            Yii::$app->session->setFlash('success', 'Продукт успешно удален');
        }

        return $this->redirect([
            '/repository/industrial-function/default/view',
            'id' => $industrialFunction->entity_id,
        ]);
    }
}