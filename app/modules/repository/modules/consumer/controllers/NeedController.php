<?php

namespace app\modules\repository\modules\consumer\controllers;

use common\models\Need;
use common\models\Product;
use common\models\ProductToNeed;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Контроллер для управления потребностями
 */
class NeedController extends Controller
{
    /**
     * Просмотр и редактирование потребности
     *
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     * @throws InvalidParamException
     */
    public function actionView(int $id): string
    {
        $need = Need::findOrThrow($id);

        if (Yii::$app->request->isPost) {
            $need->setAttributes(Yii::$app->request->post('Need'));
            if ($need->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $need->refresh();
            }
        }
        return $this->render('view', [
            'need' => $need,
            'productsProvider' => new ActiveDataProvider([
                'query' => $need->getProductsToNeeds()->with(['product.material']),
            ]),
            'productsForAdding' => ArrayHelper::map(Product::find()
                ->where([
                    'not in',
                    'id',
                    $need->getProductsToNeeds()->select(['product_id']),
                ])
                ->with(['material'])
                ->all(), 'id', 'material.name'),
        ]);
    }

    /**
     * Прикрепление продукта к потребности
     *
     * `/repository/consumer/need/attach-product`
     *
     * @param int $id Идентификатор потребность
     *
     * @return Response
     */
    public function actionAttachProduct(int $id): Response
    {
        $productToNeed = new ProductToNeed();
        $productToNeed->setAttributes(Yii::$app->request->post('ProductToNeed'));
        $productToNeed->save();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Открепление продукта от потребности
     *
     * `/repository/consumer/need/detach-product`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws Throwable
     * @throws Exception
     */
    public function actionDetachProduct(int $id): Response
    {
        $productToNeed = ProductToNeed::findOrThrow($id);
        $needID = $productToNeed->need_id;
        $productToNeed->delete();

        return $this->redirect(['view', 'id' => $needID]);
    }

    /**
     * Удаление потребности
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws StaleObjectException
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        $need = Need::findOrThrow($id);
        $consumer = $need->consumer;
        $need->delete();
        Yii::$app->session->setFlash('kv-detail-success', 'Потребность успешно удалена.');

        return $this->redirect([
            '/repository/consumer/default/view',
            'id' => $consumer->entity_id,
        ]);
    }
}