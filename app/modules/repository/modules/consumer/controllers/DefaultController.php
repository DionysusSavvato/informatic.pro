<?php

namespace app\modules\repository\modules\consumer\controllers;

use common\models\Consumer;
use common\models\Need;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для управления справочником потребителей и их потребностей
 *
 * @package app\modules\repository\controllers
 */
class DefaultController extends Controller
{
    /**
     * Список потребителей
     *
     * `/repository/consumer`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Consumer::find(),
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Вывод детализированной информации по потребителю и форма редактирования
     *
     * `/repository/consumer/{id}`
     *
     * @param int $id Идентификатор потребителя
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $consumer = Consumer::findOrThrow($id);

        if (Yii::$app->request->isPost) {
            $consumer->setAttributes(Yii::$app->request->post('Consumer'));
            if ($consumer->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
            }
        }

        return $this->render('view', [
            'consumer' => $consumer,
            'needsDataProvider' => new ActiveDataProvider([
                'query' => $consumer->getNeeds(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]),
        ]);
    }

    /**
     * Добавление потребности
     *
     * `/repository/consumer/{id}/need/attach`
     *
     * @param int $id Идентификатор потребителя
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function actionAttachNeed(int $id): Response
    {
        $consumer = Consumer::findOrThrow($id);

        $need = new Need([
            'consumer_id' => $consumer->entity_id,
        ]);
        $need->setAttributes(Yii::$app->request->post('Need'));

        if ($need->save()) {
            Yii::$app->session->setFlash('kv-detail-success', 'Потребность успешно добавлена.');
        }
        else {
            Yii::$app->session->setFlash('kv-detail-danger', 'Потребность не добавлена.');
        }

        return $this->redirect(['view', 'id' => $consumer->entity_id]);
    }

    /**
     * Открепление потребности
     *
     * `/repository/consumer/{id}/need/{needID}/detach`
     *
     * @param int $id     Идентификатор потребителя
     * @param int $needID Идентификатор потребности
     *
     * @return Response
     *
     * @throws Exception
     * @throws Throwable
     */
    public function actionDetachNeed(int $id, int $needID): Response
    {
        $consumer = Consumer::findOrThrow($id);
        $need = $consumer->getNeeds()->where(['entity_id' => $needID])->one();
        if ($need !== null && $need->delete()) {
            Yii::$app->session->setFlash('kv-detail-success', 'Потребность успешно удалена.');
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Создание потребителя
     *
     * `/repository/consumer/create`
     *
     * @return string|array
     *
     * @throws InvalidParamException
     */
    public function actionCreate()
    {
        $consumer = new Consumer();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $consumer->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($consumer);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $consumer->load(Yii::$app->request->post())) {
            if ($consumer->save()) {
                Yii::$app->session->setFlash('success', 'Потребитель успешно создан');

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $consumer,
        ]);
    }

    /**
     * Удаление потребителя
     *
     * `/repository/consumer/{id}/delete`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws Throwable
     * @throws Exception
     */
    public function actionDelete(int $id): Response
    {
        $consumer = Consumer::findOrThrow($id);
        if ($consumer->delete()) {
            Yii::$app->session->setFlash('success', "Потребитель \"{$consumer->name}\" успешно удален.");
        }
        else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении потребителя.');
        }

        return $this->redirect(['index']);
    }
}