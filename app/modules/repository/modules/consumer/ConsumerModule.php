<?php

namespace app\modules\repository\modules\consumer;


use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль для управления клиентами
 */
class ConsumerModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}