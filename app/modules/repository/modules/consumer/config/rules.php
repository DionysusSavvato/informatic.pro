<?php

return [
    'repository/consumer' => 'repository/consumer/default/index',
    'repository/consumer/create' => 'repository/consumer/default/create',
    'repository/consumer/<id:\\d+>' => 'repository/consumer/default/view',
    'repository/consumer/<id:\\d+>/need/attach' => 'repository/consumer/default/attach-need',
    'repository/consumer/<id:\\d+>/need/<needID:\\d+>/detach' => 'repository/consumer/default/detach-need',
    'repository/consumer/<id:\\d+>/delete' => 'repository/consumer/default/delete',

    'repository/consumer/need/<id:\\d+>' => 'repository/consumer/need/view',
    'repository/consumer/need/<id:\\d+>/attach/product' => 'repository/consumer/need/attach-product',
    'repository/consumer/need/detach/product/<id:\\d+>' => 'repository/consumer/need/detach-product',
    'repository/consumer/need/<id:\\d+>/delete' => 'repository/consumer/need/delete',
];