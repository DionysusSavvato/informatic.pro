<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\Consumer;
use common\models\Need;
use kartik\detail\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var Consumer           $consumer
 * @var array              $needsForSelecting
 * @var ActiveDataProvider $needsDataProvider
 */

$this->title = $consumer->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">S<sub>c</sub></span> Потребители',
        'url' => ['/repository/consumer'],
    ],
    [
        'label' => $this->title,
        'url' => ['/repository/consumer/default/view', 'id' => $consumer->entity_id],
    ],
];
$need = new Need();
?>
<div class="consumer">
    <!--Редактирование потребителя-->
    <?=
    DetailView::widget([
        'model' => $consumer,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'abbreviation',
            'name',
        ],
        'deleteOptions' => [
            'confirm' => "Вы действительно хотите удалить потребителя \"{$consumer->name}\"",
            'url' => Url::to(['/repository/consumer/default/delete', 'id' => $consumer->entity_id]),
        ],
    ]);
    ?>

    <!--Потребности потребителя-->
    <div class="consumer-needs">
        <div class="box">
            <div class="box-header with-border">
                <div class="container-fluid">
                    <h2 class="box-title">Потребности</h2>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $needsDataProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'abbreviation',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'name',
                            ],
                            [
                                'class' => ActionColumn::class,
                                'controller' => 'default',
                                'template' => '{view} {delete}',
                                'urlCreator' => function (string $action, Need $model) use ($consumer) {
                                    switch ($action) {
                                        case 'view' :
                                            return Url::to([
                                                '/repository/consumer/need/view',
                                                'id' => $model->entity_id,
                                            ]);
                                        case 'delete':
                                            return Url::to([
                                                '/repository/consumer/default/detach-need',
                                                'id' => $consumer->entity_id,
                                                'needID' => $model->entity_id,
                                            ]);
                                    }
                                },
                                'contentOptions' => [
                                    'align' => 'center',
                                ],
                            ],
                        ],
                        'options' => [
                            'class' => 'box-body',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <button type="button"
                            class="btn btn-default"
                            data-toggle="modal"
                            data-target="#modal-need">
                        <i class="fa fa-plus"></i> Добавить потребность
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!--Параметры потребителя-->
    <?= ParametersEditorWidget::widget([
        'entity' => $consumer,
    ]) ?>

</div>
<div class="modal fade" id="modal-need" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => ['/repository/consumer/default/attach-need', 'id' => $consumer->entity_id],
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Добавить потребность</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group">
                        <?= $form->field($need, 'abbreviation')->textInput(['autocomplete' => 'off']) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($need, 'name')->textInput(['autocomplete' => 'off']) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                    ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>