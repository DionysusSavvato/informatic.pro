<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\Need;
use common\models\Product;
use common\models\ProductToNeed;
use kartik\detail\DetailView;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View      $this
 * @var Need      $need
 * @var Product[] $productsForAdding
 *
 * '/repository/consumer/need/attach-product'
 */

$this->title = $need->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">S<sub>c</sub></span> Потребители',
        'url' => ['/repository/consumer'],
    ],
    [
        'label' => $need->consumer->name,
        'url' => ['/repository/consumer/default/view', 'id' => $need->consumer->entity_id],
    ],
];

$productToNeed = new ProductToNeed();
?>

<div class="need">
    <?=
    DetailView::widget([
        'model' => $need,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'abbreviation',
            'name',
            [
                'label' => 'Потребитель',
                'format' => 'raw',
                'value' => Html::a(
                    $need->consumer->name,
                    Url::toRoute([
                        '/repository/consumer/default/view',
                        'id' => $need->consumer->entity_id,
                    ]),
                    [
                        'class' => 'btn btn-default',
                        'target' => '_blank',
                    ]
                ),
                'displayOnly' => true,
            ],
        ],
        'deleteOptions' => [
            'confirm' => "Вы действительно хотите удалить потребность \"{$need->name}\"",
            'url' => Url::to([
                '/repository/consumer/need/delete',
                'id' => $need->entity_id,
            ]),
        ],
    ]);
    ?>
    <?= ParametersEditorWidget::widget([
        'entity' => $need,
    ]) ?>

    <div class="products">
        <div class="box">
            <div class="box-header with-border">
                <div class="container-fluid">
                    <h2 class="box-title">Продукты, направленные на данную потребность</h2>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $productsProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'product.abbreviation',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'product.material.name',
                            ],
                            [
                                'class' => ActionColumn::class,
                                'controller' => 'default',
                                'template' => '{view} {delete}',
                                'urlCreator' => function (string $action, ProductToNeed $model) {
                                    switch ($action) {
                                        case 'view' :
                                            return Url::to([
                                                '/repository/industrial-function/product/view',
                                                'id' => $model->product_id,
                                            ]);

                                        case 'delete':
                                            return Url::to([
                                                '/repository/consumer/need/detach-product',
                                                'id' => $model->id,
                                            ]);
                                    }
                                },
                                'contentOptions' => [
                                    'align' => 'center',
                                ],
                            ],
                        ],
                        'options' => [
                            'class' => 'box-body',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <button type="button"
                            class="btn btn-default"
                            data-toggle="modal"
                            data-target="#modal-product">
                        <i class="fa fa-plus"></i> Добавить продукт
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-product" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => ['/repository/consumer/need/attach-product', 'id' => $need->entity_id],
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Добавить продукт</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group">
                        <?= $form->field($productToNeed, 'product_id')
                            ->dropDownList($productsForAdding); ?>
                        <?= $form->field($productToNeed, 'need_id')
                            ->hiddenInput(['value' => $need->entity_id])
                            ->label(false); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                    ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
