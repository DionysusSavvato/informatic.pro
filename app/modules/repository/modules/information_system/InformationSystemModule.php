<?php

namespace app\modules\repository\modules\information_system;

use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль управления моделью информационной системы
 */
class InformationSystemModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}