<?php

namespace app\modules\repository\modules\information_system\controllers;

use common\models\InformationResource;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для управления ресурсами информационной системы
 */
class InformationResourceController extends Controller
{
    /**
     * Просмотр и редактирование информационного ресурса
     *
     * `/repository/information-system/information-resource/{id}`
     *
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $informationResource = InformationResource::findOrThrow($id);
        if (Yii::$app->request->isPost) {
            $informationResource->setAttributes(Yii::$app->request->post('InformationResource'));
            if ($informationResource->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $informationResource->refresh();
            }
        }

        return $this->render('view', [
            'informationResource' => $informationResource,

        ]);
    }

    /**
     * @return array|Response
     */
    public function actionCreate()
    {
        $informationResource = new InformationResource();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $informationResource->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($informationResource);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $informationResource->load(Yii::$app->request->post())) {
            if ($informationResource->save()) {
                Yii::$app->getSession()->setFlash('success', 'Информационная функция успешно создана');
            }
            else {
                Yii::$app->getSession()->setFlash('error', 'Ошибка');
            }
        }

        return $this->goBack();
    }

    /**
     * Удаление информацонного ресурса
     *
     * `/repository/information-system/information-resource/{id}/delete`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws Exception
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        $informationResource = InformationResource::findOrThrow($id);
        $informationFunction = $informationResource->informationFunction;
        if ($informationResource->delete()) {
            Yii::$app->session->setFlash('success',
                "Информационный продукт \"{$informationResource->name}\" успешно удален.");
        }
        else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении информационного продукта.');
        }

        return $this->redirect([
            '/repository/information-system/default/view',
            'id' => $informationFunction->entity_id,
        ]);
    }
}