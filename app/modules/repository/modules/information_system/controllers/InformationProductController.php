<?php

namespace app\modules\repository\modules\information_system\controllers;

use common\models\InformationProduct;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для управления информационными продуктами
 */
class InformationProductController extends Controller
{
    /**
     * Просмотр и редактирование информационного продукта
     *
     * `/repository/information-system/information-product/{id}`
     *
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $informationProduct = InformationProduct::findOrThrow($id);
        if (Yii::$app->request->isPost) {
            $informationProduct->setAttributes(Yii::$app->request->post('InformationProduct'));
            if ($informationProduct->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $informationProduct->refresh();
            }
        }

        return $this->render('view', [
            'informationProduct' => $informationProduct,

        ]);
    }

    /**
     * @return array|Response
     */
    public function actionCreate()
    {
        $informationProduct = new InformationProduct();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $informationProduct->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($informationProduct);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $informationProduct->load(Yii::$app->request->post())) {
            if ($informationProduct->save()) {
                Yii::$app->getSession()->setFlash('success', 'Информационная функция успешно создана');
            }
        }

        return $this->goBack();
    }

    /**
     * Удаление информацонного продукта
     *
     * `/repository/information-system/information-product/{id}/delete`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws Exception
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        $informationProduct = InformationProduct::findOrThrow($id);
        $informationFunction = $informationProduct->informationFunction;
        if ($informationProduct->delete()) {
            Yii::$app->session->setFlash('success',
                "Информационный продукт \"{$informationProduct->name}\" успешно удален.");
        }
        else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении информационного продукта.');
        }

        return $this->redirect([
            '/repository/information-system/default/view',
            'id' => $informationFunction->entity_id,
        ]);
    }
}