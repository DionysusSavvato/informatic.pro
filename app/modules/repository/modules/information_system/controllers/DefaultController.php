<?php

namespace app\modules\repository\modules\information_system\controllers;

use common\models\InformationFunction;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер управления информационными функциями
 */
class DefaultController extends Controller
{
    /**
     * Список информационных функций
     *
     * `/repository/information-function`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        $dataProvider = new ActiveDataProvider([
            'query' => InformationFunction::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Создание информационной функции
     *
     * `/repository/information-system/information-function/create`
     *
     * @return array|string|Response
     *
     * @throws InvalidParamException
     */
    public function actionCreate()
    {
        $informationFunction = new InformationFunction();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $informationFunction->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($informationFunction);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $informationFunction->load(Yii::$app->request->post())) {
            if ($informationFunction->save()) {
                Yii::$app->session->setFlash('success', 'Информационная функция успешно создана');

                return $this->redirect(['view', 'id' => $informationFunction->entity_id]);
            }
        }

        return $this->render('create', [
            'model' => $informationFunction,
        ]);
    }

    /**
     * Детализированное представление и редактирование информационной функции
     *
     * `/repository/information-system/information-function/{id}`
     *
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $informationFunction = InformationFunction::findOrThrow($id);
        if (Yii::$app->request->isPost) {
            $informationFunction->setAttributes(Yii::$app->request->post('InformationFunction'));
            if ($informationFunction->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
            }
        }

        return $this->render('view', [
            'informationFunction' => $informationFunction,
            'informationResourcesProvider' => new ActiveDataProvider([
                'query' => $informationFunction->getInformationResources(),
            ]),
            'informationProductsProvider' => new ActiveDataProvider([
                'query' => $informationFunction->getInformationProducts(),
            ]),
        ]);
    }
}