<?php

use common\models\InformationFunction;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View                $this
 * @var InformationFunction $model
 */

$this->title = 'Создать информационную функцию';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">S<sub>и</sub></span> Информационная система',
        'url' => ['/repository/information-system/information-function'],
    ],
];
?>
<div class="management">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $this->title ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($model, 'abbreviation')->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($model, 'name')->textInput(['autocomplete' => 'off']) ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-plus"></i>  Создать',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>