<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\InformationFunction;
use common\models\InformationProduct;
use common\models\InformationResource;
use kartik\detail\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View                $this
 * @var InformationFunction $informationFunction
 * @var ActiveDataProvider  $informationResourcesProvider
 * @var ActiveDataProvider  $informationProductsProvider
 */

$this->title = $informationFunction->name;

Yii::$app->user->setReturnUrl(Yii::$app->request->absoluteUrl);

$informationProduct = new InformationProduct();
$informationResource = new InformationResource();

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">S<sub>и</sub></span> Информационные функции',
        'url' => ['/repository/information-system/default/index'],
    ],
];

?>

<div class="information-function">
    <?=
    /** @noinspection PhpUnhandledExceptionInspection */
    DetailView::widget([
        'model' => $informationFunction,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'abbreviation',
            'name',
        ],
        'deleteOptions' => [
            'confirm' => "Вы действительно хотите удалить информационную функцию \"{$informationFunction->name}\"",
            'url' => Url::to([
                '/repository/information-system/default/delete',
                'id' => $informationFunction->entity_id,
            ]),
        ],
    ]);
    ?>

    <div class="information-resources">
        <div class="box">
            <div class="box-header with-border">
                <div class="container-fluid">
                    <h2 class="box-title">Исходные данные</h2>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?=
                    /** @noinspection PhpUnhandledExceptionInspection */
                    GridView::widget([
                        'dataProvider' => $informationResourcesProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'abbreviation',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'name',
                            ],
                            [
                                'class' => ActionColumn::class,
                                'controller' => 'default',
                                'template' => '{view}',
                                'urlCreator' => function (string $action, InformationResource $model) {
                                    switch ($action) {
                                        case 'view' :
                                            return Url::to([
                                                '/repository/information-system/information-resource/view',
                                                'id' => $model->entity_id,
                                            ]);
                                    }
                                },
                                'contentOptions' => [
                                    'align' => 'center',
                                ],
                            ],
                        ],
                        'options' => [
                            'class' => 'box-body',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <button type="button"
                            class="btn btn-default"
                            data-toggle="modal"
                            data-target="#modal-information-resource">
                        <i class="fa fa-plus"></i> Добавить исходные данные
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="information-products">
        <div class="box">
            <div class="box-header with-border">
                <div class="container-fluid">
                    <h2 class="box-title">Информационные продукты</h2>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?=
                    /** @noinspection PhpUnhandledExceptionInspection */
                    GridView::widget([
                        'dataProvider' => $informationProductsProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'abbreviation',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'name',
                            ],
                            [
                                'class' => ActionColumn::class,
                                'controller' => 'default',
                                'template' => '{view}',
                                'urlCreator' => function (string $action, InformationProduct $model) {
                                    switch ($action) {
                                        case 'view' :
                                            return Url::to([
                                                '/repository/information-system/information-product/view',
                                                'id' => $model->entity_id,
                                            ]);
                                    }
                                },
                                'contentOptions' => [
                                    'align' => 'center',
                                ],
                            ],
                        ],
                        'options' => [
                            'class' => 'box-body',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <button type="button"
                            class="btn btn-default"
                            data-toggle="modal"
                            data-target="#modal-information-product">
                        <i class="fa fa-plus"></i> Добавить информационный продукт
                    </button>
                </div>
            </div>
        </div>
    </div>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    ParametersEditorWidget::widget([
        'entity' => $informationFunction,
    ]) ?>
</div>


<div class="modal fade" id="modal-information-resource" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => [
                    '/repository/information-system/information-resource/create',
                ],
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Добавить исходные данные</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?= $form->field($informationResource, 'abbreviation')
                        ->textInput(['autocomplete' => 'off']) ?>
                    <?= $form->field($informationResource, 'name')
                        ->textInput(['autocomplete' => 'off']) ?>
                    <?= $form->field($informationResource, 'function_id')
                        ->hiddenInput(['value' => $informationFunction->getEntityID()])
                        ->label(false) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                    ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-information-product" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => [
                    '/repository/information-system/information-product/create',
                ],
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Добавить информационный продукт</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?= $form->field($informationProduct, 'abbreviation')
                        ->textInput(['autocomplete' => 'off']) ?>

                    <?= $form->field($informationProduct, 'name')
                        ->textInput(['autocomplete' => 'off']) ?>
                    <?= $form->field($informationProduct, 'function_id')
                        ->hiddenInput(['value' => $informationFunction->getEntityID()])
                        ->label(false) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                    ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>