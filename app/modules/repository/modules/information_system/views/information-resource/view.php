<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\InformationResource;
use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View                $this
 * @var InformationResource $informationResource
 */

$this->title = $informationResource->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">S<sub>и</sub></span> Информационные функции',
        'url' => ['/repository/information-system/default/index'],
    ],
    [
        'label' => $informationResource->informationFunction->name,
        'url' => [
            '/repository/information-system/default/view',
            'id' => $informationResource->informationFunction->entity_id,
        ],
    ],
];
?>

<div class="information-product">
    <?=
    /** @noinspection PhpUnhandledExceptionInspection */
    DetailView::widget([
        'model' => $informationResource,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'abbreviation',
            'name',
            [
                'label' => 'Информационная функция',
                'format' => 'raw',
                'value' => Html::a(
                    $informationResource->informationFunction->name,
                    Url::toRoute([
                        '/repository/information-system/default/view',
                        'id' => $informationResource->informationFunction->entity_id,
                    ]),
                    [
                        'class' => 'btn btn-default',
                        'target' => '_blank',
                    ]
                ),
                'displayOnly' => true,
            ],
        ],
        'deleteOptions' => [
            'confirm' => "Вы действительно хотите удалить исходные данные \"{$informationResource->name}\"",
            'url' => Url::to([
                '/repository/information-system/information-resource/delete',
                'id' => $informationResource->entity_id,
            ]),
        ],
    ]);
    ?>
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    ParametersEditorWidget::widget([
        'entity' => $informationResource,
    ]) ?>
</div>
