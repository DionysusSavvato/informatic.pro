<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\InformationProduct;
use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var InformationProduct $informationProduct
 */

$this->title = $informationProduct->name;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-cogs"></i> Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => '<span class="abbreviation-mini">S<sub>и</sub></span> Информационные функции',
        'url' => ['/repository/information-system/default/index'],
    ],
    [
        'label' => $informationProduct->informationFunction->name,
        'url' => [
            '/repository/information-system/default/view',
            'id' => $informationProduct->informationFunction->entity_id,
        ],
    ],
];
?>

<div class="information-product">
    <?=
    DetailView::widget([
        'model' => $informationProduct,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'abbreviation',
            'name',
            [
                'label' => 'Информационная функция',
                'format' => 'raw',
                'value' => Html::a(
                    $informationProduct->informationFunction->name,
                    Url::toRoute([
                        '/repository/information-system/default/view',
                        'id' => $informationProduct->informationFunction->entity_id,
                    ]),
                    [
                        'class' => 'btn btn-default',
                        'target' => '_blank',
                    ]
                ),
                'displayOnly' => true,
            ],
        ],
        'deleteOptions' => [
            'confirm' => "Вы действительно хотите удалить информационный продукт \"{$informationProduct->name}\"",
            'url' => Url::to([
                '/repository/information-system/information-product/delete',
                'id' => $informationProduct->entity_id,
            ]),
        ],
    ]);
    ?>
    <?= ParametersEditorWidget::widget([
        'entity' => $informationProduct,
    ]) ?>
</div>
