<?php

return [
    'repository/information-system/information-function' => 'repository/information-system/default/index',
    'repository/information-system/information-function/create' => 'repository/information-system/default/create',
    'repository/information-system/information-function/<id:\\d+>' => 'repository/information-system/default/view',

    'repository/information-system/information-product/create' => 'repository/information-system/information-product/create',
    'repository/information-system/information-product/<id:\\d+>' => 'repository/information-system/information-product/view',
    'repository/information-system/information-product/<id:\\d+>/delete' => 'repository/information-system/information-product/delete',

    'repository/information-system/information-resource/create' => 'repository/information-system/information-resource/create',
    'repository/information-system/information-resource/<id:\\d+>' => 'repository/information-system/information-resource/view',
    'repository/information-system/information-resource/<id:\\d+>/delete' => 'repository/information-system/information-resource/delete',
];