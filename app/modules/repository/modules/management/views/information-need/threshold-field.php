<?php

use common\models\InformationProductToInformationNeed;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View                                $this
 * @var InformationProductToInformationNeed $informationProductToInformationNeed
 */

?>
<style>
    .input {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
        justify-content: left;
    }

    .input > * {
        margin-left: 5px;
        margin-right: 5px;
    }

    .input :first-child {
        width: 100%;
    }
</style>

<?php $form = ActiveForm::begin([
    'action' => [
        '/repository/management/information-need/change-threshold',
        'id' => $informationProductToInformationNeed->id,
    ],
    'options' => [
        'autocomplete' => 'off',
    ],
]); ?>
<div class="input">
    <?= $form->field($informationProductToInformationNeed, 'effect_threshold_value')
        ->input('number', [
            'step' => 0.01,
            'min' => 0,
            'max' => 1,
        ])
        ->label(false) ?>
    <?= Html::submitButton('<i class="fa fa-floppy-o"></i>',
        ['encode' => false, 'class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
