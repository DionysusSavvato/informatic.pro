<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\InformationNeed;
use common\models\InformationProductToInformationNeed;
use kartik\detail\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var InformationNeed    $informationNeed
 * @var ActiveDataProvider $informationProductsProvider
 * @var array              $informationProductsForAdding
 */

$this->title = $informationNeed->name;

$this->params['breadcrumbs'] = [
    [
        'label' => 'Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => 'Менеджмент',
        'url' => ['/repository/management'],
    ],
    [
        'label' => $informationNeed->manager->name,
        'url' => ['/repository/management/default/view', 'id' => $informationNeed->manager->entity_id],
    ],
];

$infNeedToInfProduct = new InformationProductToInformationNeed();
?>
<div class="management">
    <?=
    /** @noinspection PhpUnhandledExceptionInspection */
    DetailView::widget([
        'model' => $informationNeed,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'abbreviation',
            'name',
        ],
        'deleteOptions' => [
            'confirm' => "Вы действительно хотите удалить информационную потребность \"{$informationNeed->name}\"",
            'url' => Url::to([
                '/repository/management/information-need/delete',
                'id' => $informationNeed->entity_id,
            ]),
        ],
    ]);
    ?>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    ParametersEditorWidget::widget([
        'entity' => $informationNeed,
    ]) ?>

    <div class="information-product">
        <div class="box">
            <div class="box-header with-border">
                <div class="container-fluid">
                    <h2 class="box-title">Информационные продукты, направленные на удовлетворение потребности</h2>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?= /** @noinspection PhpUnhandledExceptionInspection */
                    GridView::widget([
                        'dataProvider' => $informationProductsProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'informationProduct.abbreviation',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'informationProduct.name',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'effect_threshold_value',
                                'enableSorting' => false,
                                'content' => function (InformationProductToInformationNeed $model) {
                                    return $this->render('threshold-field', [
                                        'informationProductToInformationNeed' => $model,
                                    ]);
                                },
                            ],
                            [
                                'class' => ActionColumn::class,
                                'controller' => 'default',
                                'template' => '{view} {delete}',
                                'urlCreator' => function (
                                    string $action,
                                    InformationProductToInformationNeed $model
                                ) use (
                                    $informationNeed
                                ) {
                                    switch ($action) {
                                        case 'view':
                                            return Url::to([
                                                '/repository/information-system/information-product/view',
                                                'id' => $model->informationProduct->entity_id,
                                            ]);
                                        case 'delete':
                                            return Url::to([
                                                '/repository/management/information-need/detach-product',
                                                'id' => $informationNeed->entity_id,
                                                'informationProductID' => $model->informationProduct->entity_id,
                                            ]);
                                    }
                                },
                                'contentOptions' => [
                                    'align' => 'center',
                                ],
                            ],
                        ],
                        'options' => [
                            'class' => 'box-body',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <button type="button"
                            class="btn btn-default"
                            data-toggle="modal"
                            data-target="#modal-information-product">
                        <i class="fa fa-plus"></i> Привязать информационный продукт
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-information-product" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => [
                    '/repository/management/information-need/attach-product',
                    'id' => $informationNeed->entity_id,
                ],
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Привязать информационный продукт</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?= $form->field($infNeedToInfProduct, 'information_product_id')
                        ->dropDownList($informationProductsForAdding) ?>
                    <?= $form->field($infNeedToInfProduct, 'effect_threshold_value')
                        ->input('number', [
                            'step' => 0.01,
                            'min' => 0,
                            'max' => 1,
                            'value' => InformationProductToInformationNeed::DEFAULT_THRESHOLD_VALUE,
                        ]) ?>
                    <?= $form->field($infNeedToInfProduct, 'information_need_id')
                        ->hiddenInput(['value' => $informationNeed->entity_id])
                        ->label(false) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                    ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
