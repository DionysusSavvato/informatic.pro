<?php

use common\models\Manager;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View    $this
 * @var Manager $model
 */

$this->title = 'Создать менеджера';

$this->params['breadcrumbs'] = [
    [
        'label' => 'Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => 'Менеджмент',
        'url' => ['/repository/management'],
    ],
    [
        'label' => $this->title,
        'url' => ['/repository/management/default/create'],
    ],
];
?>
<div class="management">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $this->title ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($model, 'abbreviation')->textInput(['autocomplete' => 'off']) ?>
                <?= $form->field($model, 'name')->textInput(['autocomplete' => 'off']) ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-plus"></i>  Создать',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>