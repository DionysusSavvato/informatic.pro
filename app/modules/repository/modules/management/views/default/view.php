<?php

use app\modules\repository\widgets\parameters_editor\ParametersEditorWidget;
use common\models\IndustrialFunction;
use common\models\InformationNeed;
use common\models\Manager;
use kartik\detail\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var Manager            $manager
 * @var ActiveDataProvider $informationNeedsDataProvider
 * @var ActiveDataProvider $industrialFunctionsActiveDataProvider
 */

$this->title = $manager->name;

$informationNeed = new InformationNeed();
Yii::$app->user->setReturnUrl(Yii::$app->request->absoluteUrl);

$this->params['breadcrumbs'] = [
    [
        'label' => 'Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => 'Менеджмент',
        'url' => ['/repository/management'],
    ],
    [
        'label' => $this->title,
        'url' => ['/repository/management/default/view', 'id' => $manager->entity_id],
    ],
];
?>
<div class="management">
    <?=
    DetailView::widget([
        'model' => $manager,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'abbreviation',
            'name',
        ],
        'deleteOptions' => [
            'confirm' => "Вы действительно хотите удалить ответственное лицо \"{$manager->name}\"",
            'url' => Url::to(['/repository/management/default/delete', 'id' => $manager->entity_id]),
        ],
    ]);
    ?>

    <?= ParametersEditorWidget::widget([
        'entity' => $manager,
    ]) ?>

    <div class="industrial-functions">
        <div class="box">
            <div class="box-header with-border">
                <div class="container-fluid">
                    <h2 class="box-title">Подконтрольные производственные функции</h2>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $industrialFunctionsActiveDataProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'abbreviation',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'name',
                            ],
                            [
                                'class' => ActionColumn::class,
                                'controller' => 'default',
                                'template' => '{view}',
                                'urlCreator' => function (string $action, IndustrialFunction $model) {
                                    switch ($action) {
                                        case 'view' :
                                            return Url::to([
                                                '/repository/industrial-function/default/index',
                                                'currentFunctionID' => $model->entity_id,
                                            ]);
                                    }
                                },
                                'contentOptions' => [
                                    'align' => 'center',
                                ],
                            ],
                        ],
                        'options' => [
                            'class' => 'box-body',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="information-need">
        <div class="box">
            <div class="box-header with-border">
                <div class="container-fluid">
                    <h2 class="box-title">Информационные потребности</h2>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $informationNeedsDataProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'abbreviation',
                            ],
                            [
                                'class' => DataColumn::class,
                                'attribute' => 'name',
                            ],
                            [
                                'class' => ActionColumn::class,
                                'controller' => 'default',
                                'template' => '{view} {delete}',
                                'urlCreator' => function (string $action, InformationNeed $model) {
                                    switch ($action) {
                                        case 'view':
                                            return Url::to([
                                                '/repository/management/information-need/view',
                                                'id' => $model->entity_id,
                                            ]);
                                        case 'delete':
                                            return Url::to([
                                                '/repository/management/information-need/delete',
                                                'id' => $model->entity_id,
                                            ]);
                                    }
                                },
                                'contentOptions' => [
                                    'align' => 'center',
                                ],
                            ],
                        ],
                        'options' => [
                            'class' => 'box-body',
                        ],
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="box-footer">
                <div class="container-fluid">
                    <button type="button"
                            class="btn btn-default"
                            data-toggle="modal"
                            data-target="#modal-information-need">
                        <i class="fa fa-plus"></i> Добавить информационную потребность
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-information-need" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => [
                    '/repository/management/information-need/create',
                ],
                'options' => [
                    'autocomplete' => 'off',
                ],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Добавить информационную потребность</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?= $form->field($informationNeed, 'abbreviation')
                        ->textInput(['autocomplete' => 'off']) ?>

                    <?= $form->field($informationNeed, 'name')
                        ->textInput(['autocomplete' => 'off']) ?>
                    <?= $form->field($informationNeed, 'manager_id')
                        ->hiddenInput(['value' => $manager->getEntityID()])
                        ->label(false) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                    ['encode' => false, 'class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
