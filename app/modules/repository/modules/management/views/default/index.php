<?php

use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View               $this
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Менеджмент';

$this->params['breadcrumbs'] = [
    [
        'label' => 'Репозиторий',
        'url' => ['/repository'],
    ],
    [
        'label' => $this->title,
        'url' => ['/repository/management'],
    ],
];
?>

<div class="management">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2>
                    <?= $this->title ?>
                </h2>
                <?= Html::a(
                    '<i class="fa fa-plus"></i> Добавить класс менеджмента',
                    ['/repository/management/create'],
                    [
                        'encode' => false,
                        'class' => 'btn btn-success',
                    ]) ?>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'abbreviation',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'name',
                        ],
                        [
                            'class' => ActionColumn::class,
                            'controller' => 'default',
                            'template' => '{view}',
                            'contentOptions' => [
                                'align' => 'center',
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'box-body',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
