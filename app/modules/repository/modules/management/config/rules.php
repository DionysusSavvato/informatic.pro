<?php

return [
    'repository/management' => 'repository/management/default/index',
    'repository/management/create' => 'repository/management/default/create',
    'repository/management/<id:\\d+>' => 'repository/management/default/view',
    'repository/management/<id:\\d+>/delete' => 'repository/management/default/delete',

    'repository/management/information-need/<id:\\d+>' => 'repository/management/information-need/view',
    'repository/management/information-need/create' => 'repository/management/information-need/create',
    'repository/management/information-need/<id:\\d+>/delete' => 'repository/management/information-need/delete',
    'repository/management/information-need/<id:\\d+>/attach-product' => 'repository/management/information-need/attach-product',
    'repository/management/information-need/<id:\\d+>/detach-product/<informationProductID:\\d+>' => 'repository/management/information-need/detach-product',
    'repository/management/information-need/change-threshold/<id:\\d+>' => 'repository/management/information-need/change-threshold',
];
