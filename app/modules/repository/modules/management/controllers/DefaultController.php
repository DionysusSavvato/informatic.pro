<?php

namespace app\modules\repository\modules\management\controllers;

use common\models\Manager;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Базовый контроллер для работы с моделью менеджмента
 */
class DefaultController extends Controller
{
    /**
     * Список управляющих (менеджмент)
     *
     * `/repository/management`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Manager::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Создание менеджера
     *
     * `/repository/management/create`
     *
     * @return array|string|Response
     *
     * @throws InvalidParamException
     */
    public function actionCreate()
    {
        $manager = new Manager();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $manager->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($manager);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $manager->load(Yii::$app->request->post())) {
            if ($manager->save()) {
                Yii::$app->session->setFlash('success', 'Ответственное лицо успешно создано');

                return $this->redirect(['view', 'id' => $manager->entity_id]);
            }
        }

        return $this->render('create', [
            'model' => $manager,
        ]);
    }

    /**
     * Менеджер. Просмотр и редактирование
     *
     * `/repository/management/{id}`
     *
     * @param int $id - Идентификатор менеджера
     *
     * @return string
     *
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $manager = Manager::findOrThrow($id);
        if (Yii::$app->request->isPost) {
            $manager->setAttributes(Yii::$app->request->post('Manager'));
            if ($manager->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $manager->refresh();
            }
        }

        return $this->render('view', [
            'manager' => $manager,
            'industrialFunctionsActiveDataProvider' => new ActiveDataProvider([
                'query' => $manager->getIndustrialFunctions(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]),
            'informationNeedsDataProvider' => new ActiveDataProvider([
                'query' => $manager->getInformationNeeds(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]),
        ]);
    }

    /**
     * Удаление менеджера
     *
     * `/repository/management/{id}/delete`
     *
     * @param int $id Идентификатор менеджера
     *
     * @return Response
     *
     * @throws Exception
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        $manager = Manager::findOrThrow($id);
        if ($manager->delete()) {
            Yii::$app->session->setFlash('success', "Менеджер \"{$manager->name}\" успешно удален.");
        }
        else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении менеджера.');
        }

        return $this->redirect(['index']);
    }
}