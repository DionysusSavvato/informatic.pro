<?php

namespace app\modules\repository\modules\management\controllers;


use common\models\InformationNeed;
use common\models\InformationProduct;
use common\models\InformationProductToInformationNeed;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для работы с информационными потребностями
 */
class InformationNeedController extends Controller
{
    /**
     * Обзор информационной потребности
     *
     * `/repository/management/information-need/{id}`
     *
     * @param int $id Идентификатор информационной потребности
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $informationNeed = InformationNeed::findOrThrow($id);
        if (Yii::$app->request->isPost) {
            $informationNeed->setAttributes(Yii::$app->request->post('InformationNeed'));
            if ($informationNeed->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $informationNeed->refresh();
            }
        }

        $informationProductsProvider = new ActiveDataProvider([
            'query' => $informationNeed->getInformationProductsToInformationNeeds(),
        ]);

        $informationProductsForAdding = InformationProduct::find()
            ->where([
                'not in',
                'entity_id',
                $informationNeed->getInformationProducts()->column(),
            ])
            ->all();
        $informationProductsForAdding = ArrayHelper::map($informationProductsForAdding, 'entity_id', 'name');

        return $this->render('view', [
            'informationNeed' => $informationNeed,
            'informationProductsProvider' => $informationProductsProvider,
            'informationProductsForAdding' => $informationProductsForAdding,
        ]);
    }

    /**
     * Создание информационной потребности
     *
     * `/repository/management/information-need/create`
     *
     * @return array|Response
     */
    public function actionCreate()
    {
        $informationNeed = new InformationNeed();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $informationNeed->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($informationNeed);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $informationNeed->load(Yii::$app->request->post())) {
            /** @noinspection NestedPositiveIfStatementsInspection */
            if ($informationNeed->save()) {
                Yii::$app->getSession()->setFlash('success', 'Информационная потребность успешно создана');
            }
        }

        return $this->goBack();
    }

    /**
     * Удаление информационной потребности
     *
     * `/repository/management/information-need/{id}/delete`
     *
     * @param int $id Идентификатор информационной потребности
     *
     * @return Response
     *
     * @throws Throwable
     * @throws Exception
     */
    public function actionDelete(int $id): Response
    {
        $informationNeed = InformationNeed::findOrThrow($id);
        if ($informationNeed->delete()) {
            Yii::$app->session->setFlash('success', "Менеджер \"{$informationNeed->name}\" успешно удален.");
        }
        else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении менеджера.');
        }

        return $this->redirect([
            '/repository/management/default/view',
            'id' => $informationNeed->manager_id,
        ]);
    }

    /**
     * Прикрепление информационного продукта
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function actionAttachProduct(int $id): Response
    {
        $informationNeed = InformationNeed::findOrThrow($id);
        $infProductToInfNeed = new InformationProductToInformationNeed();
        $infProductToInfNeed->setAttributes(Yii::$app->request->post('InformationProductToInformationNeed'));
        $infProductToInfNeed->save();
        return $this->redirect(
            [
                '/repository/management/information-need/view',
                'id' => $informationNeed->entity_id,
            ]
        );
    }

    /**
     * Открепление информационного продукта
     *
     * @param int $id
     * @param int $informationProductID
     *
     * @return Response
     *
     * @throws Throwable
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     */
    public function actionDetachProduct(int $id, int $informationProductID): Response
    {
        $informationProductToInformationNeed = InformationProductToInformationNeed::findOrThrow([
            'information_need_id' => $id,
            'information_product_id' => $informationProductID,
        ]);

        if ($informationProductToInformationNeed->delete()) {
            Yii::$app->session->setFlash('success', 'Успешно.');
        }
        else {
            Yii::$app->session->setFlash('danger', 'Ошибка при удалении.');
        }

        return $this->redirect([
            'view',
            'id' => $id,
        ]);
    }

    /**
     * Изменение порогового значения
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function actionChangeThreshold(int $id): Response
    {
        $infNeedToInfProd = InformationProductToInformationNeed::findOrThrow($id);
        $infNeedToInfProd->effect_threshold_value = Yii::$app->request
            ->post('InformationProductToInformationNeed')
        ['effect_threshold_value'];
        $infNeedToInfProd->save();
        return $this->redirect([
            'view',
            'id' => $infNeedToInfProd->information_need_id,
        ]);
    }
}