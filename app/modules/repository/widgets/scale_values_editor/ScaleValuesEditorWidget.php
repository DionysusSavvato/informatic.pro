<?php

namespace app\modules\repository\widgets\scale_values_editor;

use common\models\Scale;
use yii\base\InvalidParamException;
use yii\base\Widget;

/**
 * Виджет формы редактора значений шкал
 *
 * @property Scale $scale
 */
class ScaleValuesEditorWidget extends Widget
{
    /** @var Scale */
    private $_scale;

    /**
     * @return string
     *
     * @throws InvalidParamException
     */
    public function run(): ?string
    {
        if (!$this->scale->hasFixedValues()) {
            return null;
        }

        return $this->render('scale_values_editor', [
            'scale' => $this->scale,
        ]);
    }

    /**
     * @return Scale
     */
    public function getScale(): Scale
    {
        return $this->_scale;
    }

    /**
     * @param Scale $scale
     */
    public function setScale(Scale $scale): void
    {
        $this->_scale = $scale;
    }


}