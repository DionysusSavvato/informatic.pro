var ScaleEditorApp = angular.module('ScaleEditor', []);

ScaleEditorApp.run(function run($http) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $('meta[name="token"]').attr("content");
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
});