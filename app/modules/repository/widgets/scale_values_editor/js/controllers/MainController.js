ScaleEditorApp.controller('MainController',
    [
        '$scope',
        '$http',
        'ScaleApi',
        function ($scope, $http, ScaleApi) {
            /**
             * Значения шкалы
             * @type {Array}
             */
            $scope.values = [];

            ScaleApi.getValues(scaleID).then(function (response) {
                response.data.values.forEach(function (value) {
                    value.editingMode = false;
                    value.isNewRecord = false;
                });

                $scope.values = response.data.values;
            });

            $scope.updateRank = function () {
                $scope.values.forEach(function (value, index) {
                    value.rank = index;
                    ScaleApi.putValue(value);
                });
            };

            /**
             * Перемещение значения вверх по списку
             * @param index
             */
            $scope.moveUp = function (index) {
                if (index > 0) {
                    var buffer = $scope.values[index];
                    $scope.values[index] = $scope.values[index - 1];
                    $scope.values[index - 1] = buffer;
                    $scope.updateRank();
                }
            };

            /**
             * Перемещение значения вниз по списку
             * @param index
             */
            $scope.moveDown = function (index) {
                if (index < $scope.values.length - 1) {
                    var buffer = $scope.values[index];
                    $scope.values[index] = $scope.values[index + 1];
                    $scope.values[index + 1] = buffer;
                    $scope.updateRank();
                }
            };


            /**
             * Установка режима редактирования
             * @param index Порядковый индекс значения шкалы
             * @param flag Устанавливаемый режим, false - чтение, true - редактирование
             */
            $scope.setEditable = function (index, flag) {
                $scope.values[index].editingMode = flag;
                var value = $scope.values[index];
                if (flag === false) {
                    if (value.isNewRecord) {
                        ScaleApi.postValue(value).then(function () {
                            value.isNewRecord = false;
                        });
                    }
                    else {
                        ScaleApi.putValue(value);
                    }
                }
            };

            /**
             * Удаление значения шкалы
             * @param index
             */
            $scope.remove = function (index) {
                var value = $scope.values[index];
                ScaleApi.deleteValue(value).then(function () {
                    alert("Значение успешно удалено.");
                });
            };

            /**
             * Добавление нового значения
             */
            $scope.addNewValue = function () {
                $scope.values.push({
                    scale_id: scaleID,
                    name: '',
                    rank: null,
                    editingMode: true,
                    isNewRecord: true
                })
                ;
            }
        }
    ]
);