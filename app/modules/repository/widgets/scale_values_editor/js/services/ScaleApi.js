ScaleEditorApp.service('ScaleApi',
    [
        '$http',
        function ($http) {
            this.getValues = function (scaleID) {
                return $http.get('/api/scale/' + scaleID + '?expand=values');
            };
            this.postValue = function (data) {
                return $http.post(
                    '/api/scale/' + scaleID + '/value',
                    $.param({
                        name: data.name,
                        rank: data.rank
                    })
                );
            };
            this.putValue = function (value) {
                return $http.put(
                    '/api/scale/value/' + value.scale_value_id,
                    $.param({
                        name: value.name,
                        rank: value.rank
                    }));
            };
            this.deleteValue = function (value) {
                return $http.delete('/api/scale/value/' + value.scale_value_id);
            }

        }
    ]
);