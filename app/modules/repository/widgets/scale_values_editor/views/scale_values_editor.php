<?php

use app\modules\repository\widgets\scale_values_editor\assets\ScaleEditorAsset;
use common\models\Scale;
use yii\web\View;

/** @var Scale $scale */
/** @var View $this */

ScaleEditorAsset::register($this);

?>
<div class="container-fluid" ng-app="ScaleEditor">

    <div class="box box-solid" ng-controller="MainController">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h3 class="box-title">Значения шкалы</h3>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid" ng-repeat="value in values">
                <div class="col-md-1" style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
                    <h4>{{$index+1}}</h4>
                </div>
                <div class="col-md-8" style="margin-top: 5px; margin-bottom: 5px;">
                    <input type="text" class="form-control" ng-model="value.name" ng-disabled="!value.editingMode">
                </div>
                <div class="pull-right" style="margin-top: 5px; margin-bottom: 5px;">
                    <button class="btn btn-default" title="Вверх" ng-click="moveUp($index)">
                        <i class="fa fa-arrow-up"></i>
                    </button>
                    <button class="btn btn-default" title="Вниз" ng-click="moveDown($index)">
                        <i class="fa fa-arrow-down"></i>
                    </button>
                    <button class="btn btn-success" title="Редактировать" ng-click="setEditable($index, true)"
                            ng-show="!value.editingMode">
                        <i class="fa fa-cog"></i>
                    </button>
                    <button class="btn btn-success" title="Сохранить" ng-click="setEditable($index, false)"
                            ng-show="value.editingMode">
                        <i class="fa fa-save"></i>
                    </button>
                    <button class="btn btn-danger" title="Удалить" ng-click="remove($index)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </div>
            <hr style="margin: 5px">
        </div>
        <div class="box-footer">
            <div class="container-fluid">
                <button class="btn btn-primary" ng-click="addNewValue()"><i class="fa fa-plus"></i> Добавить</button>
            </div>
        </div>
    </div>
</div>

<script>var scaleID = <?= $scale->scale_id ?>;</script>
