<?php

namespace app\modules\repository\widgets\scale_values_editor\assets;

use app\assets\AngularAsset;
use yii\web\AssetBundle;

/**
 * ScaleEditor Asset Bundle
 */
class ScaleEditorAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/repository/widgets/scale_values_editor/js';

    public $js = [
        'app.js',
        'services/ScaleApi.js',
        'controllers/MainController.js',
    ];

    public $depends = [
        AngularAsset::class,
    ];
}