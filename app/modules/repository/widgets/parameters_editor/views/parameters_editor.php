<?php

use common\interfaces\EntityInterface;
use common\models\Parameter;
use common\models\Scale;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var string $title
 * @var EntityInterface    $entity
 * @var ActiveDataProvider $dataProvider
 * @var Parameter          $model
 */

?>

<div class="box box-success">
    <div class="box-header with-border">
        <div class="container-fluid">
            <h2 class="box-title"><?= $title; ?></h2>
        </div>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <?php
            /** @noinspection PhpUnhandledExceptionInspection */
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => SerialColumn::class],
                    [
                        'class' => DataColumn::class,
                        'attribute' => 'name',
                        'filterInputOptions' => [
                            'name' => 'name',
                            'class' => 'form-control',
                        ],
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => '{view}  {delete}',
                        'contentOptions' => [
                            'align' => 'center',
                        ],
                        'urlCreator' => function (string $action, Parameter $model) {
                            switch ($action) {
                                case 'view':
                                    return Url::to([
                                        '/repository/parameter/view',
                                        'id' => $model->parameter_id,
                                    ]);
                                case 'delete':
                                    return Url::to([
                                        '/repository/parameter/delete',
                                        'id' => $model->parameter_id,
                                    ]);
                            }
                            return Url::home();
                        },
                    ],
                ],
                'options' => [
                    'class' => 'box-body',
                ],
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover',
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="container-fluid">
            <button type="button"
                    class="btn btn-default"
                    data-toggle="modal"
                    data-target="#modal-parameter">
                <i class="fa fa-plus"></i> Добавить параметр
            </button>


        </div>
    </div>
</div>

<div class="modal fade" id="modal-parameter" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
                'options' => [
                    'autocomplete' => 'off',
                ],
                'action' => ['/repository/parameter/create'],
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Добавить параметр</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?= $form->field($model, 'name')
                        ->textInput(['autocomplete' => 'off']) ?>

                    <?= $form->field($model, 'entity_id')
                        ->hiddenInput(['value' => $entity->getEntityID()])
                        ->label(false) ?>

                    <?= $form->field($model, 'scale_id')
                        ->dropDownList(
                            Scale::find()->indexBy('scale_id')->select(['name'])->column()
                        )->label('Шкала') ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>

                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-floppy-o"></i>  Сохранить',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
