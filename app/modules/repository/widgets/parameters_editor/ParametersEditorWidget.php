<?php

namespace app\modules\repository\widgets\parameters_editor;


use common\interfaces\EntityInterface;
use common\models\Parameter;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Виджет управления параметрами сущности
 *
 * @property EntityInterface $entity
 * @property string $title
 */
class ParametersEditorWidget extends Widget
{
    /** @var EntityInterface */
    private $_entity;

    /** @var string */
    private $_title = 'Параметры';

    /**
     * {@inheritDoc}
     *
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();

        if ($this->_entity === null) {
            throw new InvalidConfigException('Ошибка инициализации редактора параметров');
        }

        Yii::$app->user->setReturnUrl(Yii::$app->request->absoluteUrl);
    }

    /**
     * {@inheritDoc}
     *
     * @throws InvalidParamException
     */
    public function run(): string
    {
        return $this->render('parameters_editor', [
            'title' => $this->title,
            'entity' => $this->entity,
            'model' => new Parameter(),
            'dataProvider' => new ActiveDataProvider([
                'query' => $this->entity->getParameters(),
            ]),
        ]);
    }

    /**
     * @return EntityInterface
     */
    public function getEntity(): EntityInterface
    {
        return $this->_entity;
    }

    /**
     * @param EntityInterface $entity
     */
    public function setEntity(EntityInterface $entity)
    {
        $this->_entity = $entity;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->_title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->_title = $title;
    }
}