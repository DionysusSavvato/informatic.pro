<?php

namespace app\modules\repository;

use app\modules\repository\controllers\ScaleController;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * Модуль для управления репозиторием бизнес-процесса
 *
 * @see ScaleController::actionIndex() Список шкал
 * @see ScaleController::actionView() Вывод шкалы
 * @see ScaleController::actionCreate() Создание измерительной шкалы
 * @see ScaleController::actionDelete() Удаление измерительной шкалы
 *
 */
class RepositoryModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        parent::init();

        $this->controllerNamespace = 'app\modules\repository\controllers';

        $this->loadRules();
    }

    /**
     * Загрузка правил роутинга
     */
    private function loadRules(): void
    {
        Yii::$app->urlManager->addRules(
            ArrayHelper::merge(
                require __DIR__ . '/config/rules.php',
                require __DIR__ . '/modules/consumer/config/rules.php',
                require __DIR__ . '/modules/industrial_function/config/rules.php',
                require __DIR__ . '/modules/management/config/rules.php',
                require __DIR__ . '/modules/information_system/config/rules.php'
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}