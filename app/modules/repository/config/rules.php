<?php

return [
    'repository' => 'repository/default/index',
    'repository/structure/<id:\\d+>' => 'repository/default/structure-view',
    'repository/structure' => 'repository/default/structure',

    'repository/scale' => 'repository/scale/index',
    'repository/scale/create' => 'repository/scale/create',
    'repository/scale/<id:\\d+>' => 'repository/scale/view',
    'repository/scale/<id:\\d+>/delete' => 'repository/scale/delete',

    'repository/parameter/create' => 'repository/parameter/create',
    'repository/parameter/<id:\\d+>' => 'repository/parameter/view',
    'repository/parameter/<id:\\d+>/delete' => 'repository/parameter/delete',
];