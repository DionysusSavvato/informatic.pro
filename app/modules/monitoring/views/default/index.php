<?php

use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 */

$this->title = 'Мониторинг';
$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
];

?>

<div class="monitoring">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2>
                    <?= $this->title; ?>
                </h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <div class="list-group">
                    <a href="<?= Url::to(['/monitoring/effect/default/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">E&nbsp;</h2>
                        <h4 class="list-group-item-heading">Эффекты</h4>
                        <p class="list-group-item-text">
                            Указание эффекта информационных и производственных продуктов
                        </p>
                    </a>
                    <a href="<?= Url::to(['/monitoring/expense/default/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">З&nbsp;</h2>
                        <h4 class="list-group-item-heading">Затраты</h4>
                        <p class="list-group-item-text">
                            Ввод данных по затратам производственных ресурсов и затратам на содержание корпоративной
                            информационной системы
                        </p>
                    </a>
                    <a href="<?= Url::to(['/monitoring/parameter/default/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">K&nbsp;</h2>
                        <h4 class="list-group-item-heading">Показатели</h4>
                        <p class="list-group-item-text">
                            Задание значения показателей элементов бизнес-процессов
                        </p>
                    </a>
                    <a href="<?= Url::to(['/monitoring/change-registrator/default/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">&Delta;S<sub>сио</sub>&nbsp;</h2>
                        <h4 class="list-group-item-heading">Регистрация преобразований в КИС</h4>
                        <p class="list-group-item-text">
                            Описание и локализация изменений, произведенных в корпоративной информационной системе
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
