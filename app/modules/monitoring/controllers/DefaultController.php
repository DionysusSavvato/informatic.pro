<?php

namespace app\modules\monitoring\controllers;

use yii\base\InvalidParamException;
use yii\web\Controller;

/**
 * Базовый контроллер модуля мониторинга
 */
class DefaultController extends Controller
{
    /**
     * Базовое меню, стартовая страница модуля
     *
     * '/monitoring'
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }
}