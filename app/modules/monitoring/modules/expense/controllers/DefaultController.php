<?php

namespace app\modules\monitoring\modules\expense\controllers;

use yii\base\InvalidParamException;
use yii\web\Controller;

/**
 * Базовый контроллер модуля ввода затрат
 */
class DefaultController extends Controller
{
    /**
     * Стартовая страница модуля
     * 
     * `monitoring/expense`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }
}