<?php

namespace app\modules\monitoring\modules\expense\controllers;

use common\models\IndustrialExpense;
use common\models\IndustrialFunction;
use common\models\Resource;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidParamException;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class IndustrialController extends Controller
{
    /**
     * Форма для выбора и ввода данных по затратам
     * 
     * `monitoring/expense/industrial`
     * 
     * @param int|null $id
     * @param int|null $resourceID
     *
     * @return string|array
     *
     * @throws InvalidParamException
     */
    public function actionIndex(int $id = null, int $resourceID = null)
    {
        $expense = new IndustrialExpense();

        if (Yii::$app->request->isAjax && $expense->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($expense);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $expense->load(Yii::$app->request->post())) {
            if ($expense->save()) {
                Yii::$app->getSession()->setFlash('success', 'Успешно');
            }
        }

        return $this->render('index', [
            'function' => IndustrialFunction::findOne($id),
            'resource' => Resource::findOne($resourceID),
            'expense' => $expense,
        ]);
    }

    /**
     * Удаление данных по затрате
     *
     * `monitoring/expense/industrial/{id}/delete`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws StaleObjectException
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        IndustrialExpense::findOrThrow($id)->delete();

        return $this->redirect(['index']);
    }
}