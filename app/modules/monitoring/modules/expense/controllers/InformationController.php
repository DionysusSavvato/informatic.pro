<?php

namespace app\modules\monitoring\modules\expense\controllers;

use common\models\InformationExpense;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для работы с затратами на содержание КИС
 */
class InformationController extends Controller
{
    /**
     * Страница данных по затратам на содержание КИС
     * 
     * `monitoring/expense/information`
     *
     * @return string|array
     *
     * @throws InvalidParamException
     */
    public function actionIndex()
    {
        $model = new InformationExpense();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Успешно');
            }
        }
        return $this->render('index', [
            'model' => new InformationExpense(),
            'dataProvider' => new ActiveDataProvider([
                'query' => InformationExpense::find()
                    ->orderBy([
                        'date' => SORT_DESC,
                    ]),
            ]),
        ]);
    }

    /**
     * Удаление данных по затрате
     *
     * `monitoring/expense/information/{id}/delete`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws Exception
     * @throws StaleObjectException
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        InformationExpense::findOrThrow($id)->delete();

        return $this->redirect(['index']);
    }
}