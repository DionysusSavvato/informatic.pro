<?php

use common\models\IndustrialExpense;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View              $this
 * @var IndustrialExpense $expense
 */

?>


<div class="input-industrial-expense container-fluid">
    <h3>Ввести затраты на ресурс <?= $resource->material->name ?></h3>
    <div class="container-fluid">
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'options' => [
                'autocomplete' => 'off',
            ],
        ]); ?>
        <?= $form->field($expense, 'resource_id')
            ->hiddenInput(['value' => $resource->id])
            ->label(false); ?>
        <?= $form->field($expense, 'price')->input('number'); ?>
        <?= $form->field($expense, 'count')->input('number'); ?>
        <?= $form->field($expense, 'date')->input('date'); ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                ['encode' => false, 'class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<hr>
<div class="industrial-expenses container-fluid">
    <h3>Данные по затратам ресурса</h3>
    <?php
    echo GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $resource->getIndustrialExpenses()->orderBy(['date' => SORT_DESC]),
        ]),
        'columns' => [
            ['class' => SerialColumn::class],
            [
                'class' => DataColumn::class,
                'attribute' => 'date',
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'price',
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'count',
            ],
            [
                'class' => ActionColumn::class,
                'controller' => 'default',
                'template' => '{delete}',
                'urlCreator' => function (string $action, IndustrialExpense $model) {
                    switch ($action) {
                        case 'delete':
                            return Url::to([
                                '/monitoring/expense/industrial/delete',
                                'id' => $model->id,
                            ]);
                    }
                },
                'contentOptions' => [
                    'align' => 'center',
                ],
            ],
        ],
        'options' => [
            'class' => 'box-body',
        ],
        'tableOptions' => [
            'class' => 'table table-bordered table-hover',
        ],
    ]);
    ?>
</div>
