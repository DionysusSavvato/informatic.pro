<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use common\models\IndustrialExpense;
use common\models\IndustrialFunction;
use common\models\Resource;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var Resource           $resource
 * @var IndustrialExpense  $expense
 */

$tabs = [
    [
        'label' => 'Иерархия производственных процессов',
        'content' => IndustrialFunctionHierarchyWidget::widget([
            'baseRoute' => [
                '/monitoring/expense/industrial/index',
            ],
        ]),
        'active' => true,
    ],
];
if ($function) {
    $tabs[0]['active'] = false;

    $tabs[] = [
        'label' => "Ресурсы процесса {$function->abbreviation}",
        'content' => GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => $function->getResources(),
            ]),
            'columns' => [
                ['class' => SerialColumn::class],
                [
                    'class' => DataColumn::class,
                    'attribute' => 'abbreviation',
                ],
                [
                    'class' => DataColumn::class,
                    'attribute' => 'material.name',
                ],
                [
                    'class' => ActionColumn::class,
                    'controller' => 'default',
                    'template' => '{view}',
                    'urlCreator' => function (string $action, Resource $resource) use ($function) {
                        switch ($action) {
                            case 'view' :
                                return Url::to([
                                    '/monitoring/expense/industrial/index',
                                    'id' => $function->entity_id,
                                    'resourceID' => $resource->id,
                                ]);
                        }
                    },
                    'contentOptions' => [
                        'align' => 'center',
                    ],
                ],
            ],
            'options' => [
                'class' => 'box-body',
            ],
            'tableOptions' => [
                'class' => 'table table-bordered table-hover',
            ],
        ]),
        'active' => true,
    ];

    if ($resource) {
        $tabs[1]['active'] = false;
        $tabs[] = [
            'label' => 'Затраты на ресурс',
            'content' => $this->render('input-form', [
                'resource' => $resource,
                'expense' => $expense,
            ]),
            'active' => true,
        ];
    }
}
?>
<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => $tabs,
    ]); ?>
</div>
