<?php

use common\models\IndustrialExpense;
use common\models\IndustrialFunction;
use yii\web\View;

/**
 * @var View               $this
 * @var Resource           $resource
 * @var IndustrialFunction $function
 * @var IndustrialExpense  $expense
 */

$this->title = 'Производственные затраты';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">З</span> Затраты',
        'url' => ['/monitoring/expense'],
    ],
    [
        'label' => '<span class="abbreviation-mini">З<sub>in</sub></span> Производственные затраты',
        'url' => ['/monitoring/expense/industrial'],
    ],
];

?>

<div class="industrial-expenses container-fluid">
    <?= $this->render('index/select-form', [
        'resource' => $resource,
        'function' => $function,
        'expense' => $expense,
    ]) ?>
</div>
