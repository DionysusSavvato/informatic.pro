<?php

use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 */

$this->title = 'Затраты';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">З</span> Затраты',
        'url' => ['/monitoring/expense'],
    ],
];

?>

<div class="expenses">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2>
                    <?= $this->title; ?>
                </h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <div class="list-group">
                    <a href="<?= Url::to(['/monitoring/expense/industrial/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">З<sub>in</sub>&nbsp;</h2>
                        <h4 class="list-group-item-heading">Производственные затраты</h4>
                        <p class="list-group-item-text">
                            Указание значения производственных затрат
                        </p>
                    </a>
                    <a href="<?= Url::to(['/monitoring/expense/information/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">З<sub>into</sub>&nbsp;</h2>
                        <h4 class="list-group-item-heading">Затраты на содержание КИС</h4>
                        <p class="list-group-item-text">
                            Указание значения затрат, направленных на обеспечение работы корпоративной информационной
                            системы
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
