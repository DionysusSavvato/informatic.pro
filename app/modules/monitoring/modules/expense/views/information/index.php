<?php

use common\models\InformationExpense;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 */

$this->title = 'Затраты на КИС';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">З</span> Затраты',
        'url' => ['/monitoring/expense'],
    ],
    [
        'label' => '<span class="abbreviation-mini">З<sub>into</sub></span> Затраты на КИС',
        'url' => ['/monitoring/expense/information'],
    ],
];

?>

<div class="input-information-expense container-fluid">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h3>Ввести затраты на содержание КИС</h3>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($model, 'sum')->input('number'); ?>
                <?= $form->field($model, 'date')->input('date'); ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="information-expenses container-fluid">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2 class="box-title">Данные по затратам на КИС</h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'date',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'sum',
                        ],
                        [
                            'class' => ActionColumn::class,
                            'controller' => 'default',
                            'template' => '{delete}',
                            'urlCreator' => function (string $action, InformationExpense $model) {
                                switch ($action) {
                                    case 'delete':
                                        return Url::to([
                                            '/monitoring/expense/information/delete',
                                            'id' => $model->id,
                                        ]);
                                }
                            },
                            'contentOptions' => [
                                'align' => 'center',
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'box-body',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>