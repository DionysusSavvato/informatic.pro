<?php

return [
    'monitoring/expense' => 'monitoring/expense/default/index',

    'monitoring/expense/industrial' => 'monitoring/expense/industrial/index',
    'monitoring/expense/industrial/<id:\\d+>/delete' => 'monitoring/expense/industrial/delete',

    'monitoring/expense/information' => 'monitoring/expense/information/index',
    'monitoring/expense/information/<id:\\d+>/delete' => 'monitoring/expense/information/delete',
];