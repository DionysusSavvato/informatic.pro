<?php

namespace app\modules\monitoring\modules\expense;

use yii\base\Module;
use yii\filters\AccessControl;


/**
 * Модуль для ввода затрат
 */
class ExpenseModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}