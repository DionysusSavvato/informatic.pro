<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use common\models\InformationChange;
use common\models\InformationChangeLocalization;
use kartik\detail\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View              $this
 * @var InformationChange $model
 */

$this->title = $model->title;

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">&Delta;S<sub>сио</sub>&nbsp;</span>Регистрация преобразований в КИС',
        'url' => ['/monitoring/change-registrator/default/index'],
    ],
];

$tabs = [
    [
        'label' => 'Иерархия производственных процессов',
        'content' => IndustrialFunctionHierarchyWidget::widget([
            'baseRoute' => [
                '/monitoring/change-registrator/default/select-form',
                'changeID' => $model->id,
            ],
        ]),
        'active' => true,
    ],
];

$changesButton = '<a 
    href="' . Url::to(['/monitoring/change-registrator/default/model-changes', 'id' => $model->id]) .
    '" class="fa fa-exclamation-circle" ' . ($model->flag_model_changes_needed ? 'style="color: #c92b27;" ' : ' ') .
    ' title="Требуются правки"></a>';
?>

<div class="information-change">
    <?= DetailView::widget([
        'model' => $model,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_SUCCESS,
        ],
        'attributes' => [
            'title',
            [
                'attribute' => 'description',
                'type' => DetailView::INPUT_TEXTAREA,
                'options' => [
                    'rows' => 10,
                ],
            ],
            [
                'attribute' => 'date',
                'type' => DetailView::INPUT_DATE,
                'widgetOptions' => [
                    'pluginOptions' => ['format' => 'yyyy-mm-dd'],
                ],
            ],
        ],
        'deleteOptions' => [
            'confirm' => 'Вы действительно хотите удалить?',
            'url' => Url::to(['/monitoring/change-registrator/default/delete', 'id' => $model->id]),
        ],
        'buttons1' => "{$changesButton} {update} {delete}",
    ]);
    ?>
</div>

<div class="localization">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2 class="box-title">Места преобразований</h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?= GridView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => $model->getInformationChangeLocalizations(),
                    ]),
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'entity.typeName',
                            'label' => 'Тип сущности',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'entity.instance.abbreviation',
                            'label' => 'Аббревиатура',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'entity.instance.name',
                            'label' => 'Название',
                        ],
                        [
                            'class' => DataColumn::class,
                            'attribute' => 'industrialFunction.name',
                            'label' => 'Процесс',
                        ],
                        [
                            'class' => ActionColumn::class,
                            'controller' => 'default',
                            'template' => '{delete}',
                            'urlCreator' => function (string $action, InformationChangeLocalization $model) {
                                switch ($action) {
                                    case 'delete' :
                                        return Url::to([
                                            '/monitoring/change-registrator/default/delete-localization',
                                            'id' => $model->information_change_id,
                                            'entityID' => $model->entity_id,
                                        ]);
                                }
                            },
                            'contentOptions' => [
                                'align' => 'center',
                            ],
                        ],
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover',
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="box-footer">
            <div class="container-fluid">
                <a type="button"
                   href="<?= Url::to([
                       '/monitoring/change-registrator/default/select-form',
                       'changeID' => $model->id,
                   ]) ?>"
                   class="btn btn-default"
                >
                    <i class="fa fa-plus"></i> Добавить область преобразований
                </a>
            </div>
        </div>
    </div>
</div>