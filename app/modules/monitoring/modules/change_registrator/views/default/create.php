<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 */

$this->title = 'Регистрация преобразований в КИС';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">&Delta;S<sub>сио</sub>&nbsp;</span>Регистрация преобразований в КИС',
        'url' => ['/monitoring/change-registrator/default/index'],
    ],
];

?>

<div class="admin">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2><?= $this->title ?></h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'options' => [
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?= $form->field($model, 'title')
                    ->textInput([
                        'autocomplete' => 'off',
                    ]) ?>
                <?= $form->field($model, 'date')
                    ->input('date', [
                        'pattern' => '[0-9]{4}-[0-9]{2}-[0-9]{2}',
                    ]) ?>
                <?= $form->field($model, 'description')
                    ->textarea([
                        'autocomplete' => 'off',
                        'rows' => 10,
                    ]) ?>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-plus"></i>  Создать',
                        ['encode' => false, 'class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
