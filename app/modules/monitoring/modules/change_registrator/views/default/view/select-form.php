<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use app\widgets\structure\StructureWidget;
use common\models\IndustrialFunction;
use common\models\InformationChange;
use yii\bootstrap\Tabs;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var InformationChange  $informationChange
 */

$tabs = [
    [
        'label' => 'Иерархия производственных процессов',
        'content' => IndustrialFunctionHierarchyWidget::widget([
            'baseRoute' => [
                '/monitoring/change-registrator/default/select-form',
                'changeID' => $informationChange->id,
            ],
        ]),
        'active' => true,
    ],
];
if ($function) {
    $tabs[0]['active'] = false;
    $tabs[] = [
        'label' => "Структура процесса {$function->abbreviation}",
        'content' => StructureWidget::widget([
            'industrialFunction' => $function,
            'openInOtherWindow' => false,
            'urlConfig' => [
                '/monitoring/change-registrator/default/select',
                'changeID' => $informationChange->id,
                'functionID' => $function->entity_id,
            ],
        ]),
        'active' => true,
    ];
}
?>
<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => $tabs,
    ]); ?>
</div>
