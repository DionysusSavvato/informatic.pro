<?php

use Carbon\Carbon;
use common\models\InformationChange;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

/**
 * @var View               $this
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Регистрация изменений в КИС';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
];

/** @var InformationChange[] $models */
$models = $dataProvider->getModels();
?>

<div class="container-fluid">
    <h3>Преобразования в корпоративной информационной системе</h3>
    <a
            href="<?= Url::to(['/monitoring/change-registrator/default/create']); ?>"
            class="btn btn-success">
        <i class="fa fa-plus"></i> Зарегистрировать преобразование
    </a>
    <br>
    <br>

    <ul class="timeline">
        <?php foreach ($models as $model): ?>
            <li class="time-label">
            <span class="bg-red">
                <i class="fa fa-clock-o"></i> <?= Carbon::parse($model->date)->format('d.m.Y'); ?>
            </span>
            </li>
            <li>
                <div class="timeline-item">
                    <h3 class="timeline-header"><?= $model->title; ?></h3>

                    <div class="timeline-body">
                        <?= $model->description; ?>
                    </div>

                    <div class="timeline-footer">
                        <a href="<?= Url::to([
                            '/monitoring/change-registrator/default/view',
                            'id' => $model->id,
                        ]) ?>" class="btn btn-primary btn-xs">Подробнее</a>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>

    <?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]); ?>
</div>


