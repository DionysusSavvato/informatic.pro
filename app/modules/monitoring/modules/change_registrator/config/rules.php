<?php

return [
    'monitoring/change' => 'monitoring/change-registrator/default/index',
    'monitoring/change/<id:\\d+>' => 'monitoring/change-registrator/default/view',
    'monitoring/change/<changeID:\\d+>/select-form' => 'monitoring/change-registrator/default/select-form',
    'monitoring/change/<changeID:\\d+>/select/<entityID:\\d+>/from/<functionID:\\d+>' => 'monitoring/change-registrator/default/select',
    'monitoring/change/<id:\\d+>/delete/<entityID:\\d+>' => 'monitoring/change-registrator/default/delete-localization',
    'monitoring/change/<id:\\d+>/delete' => 'monitoring/change-registrator/default/delete',
    'monitoring/change/register' => 'monitoring/change-registrator/default/create',
    'monitoring/change/<id:\\d+>/model-changes' => 'monitoring/change-registrator/default/model-changes',
];