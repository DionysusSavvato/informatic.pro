<?php

namespace app\modules\monitoring\modules\change_registrator;

use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль регистрации изменений в КИС
 */
class ChangeRegistratorModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}