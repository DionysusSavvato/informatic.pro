<?php

namespace app\modules\monitoring\modules\change_registrator\controllers;

use common\models\IndustrialFunction;
use common\models\InformationChange;
use common\models\InformationChangeLocalization;
use Exception;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для работы с преобразованиями в КИС
 */
class DefaultController extends Controller
{
    /**
     * Вывод списка зарегистрированных преобразований КИС
     *
     * `monitoring/change`
     *
     * @return string
     *
     * @throws InvalidParamException
     * @throws InvalidConfigException
     */
    public function actionIndex(): string
    {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => InformationChange::find()
                    ->orderBy(['date' => SORT_DESC]),
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
        ]);
    }

    /**
     * Вывод и редактирование преобразования
     *
     * `monitoring/change/{id}`
     *
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     * @throws InvalidParamException
     */
    public function actionView(int $id): string
    {
        $informationChange = InformationChange::findOrThrow($id);

        if (Yii::$app->request->isPost) {
            $informationChange->setAttributes(Yii::$app->request->post('InformationChange'));
            if ($informationChange->save()) {
                Yii::$app->session->setFlash('kv-detail-success', 'Изменения успешно сохранены.');
                $informationChange->refresh();
            }
        }

        return $this->render('view', [
            'model' => $informationChange,
        ]);
    }

    /**
     * Рендеринг формы выбора объекта преобразования
     *
     * `monitoring/change/{changeID}/select-form`
     * 
     * @param int $changeID
     * @param int $id
     *
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionSelectForm(int $changeID, int $id = null): string
    {
        return $this->render('view/select-form',
            [
                'informationChange' => InformationChange::findOrThrow($changeID),
                'function' => IndustrialFunction::findOne($id),
            ]
        );
    }

    /**
     * Добавление выбранного места преобразования
     *
     * `monitoring/change/{changeID}/select/{entityID}`
     *
     * @param int $changeID
     * @param int $entityID
     * @param int $functionID
     *
     * @return Response
     */
    public function actionSelect(int $changeID, int $entityID, int $functionID): Response
    {
        $informationChangeLocalization = new InformationChangeLocalization([
            'information_change_id' => $changeID,
            'entity_id' => $entityID,
            'function_id' => $functionID,
        ]);

        if ($informationChangeLocalization->save()) {
            Yii::$app->session->setFlash('kv-detail-success', 'Успешно.');
        }

        return $this->redirect(['view', 'id' => $changeID]);
    }

    /**
     * Удаление зарегистрированного преобразования
     *
     * `monitoring/change/{id}/delete`
     *
     * @param int $id
     *
     * @return Response
     *
     * @throws StaleObjectException
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionDelete(int $id): Response
    {
        $informationChange = InformationChange::findOrThrow($id);
        $informationChange->delete();

        return $this->redirect(['index']);
    }

    /**
     * Удаление локализации преобразования
     *
     * `monitoring/change/{id}/delete/{entityID}`
     *
     * @param int $id
     * @param int $entityID
     *
     * @return Response
     *
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteLocalization(int $id, int $entityID): Response
    {
        InformationChangeLocalization::findOrThrow([
            'information_change_id' => $id,
            'entity_id' => $entityID,
        ])->delete();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Регистрация преобразования в КИС
     * 
     * `monitoring/change/register`
     *
     * @return array|string
     *
     * @throws InvalidParamException
     */
    public function actionCreate()
    {
        $informationChange = new InformationChange();

        /**
         * Если AJAX, то валидируем модель и отправляем результат валидации.
         * Блок кода для валидации формы.
         */

        if (Yii::$app->request->isAjax && $informationChange->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($informationChange);
        }

        /**
         * Если POST, то создаем.
         */
        if (Yii::$app->request->isPost && $informationChange->load(Yii::$app->request->post())) {
            if ($informationChange->save()) {
                Yii::$app->getSession()->setFlash('success', 'Успешно.');

                return $this->redirect(['index']);
            }

            Yii::$app->getSession()->setFlash('error', 'Ошибка.');
        }

        return $this->render('create', [
            'model' => $informationChange,
        ]);
    }

    /**
     * Смена статуса необходимости изменений в модели процессов
     *
     * `monitoring/change/{id}/model-changes`
     *
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionModelChanges(int $id): Response
    {
        $informationChange = InformationChange::findOrThrow($id);
        $informationChange->flag_model_changes_needed = !$informationChange->flag_model_changes_needed;
        $informationChange->save();

        return $this->redirect(['view', 'id' => $id]);
    }
}