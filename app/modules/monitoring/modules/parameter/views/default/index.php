<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use app\widgets\structure\StructureWidget;
use common\models\Entity;
use common\models\IndustrialFunction;
use common\models\Parameter;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var Entity             $entity
 * @var Parameter          $parameter
 */

$this->title = 'Параметры';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">K</span> Параметры',
        'url' => ['/monitoring/parameter'],
    ],
];

$tabs = [
    [
        'label' => 'Иерархия производственных процессов',
        'content' => IndustrialFunctionHierarchyWidget::widget([
            'baseRoute' => [
                '/monitoring/parameter/default/index',
            ],
        ]),
        'active' => true,
    ],
];
if ($function) {
    $tabs[0]['active'] = false;

    $tabs[] = [
        'label' => "Структура процесса {$function->abbreviation}",
        'content' => StructureWidget::widget([
            'industrialFunction' => $function,
            'openInOtherWindow' => false,
            'urlConfig' => [
                '/monitoring/parameter/default/index',
                'id' => $function->entity_id,
            ],
        ]),
        'active' => true,
    ];

    if ($entity) {
        $tabs[1]['active'] = false;
        $tabs[] = [
            'label' => 'Параметры',
            'content' => GridView::widget([
                'dataProvider' => new ActiveDataProvider([
                    'query' => $entity->getParameters(),
                ]),
                'columns' => [
                    ['class' => SerialColumn::class],
                    [
                        'class' => DataColumn::class,
                        'attribute' => 'name',
                    ],
                    [
                        'class' => DataColumn::class,
                        'attribute' => 'scale.name',
                        'label' => 'Шкала',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'controller' => 'default',
                        'template' => '{view}',
                        'urlCreator' => function (string $action, Parameter $parameter) use ($function, $entity) {
                            switch ($action) {
                                case 'view' :
                                    return Url::to([
                                        '/monitoring/parameter/default/index',
                                        'id' => $function->entity_id,
                                        'entityID' => $entity->entity_id,
                                        'parameterID' => $parameter->parameter_id,
                                    ]);
                            }
                        },
                        'contentOptions' => [
                            'align' => 'center',
                        ],
                    ],
                ],
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover',
                ],
            ]),
            'active' => true,
        ];
        if ($parameter) {
            $tabs[2]['active'] = false;
            $tabs[] = [
                'label' => "Структура процесса {$function->abbreviation}",
                'content' => $this->render('index/input-form', [
                    'parameter' => $parameter,
                ]),
                'active' => true,
            ];
        }
    }
}
?>

<div class="nav-tabs-custom">
    <?= Tabs::widget([
        'items' => $tabs,
    ]); ?>
</div>


