<?php

use common\models\Parameter;
use common\models\ParameterValue;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View      $this
 * @var Parameter $parameter
 */

$parameterValue = new ParameterValue();

?>

<div class="container-fluid">
    <p>
        Введите значение параметра
        <code class="name-code"><?= $parameter->name; ?></code> для
        <?=
        ($abbreviation = $parameter->entity->instance->abbreviation ?? null)
            ?
            '<code class="abbreviation-code"> ' . $abbreviation . '</code>'
            :
            ''; ?>
        <code class="name-code"><?= $parameter->entity->instance->name; ?></code>
    </p>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'action' => ['/monitoring/parameter/default/input'],
        'options' => [
            'autocomplete' => 'off',
        ],
    ]); ?>
    <?= $form->field($parameterValue, 'param_id')
        ->hiddenInput(['value' => $parameter->parameter_id])
        ->label(false); ?>
    <?php
    if ($parameter->scale->hasFixedValues()) {
        echo $form
            ->field($parameterValue, 'value')
                ->dropDownList(
                    ArrayHelper::map(
                        $parameter->scale->sortedScaleValues,
                        'scale_value_id',
                        'name'
                    )
                );
    } else {
        echo $form
            ->field($parameterValue, 'value')
                ->input('text');
    }
    ?>
    <?= $form->field($parameterValue, 'date')->input('datetime-local'); ?>
    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
            ['encode' => false, 'class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
