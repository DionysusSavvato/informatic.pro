<?php

namespace app\modules\monitoring\modules\parameter;

use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль для ввода значений параметров
 */
class ParameterModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}