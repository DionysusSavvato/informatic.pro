<?php

namespace app\modules\monitoring\modules\parameter\controllers;

use common\models\Entity;
use common\models\IndustrialFunction;
use common\models\Parameter;
use common\models\ParameterValue;
use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Базовый контроллер модуля ввода значений параметров
 */
class DefaultController extends Controller
{
    /**
     * Базовая страница модуля
     * 
     * `monitoring/parameter`
     *
     * @param int|null $id
     * @param int|null $entityID
     * @param int|null $parameterID
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(int $id = null, int $entityID = null, int $parameterID = null): string
    {
        return $this->render('index', [
            'function' => IndustrialFunction::findOne($id),
            'entity' => Entity::findOne($entityID),
            'parameter' => Parameter::findOne($parameterID),
        ]);
    }

    /**
     * Ввод значения параметра
     * 
     * `monitoring/parameter/input`
     *
     * @return array|Response
     */
    public function actionInput()
    {
        $parameterValue = new ParameterValue();
        $parameterValue->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($parameterValue);
        }

        if ($parameterValue->save()) {
            Yii::$app->getSession()->setFlash('success', 'Успешно');
        }

        return $this->redirect(['index']);
    }
}