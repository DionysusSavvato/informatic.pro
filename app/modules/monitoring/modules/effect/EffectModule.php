<?php

namespace app\modules\monitoring\modules\effect;

use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Модуль для ввода значений эффектов
 */
class EffectModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}