<?php

use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 */

$this->title = 'Эффекты';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
];

?>

<div class="effects">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h2>
                    <?= $this->title; ?>
                </h2>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <div class="list-group">
                    <a href="<?= Url::to(['/monitoring/effect/industrial/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">E<sub>in</sub>&nbsp;</h2>
                        <h4 class="list-group-item-heading">Производственные эффекты</h4>
                        <p class="list-group-item-text">
                            Указание значения эффектов - степени удовлетворения потребностей продуктом процессов
                        </p>
                    </a>
                    <a href="<?= Url::to(['/monitoring/effect/information/index']); ?>"
                       class="list-group-item">
                        <h2 class="pull-left abbreviation">E<sub>into</sub>&nbsp;</h2>
                        <h4 class="list-group-item-heading">Информационные эффекты</h4>
                        <p class="list-group-item-text">
                            Указание значения эффектов информационного обеспечения
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

