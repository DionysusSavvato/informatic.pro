<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use common\models\IndustrialFunction;
use common\models\Product;
use common\models\ProductToNeed;
use common\models\ProductToResource;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var Product            $product
 * @var ProductToNeed      $productToNeed
 * @var ProductToResource  $productToResource
 */

$this->title = 'Эффекты';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E</span> Эффекты',
        'url' => ['/monitoring/effect'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E<sub>in</sub></span> Поизводственные эффекты',
        'url' => ['/monitoring/effect/industrial'],
    ],
];

$tabs = [
    [
        'label' => 'Иерархия производственных процессов',
        'content' => IndustrialFunctionHierarchyWidget::widget([
            'baseRoute' => [
                '/monitoring/effect/industrial/index',
            ],
        ]),
        'active' => true,
    ],
];

if ($function) {
    $tabs[0]['active'] = false;
    $tabs[] = [
        'label' => "Продукты процесса {$function->abbreviation}",
        'content' => GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => $function->getProducts(),
            ]),
            'columns' => [
                ['class' => SerialColumn::class],
                [
                    'class' => DataColumn::class,
                    'attribute' => 'abbreviation',
                ],
                [
                    'class' => DataColumn::class,
                    'attribute' => 'material.name',
                ],
                [
                    'class' => ActionColumn::class,
                    'controller' => 'default',
                    'template' => '{view}',
                    'urlCreator' => function (string $action, Product $product) use ($function) {
                        switch ($action) {
                            case 'view' :
                                return Url::to([
                                    '/monitoring/effect/industrial/index',
                                    'id' => $function->entity_id,
                                    'productID' => $product->id,
                                ]);
                        }
                    },
                    'contentOptions' => [
                        'align' => 'center',
                    ],
                ],
            ],
            'options' => [
                'class' => 'box-body',
            ],
            'tableOptions' => [
                'class' => 'table table-bordered table-hover',
            ],
        ]),
        'active' => true,
    ];

    if ($product) {
        $tabs[1]['active'] = false;
        $tabs[] = [
            'label' => "Цели продукта {$product->material->name}",
            'content' => $this->render('index/target-select-from', [
                'function' => $function,
                'product' => $product,
            ]),
            'active' => true,
        ];

        if ($productToNeed || $productToResource) {
            $tabs[2]['active'] = false;
            $tabs[] = [
                'label' => 'Оценка эффекта',
                'content' => $this->render('index/input-form', [
                    'productToNeed' => $productToNeed,
                    'productToResource' => $productToResource,
                ]),
                'active' => true,
            ];
        }
    }
}


?>
<div class="container-fluid">
    <div class="nav-tabs-custom">
        <?= Tabs::widget([
            'items' => $tabs,
        ]); ?>
    </div>
</div>


