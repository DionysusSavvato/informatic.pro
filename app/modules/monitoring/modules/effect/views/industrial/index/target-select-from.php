<?php

use common\models\IndustrialFunction;
use common\models\Product;
use common\models\ProductToNeed;
use common\models\ProductToResource;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var Product            $product
 */

?>

<div class="container-fluid">
    <h3>Ресурс процесса</h3>
    <?=
    GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $product->getProductToResources(),
        ]),
        'columns' => [
            ['class' => SerialColumn::class],
            [
                'class' => DataColumn::class,
                'attribute' => 'resource.abbreviation',
                'label' => 'Аббревиатура ресурса',
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'resource.function.abbreviation',
                'label' => 'Аббревиатура функции',
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'resource.function.name',
                'label' => 'Функция',
            ],
            [
                'class' => ActionColumn::class,
                'controller' => 'default',
                'template' => '{view}',
                'urlCreator' => function (string $action, ProductToResource $productToResource) use (
                    $function,
                    $product
                ) {
                    switch ($action) {
                        case 'view' :
                            return Url::to([
                                '/monitoring/effect/industrial/index',
                                'id' => $function->entity_id,
                                'productID' => $product->id,
                                'productToResourceID' => $productToResource->id,
                            ]);
                    }
                },
                'contentOptions' => [
                    'align' => 'center',
                ],
            ],
        ],
        'options' => [
            'class' => 'box-body',
        ],
        'tableOptions' => [
            'class' => 'table table-bordered table-hover',
        ],
    ]);
    ?>
</div>
<br>
<hr>
<br>
<div class="container-fluid">
    <h3>Потребность потребителя</h3>
    <?=
    GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $product->getProductToNeeds(),
        ]),
        'columns' => [
            ['class' => SerialColumn::class],
            [
                'class' => DataColumn::class,
                'attribute' => 'need.abbreviation',
                'label' => 'Аббревиатура потребности',
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'need.name',
                'label' => 'Название потребности',
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'need.consumer.abbreviation',
                'label' => 'Аббревиатура потребителя',
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'need.consumer.name',
                'label' => 'Название потребителя',
            ],
            [
                'class' => ActionColumn::class,
                'controller' => 'default',
                'template' => '{view}',
                'urlCreator' => function (string $action, ProductToNeed $productToNeed) use (
                    $function,
                    $product
                ) {
                    switch ($action) {
                        case 'view' :
                            return Url::to([
                                '/monitoring/effect/industrial/index',
                                'id' => $function->entity_id,
                                'productID' => $product->id,
                                'productToNeedID' => $productToNeed->id,
                            ]);
                    }
                },
                'contentOptions' => [
                    'align' => 'center',
                ],
            ],
        ],
        'options' => [
            'class' => 'box-body',
        ],
        'tableOptions' => [
            'class' => 'table table-bordered table-hover',
        ],
    ]);
    ?>
</div>