<?php

use common\models\IndustrialEffect;
use common\models\ProductToNeed;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View          $this
 * @var ProductToNeed $productToNeed
 */

$effect = new IndustrialEffect();

?>

<div class="container-fluid">
    <p>
        Пожалуйста, оцените то,
        насколько продукт
        <code class="abbreviation-code"><?= $productToNeed->product->abbreviation; ?></code>
        <code class="name-code"><?= $productToNeed->product->material->name; ?></code>
        удовлетворяет потребность
        <code class="abbreviation-code"><?= $productToNeed->need->abbreviation; ?></code>
        <code class="name-code"><?= $productToNeed->need->name; ?></code>
        потребителя
        <code class="abbreviation-code"><?= $productToNeed->need->consumer->abbreviation; ?></code>
        <code class="name-code"><?= $productToNeed->need->consumer->name; ?></code>
    </p>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'action' => ['/monitoring/effect/industrial/industrial-input'],
        'options' => [
            'autocomplete' => 'off',
        ],
    ]); ?>
    <?= $form->field($effect, 'product_to_need_id')
        ->hiddenInput(['value' => $productToNeed->id])
        ->label(false); ?>
    <?= $form->field($effect, 'value')->input('number', [
        'min' => 0,
        'max' => 1,
        'step' => 0.01,
    ]); ?>
    <?= $form->field($effect, 'date')->input('date'); ?>
    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
            ['encode' => false, 'class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
