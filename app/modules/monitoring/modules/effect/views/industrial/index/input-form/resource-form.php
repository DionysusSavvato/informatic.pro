<?php

use common\models\InnerIndustrialEffect;
use common\models\ProductToResource;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View              $this
 * @var ProductToResource $productToResource
 */

$innerEffect = new InnerIndustrialEffect();

?>

<div class="container-fluid">
    <p>
        Пожалуйста, оцените качество
        <code class="name-code"><?= $productToResource->product->material->name; ?></code>,
        являющегося продуктом
        <code class="abbreviation-code"><?= $productToResource->product->abbreviation; ?></code>
        процесса
        <code class="abbreviation-code"><?= $productToResource->product->function->abbreviation; ?></code>
        <code class="name-code"><?= $productToResource->product->function->name; ?></code>
        и ресурсом
        <code class="abbreviation-code"><?= $productToResource->resource->abbreviation; ?></code>
        в процессе
        <code class="abbreviation-code"><?= $productToResource->resource->function->abbreviation; ?></code>
        <code class="name-code"><?= $productToResource->resource->function->name; ?></code>
    </p>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'action' => ['/monitoring/effect/industrial/inner-industrial-input'],
        'options' => [
            'autocomplete' => 'off',
        ],
    ]); ?>
    <?= $form->field($innerEffect, 'product_to_resource_id')
        ->hiddenInput(['value' => $productToResource->id])
        ->label(false); ?>
    <?= $form->field($innerEffect, 'value')->input('number', [
        'min' => 0,
        'max' => 1,
        'step' => 0.01,
    ]); ?>
    <?= $form->field($innerEffect, 'date')->input('date'); ?>
    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i>  Сохранить',
            ['encode' => false, 'class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
