<?php

use common\models\IndustrialFunction;
use common\models\Product;
use common\models\ProductToNeed;
use common\models\ProductToResource;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var Product            $product
 * @var ProductToNeed      $productToNeed
 * @var ProductToResource  $productToResource
 */

if ($productToNeed) {
    echo $this->render('input-form/need-form', [
        'productToNeed' => $productToNeed,
    ]);
}
else {
    echo $this->render('input-form/resource-form', [
        'productToResource' => $productToResource,
    ]);
}