<?php

use common\models\IndustrialFunction;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 */

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E</span> Эффекты',
        'url' => ['/monitoring/effect'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E<sub>into</sub></span> Эффекты информационного обеспечения',
        'url' => ['/monitoring/effect/information/index'],
    ],
];

$this->title = 'Своевременность информационного обеспечения';

$manager = $function->manager;

?>

<div class="container-fluid">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h3>
                    Своевременность информационного обеспечения
                </h3>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <p>Оцените своевременность информационного обеспечения информационных
                    потребностей <?= $manager->toString() ?>:</p>
                <?= $this->render('timeliness/input-form', [
                    'manager' => $manager,
                    'function' => $function,
                ]); ?>
            </div>
        </div>
    </div>
</div>
