<?php

use common\models\IndustrialFunction;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var array              $result
 * @var string             $date
 */

$this->title = 'Эффекты информационного обеспечения';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E</span> Эффекты',
        'url' => ['/monitoring/effect'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E<sub>into</sub></span> Эффекты информационного обеспечения',
        'url' => ['/monitoring/effect/information/index'],
    ],
];

?>


<div class="container-fluid">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $this->title; ?>
            </h3>
            <div class="box-tools pull-right">
                <a href="<?= Url::to(['/monitoring/effect/industrial/index', 'id' => $function->entity_id]); ?>"
                   class="btn btn-primary btn-block btn-sm"><i class="fa fa-arrow-right"></i> Перейти к производственным
                    эффектам </a>
            </div>
        </div>

        <div class="box-body">
            <div class="container-fluid">

                <div class="form-group">
                    <label>Дата</label>
                    <input
                            type="date"
                            class="form-control"
                            name="date"
                            pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                            disabled
                            value="<?= $date; ?>"
                    >
                </div>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th>Информационный продукт</th>
                        <th>Своевременность</th>
                        <th>Полнота</th>
                        <th>Информационная потребность</th>
                    </tr>
                    <?php foreach ($result as $effectsData): ?>
                        <tr>
                            <td style="font-size: 16px;">
                                <?= $effectsData['timelinessEffect']->
                                informationProductToInformationNeed->
                                informationProduct->toString(); ?>
                            </td>
                            <td>
                                <input
                                        type="number"
                                        class="form-control"
                                        disabled
                                        value="<?= $effectsData['timelinessEffect']->value; ?>"
                                >
                                <p><?= $effectsData['timelinessMessage']->situation->title ?? null; ?></p>
                            </td>
                            <td>
                                <input
                                        type="number"
                                        class="form-control"
                                        disabled
                                        value="<?= $effectsData['completenessEffect']->value ?? 0; ?>"
                                >
                                <p><?= $effectsData['completenessMessage']->situation->title ?? null; ?></p>
                            </td>
                            <td style="font-size: 16px;">
                                <?= $effectsData['timelinessEffect']->
                                informationProductToInformationNeed
                                    ->informationNeed->toString(); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
