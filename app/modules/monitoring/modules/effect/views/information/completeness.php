<?php

use common\models\IndustrialFunction;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var array              $timelinessEffectsData
 * @var string             $date
 */

$this->title = 'Полнота информационного обеспечения';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E</span> Эффекты',
        'url' => ['/monitoring/effect'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E<sub>into</sub></span> Эффекты информационного обеспечения',
        'url' => ['/monitoring/effect/information/index'],
    ],
];

$manager = $function->manager;

?>

<div class="container-fluid">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h3>
                    Полнота информационного обеспечения
                </h3>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <p>Оцените полноту информационного обеспечения информационных
                    потребностей <?= $manager->toString() ?>:</p>
                <?= $this->render('completeness/input-form', [
                    'function' => $function,
                    'timelinessEffectsData' => $timelinessEffectsData,
                    'date' => $date,
                ]) ?>
            </div>
        </div>
    </div>
</div>
