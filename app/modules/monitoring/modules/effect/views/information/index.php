<?php

use app\widgets\industrial_function_hierarchy\IndustrialFunctionHierarchyWidget;
use yii\web\View;


/**
 * @var View $this
 */

$this->title = 'Информационные эффекты';

$this->params['breadcrumbs'] = [
    [
        'label' => '<i class="fa fa-i-cursor"></i>Мониторинг бизнес-процессов',
        'url' => ['/monitoring'],
    ],
    [
        'label' => '<span class="abbreviation-mini">E</span> Эффекты',
        'url' => ['/monitoring/effect'],
    ],
];

?>

<div class="information-effects">
    <div class="box">
        <div class="box-header with-border">
            <div class="container-fluid">
                <h3>
                    Выберите процесс
                </h3>
            </div>
        </div>
        <div class="box-body">
            <div class="container-fluid">
                <?= IndustrialFunctionHierarchyWidget::widget([
                    'baseRoute' => [
                        '/monitoring/effect/information/timeliness',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
