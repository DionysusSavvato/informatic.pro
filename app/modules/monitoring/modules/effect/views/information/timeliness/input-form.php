<?php

use common\models\IndustrialFunction;
use common\models\InformationEffect;
use common\models\InformationEffectType;
use common\models\Manager;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var Manager            $manager
 * @var IndustrialFunction $function
 */

$relations = $manager->informationProductsToInformationNeeds;
$informationEffectClean = new InformationEffect([
    'information_effect_type_id' => InformationEffectType::TIMELINESS,
]);

?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'action' => ['/monitoring/effect/information/completeness', 'id' => $function->entity_id],
    'options' => [
        'autocomplete' => 'off',
    ],
]); ?>
<div class="form-group required">
    <label>Дата</label>
    <input type="date" class="form-control" name="date" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
</div>
<table class="table table-bordered">
    <tbody>
    <tr>
        <th>Информационный продукт</th>
        <th>Своевременность</th>
        <th>Информационная потребность</th>
    </tr>
    <?php foreach ($relations as $index => $informationProductToInformationNeed): ?>
        <?php
        $informationEffect = clone $informationEffectClean;
        $informationEffect->information_product_to_information_need_id = $informationProductToInformationNeed->id;
        ?>
        <tr>
            <td style="font-size: 16px;">
                <?= $informationProductToInformationNeed->informationProduct->toString(); ?>
            </td>
            <td>
                <?= $form->field($informationEffect, "[{$index}]value")
                    ->input('number', [
                        'min' => 0,
                        'max' => 1,
                        'step' => 1,
                    ])
                    ->label(false) ?>
                <?= $form->field($informationEffect, "[{$index}]information_product_to_information_need_id")
                    ->hiddenInput([
                        'value' => $informationEffect->information_product_to_information_need_id,
                    ])
                    ->label(false); ?>
                <?= $form->field($informationEffect, "[{$index}]information_effect_type_id")
                    ->hiddenInput([
                        'value' => $informationEffect->information_effect_type_id,
                    ])
                    ->label(false); ?>
            </td>
            <td style="font-size: 16px;">
                <?= $informationProductToInformationNeed->informationNeed->toString(); ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="form-group">
    <?= Html::submitButton('<i class="fa fa-save"></i>  Отправить',
        ['encode' => false, 'class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>



