<?php

use common\models\IndustrialFunction;
use common\models\InformationEffect;
use common\models\InformationEffectType;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var array              $timelinessEffectsData
 * @var string             $date
 */

$informationEffectClean = new InformationEffect([
    'information_effect_type_id' => InformationEffectType::COMPLETENESS,
    'date' => $date,
]);

?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => false,
    'action' => ['/monitoring/effect/information/finish', 'id' => $function->entity_id],
    'options' => [
        'autocomplete' => 'off',
    ],
]); ?>
<div class="form-group required">
    <label>Дата</label>
    <input
            type="date"
            class="form-control"
            name="date"
            disabled
            value="<?= $date; ?>"
    >
</div>
<table class="table table-bordered">
    <tbody>
    <tr>
        <th>Информационный продукт</th>
        <th>Своевременность</th>
        <th>Полнота</th>
        <th>Информационная потребность</th>
    </tr>
    <?php foreach ($timelinessEffectsData as $index => $timelinessEffectData): ?>
        <?php
        $informationEffect = clone $informationEffectClean;
        $informationEffect->information_product_to_information_need_id =
            $timelinessEffectData['effect']->information_product_to_information_need_id;
        ?>
        <tr>
            <td style="font-size: 16px;">
                <?= $timelinessEffectData['effect']->
                informationProductToInformationNeed->
                informationProduct->toString(); ?>
            </td>
            <td>
                <input
                        type="number"
                        class="form-control"
                        disabled
                        value="<?= $timelinessEffectData['effect']->value; ?>"
                >
                <p><?= $timelinessEffectData['message']->situation->title ?? null; ?></p>
            </td>
            <td>
                <input
                        type="hidden"
                        class="form-control"
                        name="InformationEffect[<?= $index; ?>][timeliness_effect_id]"
                        hidden
                        value="<?= $timelinessEffectData['effect']->id; ?>">
                <input
                        type="hidden"
                        class="form-control"
                        name="InformationEffect[<?= $index; ?>][timeliness_message_id]"
                        hidden
                        value="<?= $timelinessEffectData['message']->id ?? null; ?>">

                <?php if ($timelinessEffectData['message'] === null): ?>
                    <?= $form->field($informationEffect, "[{$index}]value")
                        ->input('number', [
                            'min' => 0,
                            'max' => 1,
                            'step' => 0.01,
                        ])
                        ->label(false) ?>
                    <?= $form->field($informationEffect, "[{$index}]information_product_to_information_need_id")
                        ->hiddenInput([
                            'value' => $informationEffect->information_product_to_information_need_id,
                        ])
                        ->label(false); ?>
                    <?= $form->field($informationEffect, "[{$index}]information_effect_type_id")
                        ->hiddenInput([
                            'value' => $informationEffect->information_effect_type_id,
                        ])
                        ->label(false); ?>
                    <?= $form->field($informationEffect, "[{$index}]date")
                        ->hiddenInput([
                            'value' => $informationEffect->date,
                        ])
                        ->label(false); ?>
                <?php else: ?>
                    <input
                            type="number"
                            class="form-control"
                            disabled
                            value="0"
                    >

                <?php endif; ?>
            </td>
            <td style="font-size: 16px;">
                <?= $timelinessEffectData['effect']->
                informationProductToInformationNeed
                    ->informationNeed->toString(); ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="form-group">
    <?= Html::submitButton('<i class="fa fa-save"></i>  Отправить',
        ['encode' => false, 'class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>

