<?php

return [
    'monitoring/effect' => 'monitoring/effect/default/index',

    'monitoring/effect/industrial' => 'monitoring/effect/industrial/index',
    'monitoring/effect/industrial/input' => 'monitoring/effect/industrial/industrial-input',
    'monitoring/effect/industrial/inner-input' => 'monitoring/effect/industrial/inner-industrial-input',

    'monitoring/effect/information' => 'monitoring/effect/information/index',
    'monitoring/effect/information/timeliness' => 'monitoring/effect/information/timeliness',
    'monitoring/effect/information/completeness' => 'monitoring/effect/information/completeness',
    'monitoring/effect/information/finish' => 'monitoring/effect/information/finish',
];