<?php

namespace app\modules\monitoring\modules\effect\controllers;

use common\models\IndustrialEffect;
use common\models\IndustrialFunction;
use common\models\InnerIndustrialEffect;
use common\models\Product;
use common\models\ProductToNeed;
use common\models\ProductToResource;
use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для ввода оценок производственного эффекта
 */
class IndustrialController extends Controller
{
    /**
     * Форма выбора и ввода оценки производственного эффекта
     * 
     * `monitoring/effect/industrial`
     * 
     * @param int|null $id
     * @param int|null $productID
     * @param int|null $productToResourceID
     * @param int|null $productToNeedID
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(
        int $id = null,
        int $productID = null,
        int $productToResourceID = null,
        int $productToNeedID = null
    ): string {
        return $this->render('index', [
            'function' => IndustrialFunction::findOne($id),
            'product' => Product::findOne($productID),
            'productToResource' => ProductToResource::findOne($productToResourceID),
            'productToNeed' => ProductToNeed::findOne($productToNeedID),
        ]);
    }

    /**
     * Ввод значения производственного эффекта
     * 
     * `/monitoring/effect/industrial/input`
     *
     * @return array|Response
     */
    public function actionIndustrialInput()
    {
        $effect = new IndustrialEffect();
        $effect->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($effect);
        }

        if ($effect->save()) {
            Yii::$app->session->setFlash('success', 'Успешно');
        }

        return $this->redirect(['index']);
    }

    /**
     * Ввод значения внутреннего производственного эффекта
     * 
     * `/monitoring/effect/industrial/inner-input`
     *
     * @return array|Response
     */
    public function actionInnerIndustrialInput()
    {
        $effect = new InnerIndustrialEffect();
        $effect->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($effect);
        }

        if ($effect->save()) {
            Yii::$app->session->setFlash('success', 'Успешно');
        }

        return $this->redirect(['index']);
    }
}