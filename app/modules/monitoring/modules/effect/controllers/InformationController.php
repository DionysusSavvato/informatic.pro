<?php

namespace app\modules\monitoring\modules\effect\controllers;

use Carbon\Carbon;
use common\models\IndustrialFunction;
use common\models\InformationEffect;
use common\models\InformationEffectType;
use common\models\Message;
use common\services\recomendation\CompletenessInformatonEffectAnalysisService;
use common\services\recomendation\TimelinessInformationEffectAnalysisService;
use Throwable;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Контроллер для ввода оценки эффектов информационного обеспечения
 */
class InformationController extends Controller
{
    /**
     * Выбор процесса
     *
     * `monitoring/effect/information`
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    /**
     * Форма для ввода значения эффекта информационного обеспечения
     *
     * `monitoring/effect/information/timeliness`
     *
     * @param int $id Идентификатор процесса
     *
     * @return string|Response
     *
     * @throws InvalidArgumentException
     * @throws NotFoundHttpException
     * @throws InvalidParamException
     */
    public function actionTimeliness(int $id)
    {
        $function = IndustrialFunction::findOrThrow($id);

        if ($function->manager === null) {
            Yii::$app->session->setFlash('warning', 'У выбранного процесса не указано ответственное лицо');
            return $this->redirect(['/monitoring/effect/default/index']);
        }

        return $this->render('timeliness', [
            'function' => $function,
        ]);
    }

    /**
     * Получение данных по своевременности ИО и вывод формы для оценки полноты ИО
     *
     * `monitoring/effect/information/completeness`
     *
     * @param int $id $id Идентификатор процесса
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionCompleteness(int $id): string
    {
        $function = IndustrialFunction::findOrThrow($id);
        $date = Yii::$app->request->post('date');
        $effectsData = Yii::$app->request->post('InformationEffect');
        $effects = [];

        $analysisService = new TimelinessInformationEffectAnalysisService([
            'industrialFunction' => $function,
        ]);

        $transaction = InformationEffect::getDb()->beginTransaction();
        try {
            foreach ($effectsData as $effectData) {
                $effect = new InformationEffect();
                $effect->setAttributes($effectData);
                $effect->date = $date;
                $effect->saveOrThrow();

                $message = $analysisService->analysis($effect);

                $effects[] = [
                    'effect' => $effect,
                    'message' => $message,
                ];
            }
            $transaction->commit();
        }
        catch (Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return $this->render('completeness', [
            'function' => $function,
            'timelinessEffectsData' => $effects,
            'date' => $date,
        ]);
    }

    /**
     * Получение данных по полноте ИО и переход к оценке
     *
     * `monitoring/effect/information/completeness`
     *
     * @param int $id
     *
     * @return string
     *
     * @throws InvalidArgumentException
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function actionFinish(int $id): string
    {
        $function = IndustrialFunction::findOrThrow($id);

        $date = null;
        $effectsData = Yii::$app->request->post('InformationEffect');

        $analysisService = new CompletenessInformatonEffectAnalysisService([
            'industrialFunction' => $function,
        ]);

        $result = [];
        $transaction = InformationEffect::getDb()->beginTransaction();
        try {
            foreach ($effectsData as $effectData) {
                $timelinessEffect = InformationEffect::findOrThrow([
                    'information_effect_type_id' => InformationEffectType::TIMELINESS,
                    'id' => ArrayHelper::getValue($effectData, 'timeliness_effect_id'),
                ]);

                if ($date === null) {
                    $date = Carbon::parse($timelinessEffect->date)->toDateString();
                }

                $timelinessMessage = Message::findOne([
                    'id' => ArrayHelper::getValue($effectData,
                        'timeliness_message_id'),
                ]);


                $completenessEffect = null;
                $completenessMessage = null;
                if ($timelinessEffect->value == 1) {
                    $completenessEffect = new InformationEffect();
                    $completenessEffect->setAttributes($effectData);
                    $completenessEffect->saveOrThrow();
                    $completenessMessage = $analysisService->analysis($completenessEffect);
                }


                $result[] = [
                    'timelinessEffect' => $timelinessEffect,
                    'timelinessMessage' => $timelinessMessage,
                    'completenessEffect' => $completenessEffect,
                    'completenessMessage' => $completenessMessage,
                ];
            }
            $transaction->commit();
        }
        catch (Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return $this->render('finish', [
            'function' => $function,
            'result' => $result,
            'date' => $date,
        ]);
    }
}