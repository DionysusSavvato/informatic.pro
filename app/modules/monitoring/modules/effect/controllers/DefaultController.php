<?php

namespace app\modules\monitoring\modules\effect\controllers;

use yii\base\InvalidParamException;
use yii\web\Controller;

/**
 * Базовый контроллер модуля ввода эффектов
 */
class DefaultController extends Controller
{
    /**
     * Базовая страница модуля
     * 
     * `monitoring/effect`
     *
     * @return string
     *
     * @throws InvalidParamException
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }
}