<?php

namespace app\modules\monitoring;

use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * Модуль мониторинга бизнес-процессов
 */
class MonitoringModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        parent::init();

        $this->controllerNamespace = 'app\modules\monitoring\controllers';

        $this->loadRules();
    }

    /**
     * Загрузка правил роутинга
     */
    private function loadRules(): void
    {
        Yii::$app->urlManager->addRules(
            ArrayHelper::merge(
                require __DIR__ . '/config/rules.php',
                require __DIR__ . '/modules/effect/config/rules.php',
                require __DIR__ . '/modules/expense/config/rules.php',
                require __DIR__ . '/modules/parameter/config/rules.php',
                require __DIR__ . '/modules/change_registrator/config/rules.php'
            )

        );
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}