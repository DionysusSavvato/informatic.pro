<?php

namespace app\widgets\structure;

use common\models\IndustrialFunction;
use yii\base\InvalidParamException;
use yii\base\Widget;

/**
 * Виджет для отрисовки структуры процесса
 *
 * @property IndustrialFunction $industrialFunction Указываемый процесс
 * @property array|null         $urlConfig          Конфигурация URL
 * @property boolean            $openInOtherWindow  Флаг, нужно ли открывать ссылки в отдельном окне
 */
class StructureWidget extends Widget
{
    /** @var IndustrialFunction */
    private $_industrialFunction;

    /** @var bool */
    private $_openInOtherWindow = true;

    /** @var array|null */
    private $_urlConfig = null;

    /**
     * @return string
     *
     * @throws InvalidParamException
     */
    public function run(): string
    {
        return $this->render('structure', [
            'function' => $this->industrialFunction,
            'urlConfig' => $this->urlConfig,
            'openInOtherWindow' => $this->openInOtherWindow,
        ]);
    }

    /**
     * @return IndustrialFunction
     */
    public function getIndustrialFunction(): IndustrialFunction
    {
        return $this->_industrialFunction;
    }

    /**
     * @param IndustrialFunction $industrialFunction
     */
    public function setIndustrialFunction(IndustrialFunction $industrialFunction): void
    {
        $this->_industrialFunction = $industrialFunction;
    }

    /**
     * @return array
     */
    public function getUrlConfig(): ?array
    {
        return $this->_urlConfig;
    }

    /**
     * @param array $urlConfig
     */
    public function setUrlConfig(array $urlConfig): void
    {
        $this->_urlConfig = $urlConfig;
    }

    /**
     * @return bool
     */
    public function getOpenInOtherWindow(): bool
    {
        return $this->_openInOtherWindow;
    }

    /**
     * @param bool $openInOtherWindow
     */
    public function setOpenInOtherWindow(bool $openInOtherWindow): void
    {
        $this->_openInOtherWindow = $openInOtherWindow;
    }
}