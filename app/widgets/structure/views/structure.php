<?php

use common\models\IndustrialFunction;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var array|null         $urlConfig
 * @var boolean            $openInOtherWindow
 */


/** @noinspection MissedViewInspection */
$this->registerCss($this->render('style.css'));
?>
<style>

</style>
<div class="main-container">
    <?= $this->render('process_model', [
        'function' => $function,
        'urlConfig' => $urlConfig,
        'openInOtherWindow' => $openInOtherWindow,
    ]); ?>
    <div class="arrow up"></div>
    <?php if ($function->manager): ?>
        <?= $this->render('manager_model', [
            'manager' => $function->manager,
            'urlConfig' => $urlConfig,
            'openInOtherWindow' => $openInOtherWindow,
        ]) ?>
    <?php endif; ?>
</div>
