<?php

use common\models\InformationNeed;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View            $this
 * @var InformationNeed $informationNeed
 * @var array|null      $urlConfig
 * @var boolean         $openInOtherWindow
 */

?>
<div class="row-container information-products-container">
    <?php foreach ($informationNeed->informationProducts as $informationProduct): ?>
        <div class="column-container">
            <div class="column-container">
                <div class="arrow up"></div>
                <a class="info-box bg-green" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                   href="<?=
                   $urlConfig ? Url::to(ArrayHelper::merge($urlConfig,
                       ['entityID' => $informationProduct->entity_id])) :
                       Url::to([
                           '/repository/information-system/information-product/view',
                           'id' => $informationProduct->entity_id,
                       ]) ?>">
                    <span class="info-box-icon"><?= $informationProduct->abbreviation; ?></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?= $informationProduct->name; ?></span>
                    </div>
                </a>
            </div>

            <div class="column-container">
                <div class="arrow up"></div>
                <a class="info-box bg-green" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                   href="<?=
                   $urlConfig ? Url::to(ArrayHelper::merge($urlConfig,
                       ['entityID' => $informationProduct->informationFunction->entity_id])) :
                       Url::to([
                           '/repository/information-system/default/view',
                           'id' => $informationProduct->informationFunction->entity_id,
                       ]) ?>">
                    <span class="info-box-icon"><?= $informationProduct->informationFunction->abbreviation; ?></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?= $informationProduct->informationFunction->name; ?></span>
                    </div>
                </a>
            </div>

            <div class="row-container">
                <?php foreach ($informationProduct->informationFunction->informationResources as $informationResource): ?>
                    <div class="column-container">
                        <div class="arrow up"></div>
                        <a class="info-box bg-green" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                           href="<?=
                           $urlConfig ? Url::to(ArrayHelper::merge($urlConfig,
                               ['entityID' => $informationResource->entity_id])) :
                               Url::to([
                                   '/repository/information-system/information-resource/view',
                                   'id' => $informationResource->entity_id,
                               ]) ?>">
                            <span class="info-box-icon"><?= $informationResource->abbreviation; ?></span>
                            <div class="info-box-content">
                                <span class="info-box-text"><?= $informationResource->name; ?></span>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

