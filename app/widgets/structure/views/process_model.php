<?php

use common\models\IndustrialFunction;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 * @var array|null         $urlConfig
 * @var boolean            $openInOtherWindow
 */

?>
<div class="row-container process-container">
    <!--РЕСУРСЫ-->
    <div class="column-container resources-container">
        <?php foreach ($function->resources as $resource): ?>
            <div class="row-container resource-container">
                <a class="info-box bg-aqua" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?> href="
                <?= $urlConfig ? Url::to(
                    ArrayHelper::merge($urlConfig, ['entityID' => $resource->material->entity_id]))
                    :
                    Url::to([
                        '/repository/industrial-function/resource/view',
                        'id' => $resource->id,
                    ]) ?>">
                    <span class="info-box-icon"><?= $resource->abbreviation ?></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?= $resource->material->name; ?></span>
                    </div>
                </a>
                <div class="arrow right"></div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row-container function-container">
        <!--ПРОЦЕСС-->
        <a class="info-box bg-aqua" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
           href="<?= $urlConfig ? Url::to(
               ArrayHelper::merge($urlConfig, ['entityID' => $function->entity_id]))
               :
               Url::to([
                   '/repository/industrial-function/default/view',
                   'id' => $function->entity_id,
               ]) ?>">
            <span class="info-box-icon"><?= $function->abbreviation ?></span>
            <div class="info-box-content">
                <span class="info-box-text"><?= $function->name; ?></span>
            </div>
        </a>
        <div class="arrow right"></div>
    </div>


    <!--ПРОДУКТЫ-->
    <div class="column-container products-container">
        <?php
        foreach ($function->products as $product) :
            ?>
            <div class="row-container product-container">
                <a class="info-box bg-aqua" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                   href="<?= $urlConfig ? Url::to(
                       ArrayHelper::merge($urlConfig, ['entityID' => $product->material->entity_id]))
                       :
                       Url::to([
                           '/repository/industrial-function/product/view',
                           'id' => $product->id,
                       ]) ?>">
                    <span class="info-box-icon"><?= $product->abbreviation ?></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?= $product->material->name; ?></span>
                    </div>
                </a>
                <div class="arrow right"></div>
                <div class="column-container" style="align-items: flex-start">
                    <div class="column-container needs-container">
                        <?php foreach ($product->needs as $need): ?>
                            <div class="row-container need-container">
                                <a class="info-box bg-red" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                                   href="<?= $urlConfig ? Url::to(
                                       ArrayHelper::merge($urlConfig, ['entityID' => $need->entity_id]))
                                       :
                                       Url::to([
                                           '/repository/consumer/need/view',
                                           'id' => $need->entity_id,
                                       ]) ?>">
                                    <span class="info-box-icon"><?= $need->abbreviation ?></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= $need->name; ?></span>
                                    </div>
                                </a>
                                <div class="arrow left"></div>
                                <a class="info-box bg-red" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                                   href="<?= $urlConfig ? Url::to(
                                       ArrayHelper::merge($urlConfig, ['entityID' => $need->consumer->entity_id]))
                                       :
                                       Url::to([
                                           '/repository/consumer/default/view',
                                           'id' => $need->consumer->entity_id,
                                       ]) ?>">
                                    <span class="info-box-icon"><?= $need->consumer->abbreviation ?></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= $need->consumer->name; ?></span>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="column-container target-resources-container">
                        <?php foreach ($product->targetResources as $resource): ?>
                            <div class="row-container">
                                <a class="info-box bg-aqua" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                                   href="<?=
                                   $urlConfig ? Url::to(
                                       ArrayHelper::merge($urlConfig, ['entityID' => $resource->material->entity_id]))
                                       :
                                       Url::to([
                                           '/repository/industrial-function/resource/view',
                                           'id' => $resource->id,
                                       ]) ?>">
                                    <span class="info-box-icon"><?= $resource->abbreviation ?></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= $resource->material->name; ?></span>
                                    </div>
                                </a>
                                <div class="arrow right"></div>
                                <a class="info-box bg-aqua" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                                   href="<?= $urlConfig ? Url::to(
                                       ArrayHelper::merge($urlConfig, ['entityID' => $resource->function->entity_id]))
                                       :
                                       Url::to([
                                           '/repository/industrial-function/default/view',
                                           'id' => $resource->function->entity_id,
                                       ]) ?>">
                                    <span class="info-box-icon"><?= $resource->function->abbreviation ?></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= $resource->function->name; ?></span>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
