<?php

use common\models\Manager;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View       $this
 * @var Manager    $manager
 * @var array|null $urlConfig
 * @var boolean    $openInOtherWindow
 */

?>

<div class="column-container control-system-container">
    <div class="row-container manager-container" style="width: 100%">
        <a class="info-box bg-yellow" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
           href="<?=
           $urlConfig ? Url::to(ArrayHelper::merge($urlConfig, ['entityID' => $manager->entity_id])) :
               Url::to([
                   '/repository/management/default/view',
                   'id' => $manager->entity_id,
               ]) ?>">
            <span class="info-box-icon"><?= $manager->abbreviation ?></span>
            <div class="info-box-content">
                <span class="info-box-text"><?= $manager->name; ?></span>
            </div>
        </a>
    </div>
    <div class="row-container" style="align-items: flex-start;">
        <?php foreach ($manager->informationNeeds as $informationNeed): ?>
            <div class="column-container">
                <div class="column-container">
                    <div class="arrow down"></div>
                    <a class="info-box bg-yellow" <?= $openInOtherWindow ? 'target="_blank"' : ''; ?>
                       href="<?=
                       $urlConfig ? Url::to(ArrayHelper::merge($urlConfig,
                           ['entityID' => $informationNeed->entity_id])) :
                           Url::to([
                               '/repository/management/information-need/view',
                               'id' => $informationNeed->entity_id,
                           ]) ?>">
                        <span class="info-box-icon"><?= $informationNeed->abbreviation; ?></span>
                        <div class="info-box-content">
                            <span class="info-box-text"><?= $informationNeed->name; ?></span>
                        </div>
                    </a>
                    <?= $this->render('information-model', [
                        'informationNeed' => $informationNeed,
                        'urlConfig' => $urlConfig,
                        'openInOtherWindow' => $openInOtherWindow,
                    ]) ?>
                </div>
            </div>

        <?php endforeach; ?>
    </div>
</div>
