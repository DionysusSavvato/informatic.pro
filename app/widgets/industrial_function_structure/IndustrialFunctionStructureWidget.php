<?php

namespace app\widgets\industrial_function_structure;

use yii\base\InvalidArgumentException;
use yii\base\Widget;

/**
 * Class IndustrialFunctionStructureWidget
 */
class IndustrialFunctionStructureWidget extends Widget
{
    /**
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function run(): string
    {
        return $this->render('widget');
    }
}