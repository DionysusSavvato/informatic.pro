VisualizatorApp.controller('MainController',
    [
        '$scope',
        '$http',
        'Api',
        'IndustrialFunctionModel',
        function ($scope, $http, Api, IndustrialFunctionModel) {

            /** @type {IndustrialFunctionModel|null} */
            $scope.parent = null;

            /** @type {IndustrialFunctionModel|null} */
            $scope.currentFunction = null;

            /** @type {IndustrialFunctionModel[]} */
            $scope.subFunctions = [];

            let plumbInstance = jsPlumb.getInstance({
                Connector: "StateMachine",
                PaintStyle: {strokeWidth: 3, stroke: "blue"},
                Endpoint: ["Dot", {radius: 1}],
                EndpointStyle: {fill: "blue"},
                Container: "canvas"
            });

            $scope.attempt = function () {
                setTimeout(function () {
                    let currentFunctionItem = document.getElementById("currentFunction");
                    let subFunctionsItems = document.querySelectorAll(".subFunction");

                    let draggable = document.querySelectorAll(".draggable");
                    draggable.forEach(function (item) {
                        item.style.position = "absolute";
                    });

                    plumbInstance.draggable(draggable);

                    let connect = function (source, target, color) {
                        plumbInstance.connect({
                            source: source,
                            target: target,
                            anchors: [
                                [
                                    "Perimeter",
                                    {
                                        shape: source.getAttribute("data-shape"),
                                        rotation: source.getAttribute("data-rotation")
                                    }
                                ],
                                [
                                    "Perimeter",
                                    {
                                        shape: target.getAttribute("data-shape"),
                                        rotation: target.getAttribute("data-rotation")
                                    }
                                ]
                            ],
                            paintStyle: {strokeWidth: 3, stroke: color},
                            overlays: [
                                [
                                    "Arrow",
                                    {
                                        width: 12,
                                        length: 12,
                                        location: 1,
                                        paintStyle: {strokeWidth: 3, stroke: color, fill: color},
                                    }
                                ]
                            ]
                        });
                    };

                    plumbInstance.batch(function () {
                        subFunctionsItems.forEach(function (subFunction) {
                            connect(currentFunctionItem, subFunction, "blue");
                        });
                        $scope.subFunctions.forEach(function (subFunction) {
                            let subFunctionElement = document.getElementById("function-" + subFunction.entity_id);
                            subFunction.dependentFunctions.forEach(function (dependentFunction) {
                                let dependentFunctionElement = document.getElementById("function-" + dependentFunction.entity_id);
                                if (dependentFunctionElement != null) {
                                    connect(subFunctionElement, dependentFunctionElement, "red");
                                }
                            });
                        });
                    });
                }, 10);

                return true;
            };

            /**
             * Инициализация приложения, загружаем корневой процесс
             */
            Api.getRoot().then(function (response) {
                $scope.currentFunction = IndustrialFunctionModel.build(response.data);

                response.data.subFunctions.forEach(function (subFunctionData) {
                    $scope.subFunctions.push(IndustrialFunctionModel.build(subFunctionData));
                });

            });

            /**
             * Выбор текущего процесса
             * @param entity_id Идентификатор процесса
             */
            $scope.selectFunction = function (entity_id) {
                plumbInstance.reset();
                Api.get(entity_id).then(function (response) {

                    $scope.currentFunction = IndustrialFunctionModel.build(response.data);

                    $scope.parent = response.data.parent;

                    $scope.subFunctions = [];

                    response.data.subFunctions.forEach(function (subFunctionData) {
                        $scope.subFunctions.push(IndustrialFunctionModel.build(subFunctionData));
                    });
                });
            };
        }
    ]
);

