VisualizatorApp.service('Api',
    [
        '$http',
        function ($http) {
            this.getRoot = function () {
                return $http.get('/api/visualizator/root');
            };

            this.get = function (industrialFunctionID) {
                return $http.get('/api/visualizator/' + industrialFunctionID);
            };
        }
    ]
);