VisualizatorApp.factory('IndustrialFunctionModel',
    [
        function () {
            function IndustrialFunctionModel(entity_id, abbreviation, name, dependentFunctions) {
                this.entity_id = entity_id;
                this.abbreviation = abbreviation;
                this.name = name;
                this.dependentFunctions = dependentFunctions ? dependentFunctions : [];
            }

            IndustrialFunctionModel.build = function (data) {
                if (data == null) {
                    return null;
                }
                return new IndustrialFunctionModel(data.entity_id, data.abbreviation, data.name, data.dependentFunctions);
            };

            return IndustrialFunctionModel;
        }
    ]
);