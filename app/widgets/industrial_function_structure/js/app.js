let VisualizatorApp = angular.module('VisualizatorApp', []);

VisualizatorApp.run(function run($http) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $('meta[name="token"]').attr("content");
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
});