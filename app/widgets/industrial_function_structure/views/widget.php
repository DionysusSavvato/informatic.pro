<?php

use app\widgets\industrial_function_structure\assets\IndustrialFunctionStructureAsset;
use yii\web\View;

/**
 * @var View $this
 */

IndustrialFunctionStructureAsset::register($this);

?>

<div ng-app="VisualizatorApp">
    <div class="workspace" style="height: 600px;" ng-controller="MainController">
        <div class="header">
            <button
                    ng-click="selectFunction(parent.entity_id)"
                    class="btn btn-default"
                    ng-if="parent != null">
                К процессу верхнего уровня
            </button>
            <h4 ng-bind="currentFunction.name"></h4>
        </div>
        <div class="canvas" style="margin-top: 20px; margin-bottom: 40px">
            <div class="row" style="display: flex; justify-content: center;">
                <div id="currentFunction" class="item" data-shape="Rectangle">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            {{currentFunction.abbreviation}} {{currentFunction.name}}
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <a href="/repository/structure/{{currentFunction.entity_id}}"
                               target="_blank"
                               class="btn btn-success btn-xs">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div ng-repeat="subFunction in subFunctions" ng-init="$last && attempt()">
                        <div class="item draggable subFunction"
                             id="function-{{subFunction.entity_id}}"
                             data-shape="Rectangle"
                             style="top: 400px; left: {{$index * 170 + 250}}px"
                        >
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="font-size: 12px;">
                                    {{subFunction.abbreviation}} {{subFunction.name}}
                                </div>
                                <div class="panel-footer" style="text-align: right">
                                    <button ng-click="selectFunction(subFunction.entity_id)"
                                            class="btn btn-success btn-xs">
                                        <small class="fa fa-download"></small>
                                    </button>
                                    <a href="/repository/structure/{{subFunction.entity_id}}"
                                       target="_blank"
                                       class="btn btn-success btn-xs">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

