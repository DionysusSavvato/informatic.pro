<?php

namespace app\widgets\industrial_function_structure\assets;


use app\assets\AngularAsset;
use yii\web\AssetBundle;

class IndustrialFunctionStructureAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/industrial_function_structure';

    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/app.js',
        'js/services/IndustrialFunctionModel.js',
        'js/services/Api.js',
        'js/controllers/MainController.js',
    ];

    public $depends = [
        AngularAsset::class,
    ];
}