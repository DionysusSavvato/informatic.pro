<?php

namespace app\widgets\industrial_function_hierarchy;

use app\widgets\industrial_function_hierarchy\helpers\HierarchyHelper;
use common\models\IndustrialFunction;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\ViewNotFoundException;
use yii\base\Widget;

/**
 * Виджет для отрисовки иерархии производственных процессов
 *
 * @property array $baseRoute Конфигурация URL
 */
class IndustrialFunctionHierarchyWidget extends Widget
{
    /** @var array */
    private $_baseRoute;

    /**
     * @return string
     *
     * @throws ViewNotFoundException
     * @throws InvalidCallException
     * @throws InvalidParamException
     * @throws InvalidConfigException
     */
    public function run(): string
    {
        return $this->render('industrial_function_hierarchy', [
            'items' => $this->getTreeItems(),
        ]);
    }

    /**
     * Получение конфигураций дерева
     *
     * @return array
     *
     * @throws ViewNotFoundException
     * @throws InvalidCallException
     * @throws InvalidConfigException
     */
    protected function getTreeItems(): array
    {
        $items = [];

        $rootFunctions = IndustrialFunction::find()
            ->root()
            ->all();

        foreach ($rootFunctions as $rootFunction) {
            $items[] = HierarchyHelper::getItemConfig(
                $rootFunction,
                $this->baseRoute
            );
        }

        return $items;
    }

    /**
     * @return array
     */
    public function getBaseRoute(): array
    {
        return $this->_baseRoute;
    }

    /**
     * @param array $baseRoute
     */
    public function setBaseRoute(array $baseRoute): void
    {
        $this->_baseRoute = $baseRoute;
    }
}