<?php

namespace app\widgets\industrial_function_hierarchy\helpers;

use common\models\IndustrialFunction;
use Yii;
use yii\base\InvalidCallException;
use yii\base\ViewNotFoundException;
use yii\helpers\ArrayHelper;

/**
 * Хелпер для работы с иерархией производственных функций
 */
class HierarchyHelper
{
    /**
     * Формирование массива иерархии процессов для виджета Menu
     *
     * @param IndustrialFunction $industrialFunction
     * @param array              $route
     *
     * @return array
     *
     * @throws ViewNotFoundException
     * @throws InvalidCallException
     */
    public static function getItemConfig(IndustrialFunction $industrialFunction, array $route): array
    {
        $item = [
            'label' => Yii::$app->view->render(
                '@app/widgets/industrial_function_hierarchy/views/item',
                [
                    'function' => $industrialFunction,
                ]
            ),
            'encode' => false,
            'url' => ArrayHelper::merge($route, ['id' => $industrialFunction->entity_id,]),
            'options' => [
                'style' => ['margin-top' => '5px', 'margin-bottom' => '5px'],
            ],
        ];
        $items = [];
        if ($industrialFunction->subFunctions) {
            foreach ($industrialFunction->subFunctions as $subFunction) {
                $items[] = static::getItemConfig($subFunction, $route);
            }
        }
        return ArrayHelper::merge($item, ['items' => $items]);
    }
}