<?php

use common\models\IndustrialFunction;
use yii\web\View;

/**
 * @var View               $this
 * @var IndustrialFunction $function
 */

$this->registerCss($this->render('item-style.css'), [], 'item-style')

?>

<div class="function-container">
    <div class="function">
        <div class="function-abbreviation"><?= $function->abbreviation; ?></div>
        <div class="function-name"><?= $function->name; ?></div>
    </div>
    <?php if ($function->manager): ?>
        <div class="arrow left"></div>
        <div class="manager">
            <div class="manager-abbreviation"><?= $function->manager->abbreviation; ?></div>
            <div class="manager-name"><?= $function->manager->name; ?></div>
        </div>
    <?php endif; ?>
</div>