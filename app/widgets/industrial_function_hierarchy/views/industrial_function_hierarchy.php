<?php

use yii\web\View;
use yii\widgets\Menu;

/**
 * @var View $this
 */

$this->registerCss(
    $this->render('style.css')
);
?>
<div class="container-fluid">
    <?= Menu::widget([
        'items' => $items,
        'linkTemplate' => '<a href="{url}" style="display: inline-block;">{label}</a>',
        'options' => [
            'class' => 'tree',
        ],
    ]) ?>
</div>
