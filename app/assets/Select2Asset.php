<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Select2 Asset Bundle
 */
class Select2Asset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins/select2';

    public $css = [
        'select2.css',
    ];
    public $js = [
        'select2.js',
        'i18n/ru.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}