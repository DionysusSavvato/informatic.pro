<?php

namespace app\assets;


use yii\web\AssetBundle;

/**
 * AngularJS Asset Bundle
 */
class AngularAsset extends AssetBundle
{
    public $sourcePath = '@bower/angular';

    public $js = [
        YII_ENV_DEV ? 'angular.js' : 'angular.min.js',
    ];
}