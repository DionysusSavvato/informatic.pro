<?php

namespace app\assets;

use yii\web\AssetBundle;

class ChartJsAsset extends AssetBundle
{
    public $sourcePath = '@vendor/nnnick/chartjs/dist';

    public $js = [
        YII_ENV_DEV ? 'Chart.bundle.js' : 'Chart.bundle.min.js',
    ];
}