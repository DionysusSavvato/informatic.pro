<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * JsPlumbAsset Asset Bundle
 *
 */
class JsPlumbAsset extends AssetBundle
{
    public $sourcePath = '@bower/jsplumb/dist/js';

    public $js = [
        YII_ENV_DEV ? 'jsplumb.js' : 'jsplumb.min.js',
    ];
}