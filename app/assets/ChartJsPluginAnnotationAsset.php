<?php

namespace app\assets;

use yii\web\AssetBundle;

class ChartJsPluginAnnotationAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/chartjs-plugin-annotation';

    public $js = [
        'chartjs-plugin-annotation.js',
    ];

    public $depends = [
        ChartJsAsset::class,
    ];
}