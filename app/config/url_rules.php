<?php
/**
 * Здесь хранятся Url rules для приложения backend
 */
return [
    /** Роутинг MainController */
    '' => 'main/index',
    '<action:(login|logout)>' => 'main/<action>',
];