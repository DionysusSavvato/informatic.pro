<?php

use common\models\User;
use dmstr\web\AdminLteAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\log\FileTarget;
use yii\web\JqueryAsset;

return [
    'assetManager' => [
        'bundles' => [
            AdminLteAsset::class => [
                'skin' => 'skin-blue',
            ],
            JqueryAsset::class => [
                'js' => [
                    YII_DEBUG ? 'jquery.js' : 'jquery.min.js',
                ],
            ],
            BootstrapAsset::class => [
                'css' => [
                    YII_DEBUG ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                ],
            ],
            BootstrapPluginAsset::class => [
                'js' => [
                    YII_DEBUG ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                ],
            ],
        ],
        'forceCopy' => YII_DEBUG,
    ],
    'request' => [
        'csrfParam' => '_csrf_informatic',
        'cookieValidationKey' => '3hdXUgw48oNkDNkEx8V1OtjrkN9FI1u0',
    ],
    'user' => [
        'identityClass' => User::class,
        'enableAutoLogin' => true,
        'identityCookie' => [
            'name' => '_identity_informatic_pro',
            'httpOnly' => true,
        ],
        'loginUrl' => ['main/login'],
    ],
    'session' => [
        // this is the name of the session cookie used for login on the backend
        'name' => 'informatic_pro_app',
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => FileTarget::class,
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'errorHandler' => [
        'errorAction' => 'main/error',
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => require_once(__DIR__ . '/url_rules.php'),
    ],
];