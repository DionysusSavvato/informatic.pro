<?php

use app\modules\admin\AdminModule;
use app\modules\efficiency\EfficiencyModule;
use app\modules\monitoring\modules\change_registrator\ChangeRegistratorModule;
use app\modules\monitoring\modules\effect\EffectModule;
use app\modules\monitoring\modules\expense\ExpenseModule;
use app\modules\monitoring\modules\parameter\ParameterModule;
use app\modules\monitoring\MonitoringModule;
use app\modules\recomendation\RecomendationModule;
use app\modules\repository\modules\consumer\ConsumerModule;
use app\modules\repository\modules\industrial_function\IndustrialFunctionModule;
use app\modules\repository\modules\information_system\InformationSystemModule;
use app\modules\repository\modules\management\ManagementModule;
use app\modules\repository\RepositoryModule;

return [
    'admin' => [
        'class' => AdminModule::class,
    ],
    'repository' => [
        'class' => RepositoryModule::class,
        'modules' => [
            'consumer' => [
                'class' => ConsumerModule::class,
            ],
            'industrial-function' => [
                'class' => IndustrialFunctionModule::class,
            ],
            'management' => [
                'class' => ManagementModule::class,
            ],
            'information-system' => [
                'class' => InformationSystemModule::class,
            ],
        ],
    ],
    'monitoring' => [
        'class' => MonitoringModule::class,
        'modules' => [
            'effect' => [
                'class' => EffectModule::class,
            ],
            'expense' => [
                'class' => ExpenseModule::class,
            ],
            'parameter' => [
                'class' => ParameterModule::class,
            ],
            'change-registrator' => [
                'class' => ChangeRegistratorModule::class,
            ],
        ],
    ],
    'recomendation' => [
        'class' => RecomendationModule::class,
    ],
    'efficiency' => [
        'class' => EfficiencyModule::class,
    ],
];