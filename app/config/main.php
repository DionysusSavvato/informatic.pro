<?php

use yii\helpers\ArrayHelper;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

$config = [
    'id' => 'app',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\controllers',
    'bootstrap' => [
        'log',
        'admin',
        'repository',
        'monitoring',
        'recomendation',
        'efficiency',
    ],
    'modules' => require __DIR__ . '/modules.php',
    'components' => require __DIR__ . '/components.php',
    'defaultRoute' => 'main',
    'params' => $params,
];

if (YII_DEBUG) {
    $config = ArrayHelper::merge(
        $config,
        require __DIR__ . '/dev_config.php'
    );
}

return $config;