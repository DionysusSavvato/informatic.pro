<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $content string */
?>

<header class="main-header">

    <?=
    Html::a(
        '<span class="logo-mini">I.PRO</span><span class="logo-lg">' . Yii::$app->name . '</span>',
        Yii::$app->homeUrl, ['class' => 'logo'])
    ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle">
                        <span class="hidden-xs">
                            <b>
                                <?=
                                /** @noinspection PhpUndefinedFieldInspection */
                                Yii::$app->user->identity->username
                                ?>
                            </b>
                        </span>
                    </a>
                </li>
                <li>
                    <?= Html::a(
                        '<i class="fa fa-sign-out" style="font-size: 18px;"></i>',
                        ['/main/logout'],
                        ['data-method' => 'post']
                    ) ?>
                </li>
            </ul>
        </div>
    </nav>
</header>
