<?php

use dmstr\widgets\Alert;
use yii\widgets\Breadcrumbs;

/**
 * @var $content
 */
?>
<div class="content-wrapper">
    <section class="content-header">
        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'encodeLabels' => false,
                'homeLink' => [
                    'label' => '<i class="fa fa-dashboard"></i> Главная',
                    'url' => ['/'],
                ],
            ]
        ) ?>
        <br>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<?php
if (YII_ENV_PROD):
    ?>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Версия</b> <?= Yii::$app->version ?>
        </div>
        <strong>
            ИГЭУ, Кафедра информационных технологий
        </strong>
    </footer>

<?php
endif;
?>
