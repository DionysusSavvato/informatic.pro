<?php

use common\models\InformationChange;
use dmstr\widgets\Menu;
use yii\helpers\ArrayHelper;

$debugItems = [
    [
        'label' => 'Gii',
        'icon' => 'file-code-o',
        'url' => ['/gii'],
    ],
    [
        'label' => 'Debug',
        'icon' => 'dashboard',
        'url' => ['/debug'],
    ],
];

$modelChangesNeeded = InformationChange::find()->modelChangesNeeded()->exists();
$warningMessage = $modelChangesNeeded ? '<i class="fa fa-exclamation-circle" style="color: #c92b27;" title="Требуются правки"></i>' : '';

?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?= Menu::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu'],
                'items' => ArrayHelper::merge(
                    [
                        [
                            'label' => "Репозиторий {$warningMessage}",
                            'icon' => 'cogs',
                            'url' => ['/repository'],
                        ],
                        [
                            'label' => 'Мониторинг',
                            'icon' => 'i-cursor',
                            'url' => ['/monitoring'],
                        ],
                        [
                            'label' => 'Рекомендации',
                            'icon' => 'exclamation-triangle',
                            'url' => ['/recomendation'],
                        ],
                        [
                            'label' => 'Эффективность ИО',
                            'icon' => 'line-chart',
                            'url' => ['/efficiency'],
                        ],
                        [
                            'label' => 'Пользователи',
                            'icon' => 'users',
                            'url' => ['/admin'],
                        ],
                    ],
                    YII_DEBUG ? $debugItems : []),
            ]
        ) ?>
    </section>
</aside>
