<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<section class="content">

    <div class="error-page">
        <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>
        <div class="error-content">
            <h3><?= $name ?></h3>
            <p>
                <?= nl2br(Html::encode($message)) ?>
            </p>
            <p>
                Ошибка произошла пока веб-сервер обрабатывал ваш запрос.
                Пожалуйста, свяжитесь с разработчиком, если вы считаете, что это серверная ошибка. Спасибо.
            </p>
            <p>
                <a href="mailto:d.savvato@gmail.com">d.savvato@gmail.com</a>
            </p>
            <p>
                Тем временем, вы можете
                <a href='<?= Yii::$app->homeUrl ?>'>вернуться на главную страницу</a>
                .
            </p>
        </div>
    </div>
</section>
