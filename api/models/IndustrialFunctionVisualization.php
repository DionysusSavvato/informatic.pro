<?php

namespace api\models;

use common\models\IndustrialFunction;
use common\queries\IndustrialFunctionQuery;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class IndustrialFunctionVisualization extends IndustrialFunction
{
    /**
     * @return IndustrialFunctionQuery
     *
     * @throws InvalidConfigException
     */
    public static function find(): IndustrialFunctionQuery
    {
        return parent::find()
            ->with([
                'parent',
                'subFunctions',
            ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return ArrayHelper::merge(parent::fields(), [
            'parent' => 'parent',
            'subFunctions' => 'subFunctions',
            'dependentFunctions' => 'dependentIndustrialFunctions',
        ]);
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getSubFunctions(): IndustrialFunctionQuery
    {
        return $this->hasMany(static::class, ['parent_id' => 'entity_id']);
    }

    /**
     * @return IndustrialFunctionQuery|ActiveQuery
     */
    public function getDependentIndustrialFunctions(): IndustrialFunctionQuery
    {
        return $this->hasMany(static::class, ['entity_id' => 'function_id'])
            ->via('resourcesOfDependentFunctions');
    }
}