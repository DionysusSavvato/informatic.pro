<?php

namespace api\controllers;

use Yii;
use yii\rest\Controller;

/**
 * Class MainController
 *
 * @package api\controllers
 */
class MainController extends Controller
{
    /**
     * Информация о проекте
     *
     * `GET api/info`
     *
     * @return array
     */
    public function actionInfo(): array
    {
        return [
            'project' => Yii::$app->name,
            'version' => Yii::$app->version,
            'environment' => YII_ENV,
        ];
    }
}