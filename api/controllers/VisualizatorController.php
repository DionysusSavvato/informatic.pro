<?php

namespace api\controllers;

use api\models\IndustrialFunctionVisualization;
use yii\base\InvalidConfigException;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class VisualizatorController extends Controller
{
    /**
     * Вывод корневого процесса
     *
     * `GET api/visualizator/root`
     *
     * @return IndustrialFunctionVisualization|null
     *
     * @throws InvalidConfigException
     */
    public function actionRootFunction(): ?IndustrialFunctionVisualization
    {
        return IndustrialFunctionVisualization::find()->root()->one();
    }

    /**
     * Вывод процесса
     *
     * `GET api/visualizator/{id}`
     *
     * @param int $id
     *
     * @return IndustrialFunctionVisualization
     *
     * @throws NotFoundHttpException
     */
    public function actionFunction(int $id): IndustrialFunctionVisualization
    {
        return IndustrialFunctionVisualization::findOrThrow($id);
    }
}