<?php

namespace api\controllers;

use common\models\Scale;
use common\models\ScaleValue;
use Exception;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * REST-контроллер для управления шкалами
 *
 * * `GET,HEAD api/scale` - Список шкал
 * * `GET,HEAD api/scale/{id}` - Шкала по id
 * * `POST api/scale` - Создание шкалы
 * * `POST api/scale/{id}/value` - Добавление значения шкалы
 * * `PUT,PATCH api/scale/{id}` - Изменение шкалы
 * * `PUT,PATCH api/scale/value/{id}` - Изменение значения шкалы
 * * `DELETE api/scale/{id}` - Удаление шкалы
 * * `DELETE api/scale/value/{id}` - Удаление значения шкалы
 */
class ScaleController extends ActiveController
{

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'authenticator' => [
                'class' => HttpBearerAuth::class,
            ],

            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function init(): void
    {
        $this->modelClass = Scale::class;
        parent::init();
    }

    /**
     * Добавление значения шкалы
     *
     * `POST api/scale/{id}/value`
     *
     * @param int $id Идентификатор шкалы
     *
     * @return ScaleValue
     *
     * @throws BadRequestHttpException
     */
    public function actionCreateValue(int $id): ScaleValue
    {
        $scale = Scale::findOne($id);
        if ($scale === null) {
            throw new BadRequestHttpException("Шкала {$id} не найдена.");
        }
        $scaleValue = new ScaleValue(Yii::$app->request->post());
        $scaleValue->scale_id = $scale->scale_id;
        $scaleValue->save();

        return $scaleValue;
    }

    /**
     * Изменение значения шкалы
     *
     * `PUT,PATCH api/scale/value/{id}`
     *
     * @param int $id Идентификатор значения шкалы
     *
     * @return ScaleValue
     *
     * @throws BadRequestHttpException
     */
    public function actionUpdateValue(int $id): ScaleValue
    {
        $scaleValue = ScaleValue::findOne($id);
        if ($scaleValue === null) {
            throw new BadRequestHttpException("Значение шкалы {$id} не найдено");
        }
        $scaleValue->setAttributes(Yii::$app->request->post());
        $scaleValue->save();

        return $scaleValue;
    }

    /**
     * Удаление значения шкалы
     *
     * `DELETE api/scale/value/{id}`
     *
     * @param int $id Идентификатор значения шкалы
     *
     * @throws BadRequestHttpException
     * @throws Exception
     * @throws StaleObjectException
     */
    public function actionDeleteValue(int $id): void
    {
        $scaleValue = ScaleValue::findOne($id);
        if ($scaleValue === null) {
            throw new BadRequestHttpException("Значение шкалы {$id} не найдено");
        }
        $scaleValue->delete();

        Yii::$app->response->statusCode = 204;
    }
}