<?php

namespace api\modules\monitoring;

use Yii;
use yii\base\Module;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Модуль API мониторинга бизнес-процессов
 */
class MonitoringModule extends Module
{
    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        parent::init();
        Yii::$app->urlManager->addRules(require __DIR__ . '/config/rules.php');
    }


    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'authenticator' => [
                'class' => HttpBearerAuth::class,
            ],
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
}