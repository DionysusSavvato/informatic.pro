<?php

namespace api\modules\monitoring\controllers;

use common\models\Parameter;
use common\models\ParameterValue;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

/**
 * API-контроллер для записи значений параметров
 *
 * `GET,HEAD /monitoring/parameter/entity/{id}` - Список параметров сущности
 * `POST /monitoring/parameter/value` - Запись значения параметра сущности
 * `DELETE /monitoring/parameter/value/{id}` - Удаление значения параметра сущности
 */
class ParameterController extends ActiveController
{
    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        $this->modelClass = ParameterValue::class;
        parent::init();
    }

    /**
     * Список параметров сущности
     *
     * `GET,HEAD /monitoring/parameter/entity/{id}`
     *
     * @param int $id
     *
     * @return ActiveDataProvider
     */
    public function actionIndex(int $id): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => Parameter::find()->where(['$entity_id' => $id])
        ]);
    }
}