<?php

use yii\rest\UrlRule;

return [
    [
        'class' => UrlRule::class,
        'patterns' => [
            'GET,HEAD entity/{id}' => 'index',
            'POST value' => 'create',
            'DELETE value/{id}' => 'delete'
        ],
        'tokens' => [
            '{id}' => '<id:\\d+>',
        ],
        'controller' => [
            'monitoring/parameter' => 'monitoring/parameter',
        ],
    ]
];