<?php

namespace api\modules\consumer\models;

use common\models\Consumer as CommonConsumer;

/**
 * Модель покупателя для API
 */
class Consumer extends CommonConsumer
{
    /**
     * {@inheritDoc}
     */
    public function extraFields(): array
    {
        return [
            'needs' => 'needs',
            'parameters' => 'parameters',
        ];
    }
}