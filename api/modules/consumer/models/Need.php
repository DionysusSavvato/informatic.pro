<?php

namespace api\modules\consumer\models;

use common\models\Need as CommonNeed;

/**
 * Модель потребности для API
 */
class Need extends CommonNeed
{
    /**
     * {@inheritDoc}
     */
    public function extraFields(): array
    {
        return [
            'consumers' => 'consumers',
            'products' => 'products',
            'parameters' => 'parameters',
        ];
    }
}