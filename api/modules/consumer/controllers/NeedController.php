<?php

namespace api\modules\consumer\controllers;

use api\modules\consumer\models\Need;
use yii\rest\ActiveController;

/**
 * Контроллер для управления потребностями
 *
 * * `GET,HEAD api/consumer/need` - Список потребнотей
 * * `GET,HEAD api/consumer/need/{id}` - Потребность
 * * `POST api/consumer/need` - Создать потребность
 * * `PUT,PATCH api/consumer/need/{id}` - Редактировать потребность
 * * `DELETE api/consumer/need/{id}` - Удалить потребность
 */
class NeedController extends ActiveController
{
    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        $this->modelClass = Need::class;
        parent::init();
    }
}