<?php

namespace api\modules\consumer\controllers;

use api\modules\consumer\models\Consumer;
use yii\rest\ActiveController;

/**
 * Контроллер для управления списком потребителей
 *
 * * `GET,HEAD api/consumer` - Список потребителей
 * * `GET,HEAD api/consumer/{id}` - Потребитель
 * * `POST api/consumer` - Создание потребителя
 * * `PUT,PATCH api/consumer/{id}` - Редактировать потребителя
 * * `DELETE api/consumer/{id}` - Удалить потребтеля
 */
class DefaultController extends ActiveController
{
    /**
     * {@inheritDoc}
     */
    public function init(): void
    {
        $this->modelClass = Consumer::class;
        parent::init();
    }
}