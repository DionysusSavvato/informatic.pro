<?php

use yii\rest\UrlRule;

return [
    [
        'class' => UrlRule::class,
        'patterns' => [
            'GET,HEAD' => 'index',
            'GET,HEAD {id}' => 'view',
            'POST' => 'create',
            'PUT,PATCH {id}' => 'update',
            'DELETE {id}' => 'delete',
        ],
        'tokens' => [
            '{id}' => '<id:\\d+>',
        ],
        'controller' => [
            'consumer' => 'consumer/default',
        ],
    ],
    [
        'class' => UrlRule::class,
        'patterns' => [
            'GET,HEAD' => 'index',
            'GET,HEAD {id}' => 'view',
            'POST' => 'create',
            'PUT,PATCH {id}' => 'update',
            'DELETE {id}' => 'delete',
        ],
        'tokens' => [
            '{id}' => '<id:\\d+>',
        ],
        'controller' => [
            'consumer/need' => 'consumer/need',
        ],
    ],
];