<?php

use api\modules\consumer\ConsumerModule;
use api\modules\monitoring\MonitoringModule;
use yii\debug\Module as DebugModule;
use yii\gii\Module as GiiModule;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

$config = [
    'id' => 'informatic-rest.pro',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'consumer',
    ],
    'controllerNamespace' => 'api\controllers',
    'modules' => [
        'consumer' => [
            'class' => ConsumerModule::class,
        ],
        'monitoring' => [
            'class' => MonitoringModule::class,
        ],
    ],
    'components' => require 'components.php',
    'params' => $params,
];

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => DebugModule::class,
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => GiiModule::class,
    ];
}

return $config;