<?php

use common\models\User;
use yii\log\FileTarget;
use yii\web\Response;

return [
    'cache' => null,
    'request' => [
        'enableCookieValidation' => false,
        'enableCsrfCookie' => false,
    ],
    'response' => [
        'format' => Response::FORMAT_JSON,
    ],
    'user' => [
        'identityClass' => User::class,
        'enableSession' => false,
    ],

    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => FileTarget::class,
                'levels' => ['error', 'warning'],
            ],
        ],
    ],

    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
            'GET info' => 'main/info',
            'GET test' => 'test/test',

            /** @see \api\controllers\ScaleController */
            'GET scale' => 'scale/index',
            'GET scale/<id:\\d+>' => 'scale/view',
            'POST scale' => 'scale/create',
            'POST scale/<id:\\d+>/value' => 'scale/create-value',
            'PUT,PATCH scale/<id:\\d+>' => 'scale/update',
            'PUT,PATCH scale/value/<id:\\d+>' => 'scale/update-value',
            'DELETE scale/<id:\\d+>' => 'scale/delete',
            'DELETE scale/value/<id:\\d+>' => 'scale/delete-value',


            'GET visualizator/root' => 'visualizator/root-function',
            'GET visualizator/<id:\\d+>' => 'visualizator/function',
        ],
    ],
];