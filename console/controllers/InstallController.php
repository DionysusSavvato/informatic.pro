<?php

namespace console\controllers;

use common\models\User;
use yii\base\Exception as BaseException;
use yii\console\Controller;
use yii\db\Exception;
use yii\validators\EmailValidator;

/**
 * {@inheritDoc}
 */
class InstallController extends Controller
{
    const SUPER_ADMIN_USERNAME = 'SuperAdministrator';

    /**
     * Создание учетки СуперАдминистратора
     *
     * @throws BaseException
     */
    public function actionCreateSuperAdministrator(): void
    {
        if (!User::findByUsername(self::SUPER_ADMIN_USERNAME)) {
            $email = $this->prompt('Enter the e-mail: ',
                [
                    'required' => true,
                    'validator' => function ($input, &$error) {
                        $emailValidator = new EmailValidator();
                        return $emailValidator->validate($input, $error);
                    },
                ]);
            $password = $this->prompt('Enter the password: ',
                [
                    'required' => true,
                    'validator' => function ($input, &$error) {
                        if (mb_strlen(trim($input)) < 6) {
                            $error = 'The password may be 6 symbols or more';
                            return false;
                        }
                        return true;
                    },
                ]);
            $password = trim($password);
            $admin = new User();
            $admin->username = self::SUPER_ADMIN_USERNAME;
            $admin->email = $email;
            $admin->password = $password;
            $admin->generateAuthKey();
            if ($admin->save()) {
                echo 'SuperAdministrator is created successfully';
            }
            else {
                throw new Exception('Error in saving to DB');
            }
        }
        else {
            echo "SuperAdministrator is already created.\n";
        }
    }
}