<?php

namespace console\controllers;

use common\models\User;
use Exception;
use Yii;
use yii\base\Exception as BaseException;
use yii\console\Controller;
use yii\rbac\Permission;
use yii\rbac\Role;

/**
 * Class RbacController
 * @package console\controllers
 */
class RbacController extends Controller
{
    /**
     * Имя роли Admin
     */
    const ADMIN_ROLE = 'admin';

    /**
     * Инициализация данных RBAC
     *
     * `php yii rbac/init`
     *
     * @throws Exception
     */
    public function actionInit()
    {
        $this->createAdminRbac();
    }

    /**
     * Создание RBAC данных по админу
     *
     * @throws Exception
     */
    private function createAdminRbac()
    {
        $userManagement = $this->createUserManagementPermission();
        $admin = $this->createAdminRole();
        $this->assignPermissionWithRole($admin, $userManagement);
        $this->assignAdminToSuperAdmin();
    }

    /**
     * Создание разрешения на управление учетными записями пользователей
     *
     * @return Permission
     * @throws Exception
     */
    private function createUserManagementPermission()
    {
        $auth = Yii::$app->authManager;
        $userManagement = $auth->createPermission('userManagement');
        $userManagement->description = 'Управление учетными записями пользователей';
        $auth->add($userManagement);
        return $userManagement;
    }

    /**
     * Создание роли адинистратора системы
     *
     * @return Role
     * @throws Exception
     */
    private function createAdminRole()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->createRole(self::ADMIN_ROLE);
        $admin->description = 'Системный администратор';
        $auth->add($admin);
        return $admin;
    }

    /**
     * Связывание роли с разрешением
     *
     * @param Role       $role
     * @param Permission $permission
     *
     * @throws BaseException
     */
    private function assignPermissionWithRole($role, $permission)
    {
        $auth = Yii::$app->authManager;
        $auth->addChild($role, $permission);
    }

    /**
     * Присвоение роли admin СуперАдминистратору
     *
     * @throws Exception
     */
    private function assignAdminToSuperAdmin()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(self::ADMIN_ROLE);
        $user = User::findByUsername(InstallController::SUPER_ADMIN_USERNAME);
        if ($user) {
            $auth->assign($admin, $user->id);
        }
    }

    /**
     * Связывание роли с пользователем
     *
     * @param Role    $role
     * @param integer $userID
     *
     * @throws Exception
     */
    private function assignRoleWithUser($role, $userID)
    {
        $auth = Yii::$app->authManager;
        if (User::findOne($userID)) {
            $auth->assign($role, $userID);
        }
    }
}