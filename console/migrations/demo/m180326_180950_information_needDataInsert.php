<?php

use yii\db\Migration;

class m180326_180950_information_needDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('information_need',
            ['entity_id', 'name', 'abbreviation', 'manager_id'],
            [
                [
                    'entity_id' => '4',
                    'name' => 'Информация о современной моде',
                    'abbreviation' => 'Uinf11',
                    'manager_id' => '3',
                ],
                [
                    'entity_id' => '5',
                    'name' => 'Информация об аналогичных проектах',
                    'abbreviation' => 'Uinf12',
                    'manager_id' => '3',
                ],
                [
                    'entity_id' => '21',
                    'name' => 'Информация о методах технологической обработки',
                    'abbreviation' => 'Uinf21',
                    'manager_id' => '20',
                ],
                [
                    'entity_id' => '22',
                    'name' => 'Информация об технико-экономических требованиях',
                    'abbreviation' => 'Uinf22',
                    'manager_id' => '20',
                ],
                [
                    'entity_id' => '23',
                    'name' => 'Данные продолжительности всех процессов и связанных с ними затрат',
                    'abbreviation' => 'Uinf23',
                    'manager_id' => '20',
                ],
                [
                    'entity_id' => '39',
                    'name' => 'Информация о технологии производства',
                    'abbreviation' => 'Uinf3',
                    'manager_id' => '38',
                ],
                [
                    'entity_id' => '49',
                    'name' => 'Информация о технологии производства',
                    'abbreviation' => 'Uinf4',
                    'manager_id' => '48',
                ],
                [
                    'entity_id' => '58',
                    'name' => 'Информация о технологии производства',
                    'abbreviation' => 'Uinf5',
                    'manager_id' => '57',
                ],
                [
                    'entity_id' => '68',
                    'name' => 'Информация о свойствах тканей и характеристике оборудований',
                    'abbreviation' => 'Uinf61',
                    'manager_id' => '67',
                ],
                [
                    'entity_id' => '69',
                    'name' => 'Информация о требованиях к упаковке и отделке',
                    'abbreviation' => 'Uinf62',
                    'manager_id' => '67',
                ],
                [
                    'entity_id' => '70',
                    'name' => 'Информация о методах упаковки',
                    'abbreviation' => 'Uinf63',
                    'manager_id' => '67',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('information_need CASCADE');
    }
}
