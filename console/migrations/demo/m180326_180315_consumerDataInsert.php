<?php

use yii\db\Migration;

class m180326_180315_consumerDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('consumer',
            ['entity_id', 'name', 'abbreviation'],
            [
                [
                    'entity_id' => '82',
                    'name' => 'Магазин одежды «Одежда КиО»',
                    'abbreviation' => 'П1',
                ],
                [
                    'entity_id' => '83',
                    'name' => 'Магазин одежды «Одежда для всей семьи»',
                    'abbreviation' => 'П2',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('consumer CASCADE');
    }
}
