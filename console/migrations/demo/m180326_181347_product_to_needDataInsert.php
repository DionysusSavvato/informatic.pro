<?php

use yii\db\Migration;

class m180326_181347_product_to_needDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('product_to_need',
            ['id', 'product_id', 'need_id', 'effect_name'],
            [
                [
                    'id' => '1',
                    'product_id' => '7',
                    'need_id' => '84',
                    'effect_name' => null,
                ],
                [
                    'id' => '2',
                    'product_id' => '7',
                    'need_id' => '85',
                    'effect_name' => null,
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('product_to_need CASCADE');
    }
}
