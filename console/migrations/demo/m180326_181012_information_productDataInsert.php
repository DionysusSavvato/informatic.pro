<?php

use yii\db\Migration;

class m180326_181012_information_productDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('information_product',
            ['entity_id', 'name', 'abbreviation', 'function_id'],
            [
                [
                    'entity_id' => '7',
                    'name' => 'Отчет о научных и маркетинговых исследованиях',
                    'abbreviation' => 'Pinf11',
                    'function_id' => '6',
                ],
                [
                    'entity_id' => '11',
                    'name' => 'Отчет о моделях-аналогах',
                    'abbreviation' => 'Pinf12',
                    'function_id' => '10',
                ],
                [
                    'entity_id' => '25',
                    'name' => 'Отчет о методах технологической обработки ',
                    'abbreviation' => 'Pinf21',
                    'function_id' => '24',
                ],
                [
                    'entity_id' => '27',
                    'name' => 'Отчет о технико-экономических требованиях ',
                    'abbreviation' => 'Pinf22',
                    'function_id' => '26',
                ],
                [
                    'entity_id' => '29',
                    'name' => 'Технический эскиз',
                    'abbreviation' => 'Pinf23',
                    'function_id' => '28',
                ],
                [
                    'entity_id' => '41',
                    'name' => 'Технология производства ',
                    'abbreviation' => 'Pinf3',
                    'function_id' => '40',
                ],
                [
                    'entity_id' => '73',
                    'name' => 'Отчет о требованиях к упаковке и отделке изделий',
                    'abbreviation' => 'Pinf62',
                    'function_id' => '72',
                ],
                [
                    'entity_id' => '74',
                    'name' => 'Отчет о свойствах тканей изделий и характеристиках оборудования',
                    'abbreviation' => 'Pinf61',
                    'function_id' => '71',
                ],
                [
                    'entity_id' => '76',
                    'name' => 'Отчет о методах упаковки',
                    'abbreviation' => 'Pinf63',
                    'function_id' => '75',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('information_product CASCADE');
    }
}
