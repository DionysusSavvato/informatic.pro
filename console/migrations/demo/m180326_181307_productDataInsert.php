<?php

use yii\db\Migration;

class m180326_181307_productDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('product',
            ['id', 'abbreviation', 'material_id', 'function_id'],
            [
                [
                    'id' => '1',
                    'abbreviation' => 'P1',
                    'material_id' => '19',
                    'function_id' => '16',
                ],
                [
                    'id' => '2',
                    'abbreviation' => 'P21',
                    'material_id' => '36',
                    'function_id' => '31',
                ],
                [
                    'id' => '3',
                    'abbreviation' => 'P22',
                    'material_id' => '37',
                    'function_id' => '31',
                ],
                [
                    'id' => '4',
                    'abbreviation' => 'P3',
                    'material_id' => '46',
                    'function_id' => '45',
                ],
                [
                    'id' => '5',
                    'abbreviation' => 'P4',
                    'material_id' => '56',
                    'function_id' => '51',
                ],
                [
                    'id' => '6',
                    'abbreviation' => 'P5',
                    'material_id' => '65',
                    'function_id' => '59',
                ],
                [
                    'id' => '7',
                    'abbreviation' => 'P6',
                    'material_id' => '81',
                    'function_id' => '77',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('product CASCADE');
    }
}
