<?php

use yii\db\Migration;

class m180326_180734_industrial_functionDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('industrial_function',
            ['entity_id', 'name', 'abbreviation', 'parent_id', 'manager_id'],
            [
                [
                    'entity_id' => '1',
                    'name' => 'Швейное производство',
                    'abbreviation' => 'F',
                    'parent_id' => null,
                    'manager_id' => null,
                ],
                [
                    'entity_id' => '16',
                    'name' => 'Проектирование моделей',
                    'abbreviation' => 'F1',
                    'parent_id' => '1',
                    'manager_id' => '3',
                ],
                [
                    'entity_id' => '31',
                    'name' => 'Технологическая обработка',
                    'abbreviation' => 'F2',
                    'parent_id' => '1',
                    'manager_id' => '20',
                ],
                [
                    'entity_id' => '45',
                    'name' => 'Подготовка и раскрой изделия',
                    'abbreviation' => 'F3',
                    'parent_id' => '1',
                    'manager_id' => '38',
                ],
                [
                    'entity_id' => '51',
                    'name' => 'Пошив изделий',
                    'abbreviation' => 'F4',
                    'parent_id' => '1',
                    'manager_id' => '48',
                ],
                [
                    'entity_id' => '59',
                    'name' => 'Отделка изделий',
                    'abbreviation' => 'F5',
                    'parent_id' => '1',
                    'manager_id' => '57',
                ],
                [
                    'entity_id' => '77',
                    'name' => 'Упаковка изделий',
                    'abbreviation' => 'F6',
                    'parent_id' => '1',
                    'manager_id' => '67',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('industrial_function CASCADE');
    }
}
