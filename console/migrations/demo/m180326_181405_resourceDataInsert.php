<?php

use yii\db\Migration;

class m180326_181405_resourceDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('resource',
            ['id', 'abbreviation', 'material_id', 'function_id'],
            [
                [
                    'id' => '1',
                    'abbreviation' => 'R11',
                    'material_id' => '17',
                    'function_id' => '16',
                ],
                [
                    'id' => '2',
                    'abbreviation' => 'R12',
                    'material_id' => '18',
                    'function_id' => '16',
                ],
                [
                    'id' => '3',
                    'abbreviation' => 'R21',
                    'material_id' => '32',
                    'function_id' => '31',
                ],
                [
                    'id' => '4',
                    'abbreviation' => 'R22',
                    'material_id' => '33',
                    'function_id' => '31',
                ],
                [
                    'id' => '5',
                    'abbreviation' => 'R23',
                    'material_id' => '19',
                    'function_id' => '31',
                ],
                [
                    'id' => '6',
                    'abbreviation' => 'R24',
                    'material_id' => '18',
                    'function_id' => '31',
                ],
                [
                    'id' => '7',
                    'abbreviation' => 'R25',
                    'material_id' => '34',
                    'function_id' => '31',
                ],
                [
                    'id' => '8',
                    'abbreviation' => 'R26',
                    'material_id' => '35',
                    'function_id' => '31',
                ],
                [
                    'id' => '9',
                    'abbreviation' => 'R31',
                    'material_id' => '36',
                    'function_id' => '45',
                ],
                [
                    'id' => '10',
                    'abbreviation' => 'R32',
                    'material_id' => '33',
                    'function_id' => '45',
                ],
                [
                    'id' => '11',
                    'abbreviation' => 'R33',
                    'material_id' => '18',
                    'function_id' => '45',
                ],
                [
                    'id' => '12',
                    'abbreviation' => 'R34',
                    'material_id' => '47',
                    'function_id' => '45',
                ],
                [
                    'id' => '13',
                    'abbreviation' => 'R35',
                    'material_id' => '34',
                    'function_id' => '45',
                ],
                [
                    'id' => '14',
                    'abbreviation' => 'R36',
                    'material_id' => '35',
                    'function_id' => '45',
                ],
                [
                    'id' => '15',
                    'abbreviation' => 'R41',
                    'material_id' => '46',
                    'function_id' => '51',
                ],
                [
                    'id' => '16',
                    'abbreviation' => 'R42',
                    'material_id' => '52',
                    'function_id' => '51',
                ],
                [
                    'id' => '17',
                    'abbreviation' => 'R43',
                    'material_id' => '53',
                    'function_id' => '51',
                ],
                [
                    'id' => '18',
                    'abbreviation' => 'R44',
                    'material_id' => '54',
                    'function_id' => '51',
                ],
                [
                    'id' => '19',
                    'abbreviation' => 'R45',
                    'material_id' => '55',
                    'function_id' => '51',
                ],
                [
                    'id' => '20',
                    'abbreviation' => 'R51',
                    'material_id' => '56',
                    'function_id' => '59',
                ],
                [
                    'id' => '21',
                    'abbreviation' => 'R52',
                    'material_id' => '52',
                    'function_id' => '59',
                ],
                [
                    'id' => '22',
                    'abbreviation' => 'R53',
                    'material_id' => '60',
                    'function_id' => '59',
                ],
                [
                    'id' => '23',
                    'abbreviation' => 'R54',
                    'material_id' => '61',
                    'function_id' => '59',
                ],
                [
                    'id' => '24',
                    'abbreviation' => 'R55',
                    'material_id' => '62',
                    'function_id' => '59',
                ],
                [
                    'id' => '25',
                    'abbreviation' => 'R56',
                    'material_id' => '63',
                    'function_id' => '59',
                ],
                [
                    'id' => '26',
                    'abbreviation' => 'R57',
                    'material_id' => '34',
                    'function_id' => '59',
                ],
                [
                    'id' => '27',
                    'abbreviation' => 'R58',
                    'material_id' => '64',
                    'function_id' => '59',
                ],
                [
                    'id' => '28',
                    'abbreviation' => 'R61',
                    'material_id' => '65',
                    'function_id' => '77',
                ],
                [
                    'id' => '29',
                    'abbreviation' => 'R62',
                    'material_id' => '78',
                    'function_id' => '77',
                ],
                [
                    'id' => '30',
                    'abbreviation' => 'R63',
                    'material_id' => '79',
                    'function_id' => '77',
                ],
                [
                    'id' => '31',
                    'abbreviation' => 'R64',
                    'material_id' => '80',
                    'function_id' => '77',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('resource CASCADE');
    }
}
