<?php

use yii\db\Migration;

class m180326_181043_information_product_to_information_needDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('information_product_to_information_need',
            ['id', 'information_product_id', 'information_need_id', 'effect_threshold_value'],
            [
                [
                    'id' => '1',
                    'information_product_id' => '7',
                    'information_need_id' => '4',
                    'effect_threshold_value' => '0.5',
                ],
                [
                    'id' => '2',
                    'information_product_id' => '11',
                    'information_need_id' => '5',
                    'effect_threshold_value' => '0.6',
                ],
                [
                    'id' => '3',
                    'information_product_id' => '25',
                    'information_need_id' => '21',
                    'effect_threshold_value' => '0.5',
                ],
                [
                    'id' => '4',
                    'information_product_id' => '27',
                    'information_need_id' => '22',
                    'effect_threshold_value' => '0.5',
                ],
                [
                    'id' => '5',
                    'information_product_id' => '29',
                    'information_need_id' => '23',
                    'effect_threshold_value' => '0.5',
                ],
                [
                    'id' => '6',
                    'information_product_id' => '41',
                    'information_need_id' => '39',
                    'effect_threshold_value' => '0.7',
                ],
                [
                    'id' => '7',
                    'information_product_id' => '41',
                    'information_need_id' => '49',
                    'effect_threshold_value' => '0.7',
                ],
                [
                    'id' => '8',
                    'information_product_id' => '41',
                    'information_need_id' => '58',
                    'effect_threshold_value' => '0.7',
                ],
                [
                    'id' => '9',
                    'information_product_id' => '74',
                    'information_need_id' => '68',
                    'effect_threshold_value' => '0.5',
                ],
                [
                    'id' => '10',
                    'information_product_id' => '73',
                    'information_need_id' => '69',
                    'effect_threshold_value' => '0.5',
                ],
                [
                    'id' => '11',
                    'information_product_id' => '76',
                    'information_need_id' => '70',
                    'effect_threshold_value' => '0.5',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('information_product_to_information_need CASCADE');
    }
}
