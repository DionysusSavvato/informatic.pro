<?php

use yii\db\Migration;

class m180326_181118_materialDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('material',
            ['entity_id', 'name'],
            [
                [
                    'entity_id' => '17',
                    'name' => 'Задача, цель, потребительские требования',
                ],
                [
                    'entity_id' => '18',
                    'name' => 'Программное средство САПР',
                ],
                [
                    'entity_id' => '19',
                    'name' => 'Технический эскиз',
                ],
                [
                    'entity_id' => '32',
                    'name' => 'Картон',
                ],
                [
                    'entity_id' => '33',
                    'name' => 'Ножная машина для раскроя',
                ],
                [
                    'entity_id' => '34',
                    'name' => 'Печатная машина',
                ],
                [
                    'entity_id' => '35',
                    'name' => 'Краска для картриджей',
                ],
                [
                    'entity_id' => '36',
                    'name' => 'Лекала',
                ],
                [
                    'entity_id' => '37',
                    'name' => 'Технология производства',
                ],
                [
                    'entity_id' => '46',
                    'name' => 'Пачки выкроенных частей изделий',
                ],
                [
                    'entity_id' => '47',
                    'name' => 'Ткани',
                ],
                [
                    'entity_id' => '52',
                    'name' => 'Нитки',
                ],
                [
                    'entity_id' => '53',
                    'name' => 'Иглы',
                ],
                [
                    'entity_id' => '54',
                    'name' => 'Клей',
                ],
                [
                    'entity_id' => '55',
                    'name' => 'Швейные машины',
                ],
                [
                    'entity_id' => '56',
                    'name' => 'Швейные изделия',
                ],
                [
                    'entity_id' => '60',
                    'name' => 'Пуговицы',
                ],
                [
                    'entity_id' => '61',
                    'name' => 'Молнии',
                ],
                [
                    'entity_id' => '62',
                    'name' => 'Краски',
                ],
                [
                    'entity_id' => '63',
                    'name' => 'Вышивальная машина',
                ],
                [
                    'entity_id' => '64',
                    'name' => 'Машины влажно-тепловой обработки ',
                ],
                [
                    'entity_id' => '65',
                    'name' => 'Швейные изделия после отделки',
                ],
                [
                    'entity_id' => '78',
                    'name' => 'Упаковки',
                ],
                [
                    'entity_id' => '79',
                    'name' => 'Этикетки',
                ],
                [
                    'entity_id' => '80',
                    'name' => 'Упаковочная машина',
                ],
                [
                    'entity_id' => '81',
                    'name' => 'Готовые изделия',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('material CASCADE');
    }
}
