<?php

use yii\db\Migration;

class m180326_180201_entityDataInsert extends Migration
{

    public function safeUp()
    {
        $this->batchInsert('entity',
            ['entity_id', 'entity_type_id'],
            [
                [
                    'entity_id' => '1',
                    'entity_type_id' => '1',
                ],
                [
                    'entity_id' => '16',
                    'entity_type_id' => '1',
                ],
                [
                    'entity_id' => '31',
                    'entity_type_id' => '1',
                ],
                [
                    'entity_id' => '45',
                    'entity_type_id' => '1',
                ],
                [
                    'entity_id' => '51',
                    'entity_type_id' => '1',
                ],
                [
                    'entity_id' => '59',
                    'entity_type_id' => '1',
                ],
                [
                    'entity_id' => '77',
                    'entity_type_id' => '1',
                ],
                [
                    'entity_id' => '17',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '18',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '19',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '32',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '33',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '34',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '35',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '36',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '37',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '46',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '47',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '52',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '53',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '54',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '55',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '56',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '60',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '61',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '62',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '63',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '64',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '65',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '78',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '79',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '80',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '81',
                    'entity_type_id' => '2',
                ],
                [
                    'entity_id' => '82',
                    'entity_type_id' => '3',
                ],
                [
                    'entity_id' => '83',
                    'entity_type_id' => '3',
                ],
                [
                    'entity_id' => '84',
                    'entity_type_id' => '5',
                ],
                [
                    'entity_id' => '85',
                    'entity_type_id' => '5',
                ],
                [
                    'entity_id' => '2',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '3',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '20',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '38',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '48',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '57',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '66',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '67',
                    'entity_type_id' => '6',
                ],
                [
                    'entity_id' => '4',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '5',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '21',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '22',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '23',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '39',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '49',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '58',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '68',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '69',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '70',
                    'entity_type_id' => '7',
                ],
                [
                    'entity_id' => '7',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '11',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '25',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '27',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '29',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '41',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '73',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '74',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '76',
                    'entity_type_id' => '8',
                ],
                [
                    'entity_id' => '6',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '10',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '24',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '26',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '28',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '40',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '71',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '72',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '75',
                    'entity_type_id' => '9',
                ],
                [
                    'entity_id' => '8',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '9',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '12',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '13',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '14',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '15',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '30',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '42',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '43',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '44',
                    'entity_type_id' => '10',
                ],
                [
                    'entity_id' => '50',
                    'entity_type_id' => '10',
                ],
            ]
        );
    }

    public function safeDown()
    {
        $this->truncateTable('entity CASCADE');
    }
}
