<?php

use yii\db\Migration;

class m180326_181426_product_to_resourceDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('product_to_resource',
            ['id', 'product_id', 'resource_id', 'effect_name'],
            [
                [
                    'id' => '1',
                    'product_id' => '1',
                    'resource_id' => '5',
                    'effect_name' => null,
                ],
                [
                    'id' => '2',
                    'product_id' => '4',
                    'resource_id' => '15',
                    'effect_name' => null,
                ],
                [
                    'id' => '3',
                    'product_id' => '6',
                    'resource_id' => '28',
                    'effect_name' => null,
                ],
                [
                    'id' => '4',
                    'product_id' => '2',
                    'resource_id' => '9',
                    'effect_name' => null,
                ],
                [
                    'id' => '5',
                    'product_id' => '5',
                    'resource_id' => '20',
                    'effect_name' => null,
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('product_to_resource CASCADE');
    }
}
