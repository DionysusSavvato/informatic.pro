<?php

use yii\db\Migration;

class m180326_180711_managerDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('manager',
            ['entity_id', 'name', 'abbreviation'],
            [
                [
                    'entity_id' => '2',
                    'name' => 'Директор фабрики',
                    'abbreviation' => 'ОЛ',
                ],
                [
                    'entity_id' => '3',
                    'name' => 'Мастер-технолог',
                    'abbreviation' => 'ОЛ1',
                ],
                [
                    'entity_id' => '20',
                    'name' => 'Технолог-конструктор',
                    'abbreviation' => 'ОЛ2',
                ],
                [
                    'entity_id' => '38',
                    'name' => 'Мастер раскройного цеха',
                    'abbreviation' => 'ОЛ3',
                ],
                [
                    'entity_id' => '48',
                    'name' => 'Мастер швейного цеха',
                    'abbreviation' => 'ОЛ4',
                ],
                [
                    'entity_id' => '57',
                    'name' => 'Мастер отделочного цеха',
                    'abbreviation' => 'ОЛ5',
                ],
                [
                    'entity_id' => '67',
                    'name' => 'Упаковщик',
                    'abbreviation' => 'ОЛ6',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('manager CASCADE');
    }
}
