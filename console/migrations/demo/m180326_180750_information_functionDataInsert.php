<?php

use yii\db\Migration;

class m180326_180750_information_functionDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('information_function',
            ['entity_id', 'name', 'abbreviation'],
            [
                [
                    'entity_id' => '6',
                    'name' => 'Исследование тенденций в моде',
                    'abbreviation' => 'Finf11',
                ],
                [
                    'entity_id' => '10',
                    'name' => 'Поиск аналогов',
                    'abbreviation' => 'Finf12',
                ],
                [
                    'entity_id' => '24',
                    'name' => 'Поиск методов технологической обработки',
                    'abbreviation' => 'Finf21',
                ],
                [
                    'entity_id' => '26',
                    'name' => 'Исследование технико-экономических требований',
                    'abbreviation' => 'Finf22',
                ],
                [
                    'entity_id' => '28',
                    'name' => 'Выбор и представление необходимых данных из технического эскиза',
                    'abbreviation' => 'Finf23',
                ],
                [
                    'entity_id' => '40',
                    'name' => 'Выбор и представление необходимых данных из технологии производства',
                    'abbreviation' => 'Finf3',
                ],
                [
                    'entity_id' => '71',
                    'name' => 'Поиск и предоставление свойств тканей изделий и характеристик оборудования',
                    'abbreviation' => 'Finf61',
                ],
                [
                    'entity_id' => '72',
                    'name' => 'Предоставление требований к упаковке и отделке изделий',
                    'abbreviation' => 'F62',
                ],
                [
                    'entity_id' => '75',
                    'name' => 'Поиск и предоставление методов упаковки',
                    'abbreviation' => 'Pinf63',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('information_function CASCADE');
    }
}
