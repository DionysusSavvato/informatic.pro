<?php

use yii\db\Migration;

class m180326_180525_needDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('need',
            ['entity_id', 'name', 'abbreviation', 'consumer_id'],
            [
                [
                    'entity_id' => '84',
                    'name' => 'Потребность в модной швейной продукции',
                    'abbreviation' => 'U1',
                    'consumer_id' => '82',
                ],
                [
                    'entity_id' => '85',
                    'name' => 'Потребность в модной швейной продукции',
                    'abbreviation' => 'U2',
                    'consumer_id' => '83',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('need CASCADE');
    }
}
