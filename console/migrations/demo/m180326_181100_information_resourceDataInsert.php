<?php

use yii\db\Migration;

class m180326_181100_information_resourceDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('information_resource',
            ['entity_id', 'name', 'abbreviation', 'function_id'],
            [
                [
                    'entity_id' => '8',
                    'name' => 'Модели одежды в конкретном сезоне',
                    'abbreviation' => 'Rinf111',
                    'function_id' => '6',
                ],
                [
                    'entity_id' => '9',
                    'name' => 'Цветовые характеристики сезона',
                    'abbreviation' => 'Rinf112',
                    'function_id' => '6',
                ],
                [
                    'entity_id' => '12',
                    'name' => 'Тип изделий',
                    'abbreviation' => 'Rinf121',
                    'function_id' => '10',
                ],
                [
                    'entity_id' => '13',
                    'name' => 'Элементы кроя',
                    'abbreviation' => 'Rinf122',
                    'function_id' => '10',
                ],
                [
                    'entity_id' => '14',
                    'name' => 'Сведения о методике конструирования',
                    'abbreviation' => 'Rinf123',
                    'function_id' => '10',
                ],
                [
                    'entity_id' => '15',
                    'name' => 'Размерные признаки',
                    'abbreviation' => 'Rinf124',
                    'function_id' => '10',
                ],
                [
                    'entity_id' => '30',
                    'name' => 'Время и затраты на каждый процесс',
                    'abbreviation' => 'Rinf231',
                    'function_id' => '28',
                ],
                [
                    'entity_id' => '42',
                    'name' => 'Продолжительность производственного процесса',
                    'abbreviation' => 'Rinf31',
                    'function_id' => '40',
                ],
                [
                    'entity_id' => '43',
                    'name' => 'Себестоимость и ресурсоёмкость продукции',
                    'abbreviation' => 'Rinf32',
                    'function_id' => '40',
                ],
                [
                    'entity_id' => '44',
                    'name' => 'Характеристики тканей',
                    'abbreviation' => 'Rinf33',
                    'function_id' => '40',
                ],
                [
                    'entity_id' => '50',
                    'name' => 'Задание для отделки изделий',
                    'abbreviation' => 'Rinf34',
                    'function_id' => '40',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('information_resource CASCADE');
    }
}
