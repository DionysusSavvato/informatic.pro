<?php

use yii\db\Migration;

class m170705_183050_entity extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%entity}}',
            [
                'entity_id' => $this->primaryKey(11)->unsigned(),
                'entity_type_id' => $this->integer(11)->unsigned()->notNull(),
            ], $tableOptions
        );

    }

    public function safeDown(): void
    {
        $this->dropTable('{{%entity}}');
    }
}
