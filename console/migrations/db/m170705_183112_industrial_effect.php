<?php

use yii\db\Migration;

class m170705_183112_industrial_effect extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp(): void
    {
        $this->createTable('industrial_effect', [
            'id' => $this->primaryKey()->unsigned(),
            'product_to_need_id' => $this->integer()->unsigned()->notNull(),
            'value' => $this->float()->notNull(),
            'date' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown(): void
    {
        $this->dropTable('industrial_effect');
    }
}
