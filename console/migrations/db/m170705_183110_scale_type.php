<?php

use yii\db\Migration;

class m170705_183110_scale_type extends Migration
{
    public function init(): void
    {
        $this->db = 'db';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%scale_type}}',
            [
                'scale_type_id' => $this->primaryKey()->unsigned(),
                'name' => $this->string()->notNull(),
            ], $tableOptions
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%scale_type}}');
    }
}
