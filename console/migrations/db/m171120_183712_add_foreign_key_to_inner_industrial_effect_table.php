<?php

use yii\db\Migration;

class m171120_183712_add_foreign_key_to_inner_industrial_effect_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_inner_industrial_effect__product_to_resource',
            'inner_industrial_effect', 'product_to_resource_id',
            'product_to_resource', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey('fk_inner_industrial_effect__product_to_resource', 'inner_industrial_effect');
    }
}
