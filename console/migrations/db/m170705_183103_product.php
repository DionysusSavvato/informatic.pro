<?php

use yii\db\Migration;

class m170705_183103_product extends Migration
{

    public function init(): void
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product}}',
            [
                'id' => $this->primaryKey()->unsigned(),
                'abbreviation' => $this->string(20)->notNull(),
                'material_id' => $this->integer(11)->unsigned()->notNull(),
                'function_id' => $this->integer(11)->unsigned()->notNull(),
            ], $tableOptions
        );
        $this->createIndex(
            'ui_material_id__function_id',
            '{{%product}}',
            ['function_id', 'material_id'],
            true
        );

    }

    public function safeDown(): void
    {
        $this->dropIndex('ui_material_id__function_id', '{{%product}}');
        $this->dropTable('{{%product}}');
    }
}
