<?php

use yii\db\Migration;

class m170705_183048_consumer extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%consumer}}',
            [
                'entity_id' => $this->primaryKey(11)->unsigned(),
                'name' => $this->string(255)->notNull(),
                'abbreviation' => $this->string(20)->notNull(),
            ], $tableOptions
        );

    }

    public function safeDown(): void
    {
        $this->dropTable('{{%consumer}}');
    }
}
