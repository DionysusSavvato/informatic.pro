<?php

use yii\db\Migration;

class m170715_115939_add_foreign_key_to_scale_value_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_scale_value_scale_id',
            '{{%scale_value}}', 'scale_id',
            '{{%scale}}', 'scale_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_scale_value_scale_id', '{{%scale_value}}');
    }
}
