<?php

use yii\db\Migration;

class m171119_194910_add_foreign_key_to_industrial_effect_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_industrial_effect__product_to_need',
            'industrial_effect', 'product_to_need_id',
            'product_to_need', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey(
            'fk_industrial_effect__product_to_need',
            'industrial_effect'
        );
    }
}
