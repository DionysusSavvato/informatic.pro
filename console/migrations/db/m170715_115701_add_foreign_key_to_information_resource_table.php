<?php

use yii\db\Migration;

class m170715_115701_add_foreign_key_to_information_resource_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_information_resource_entity_id',
            '{{%information_resource}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_information_resource_entity_id', '{{%information_resource}}');
    }
}
