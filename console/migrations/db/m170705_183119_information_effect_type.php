<?php

use common\models\InformationEffectType;
use yii\db\Migration;

class m170705_183119_information_effect_type extends Migration
{
    private const TABLE_NAME = 'information_effect_type';

    private const TYPES = [
        InformationEffectType::TIMELINESS => 'Своевременность',
        InformationEffectType::COMPLETENESS => 'Полнота',
    ];

    public function safeUp(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
        ]);

        foreach (self::TYPES as $typeID => $typeName) {
            $this->insert(self::TABLE_NAME, [
                'id' => $typeID,
                'name' => $typeName,
            ]);
        }
    }

    public function safeDown(): void
    {
        $this->dropTable(
            self::TABLE_NAME
        );
    }
}
