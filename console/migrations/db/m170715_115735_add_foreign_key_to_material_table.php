<?php

use yii\db\Migration;

class m170715_115735_add_foreign_key_to_material_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_material_entity_id',
            '{{%material}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_material_entity_id', '{{%material}}');
    }
}
