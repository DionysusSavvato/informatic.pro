<?php

use yii\db\Migration;

class m170715_115524_add_foreign_key_to_industrial_function_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_function_parent_id',
            '{{%industrial_function}}', 'parent_id',
            '{{%industrial_function}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_function_parent_id', '{{%industrial_function}}');
    }
}
