<?php

use yii\db\Migration;

class m170715_120004_add_foreign_key_to_supplier_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_supplier_entity_id',
            '{{%supplier}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_supplier_entity_id', '{{%supplier}}');
    }
}
