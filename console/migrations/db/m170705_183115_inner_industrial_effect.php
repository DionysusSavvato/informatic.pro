<?php

use yii\db\Migration;

class m170705_183115_inner_industrial_effect extends Migration
{
    public function safeUp(): void
    {
        $this->createTable('inner_industrial_effect', [
            'id' => $this->primaryKey()->unsigned(),
            'product_to_resource_id' => $this->integer()->unsigned()->notNull(),
            'value' => $this->float()->notNull(),
            'date' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
        ]);
    }

    public function safeDown(): void
    {
        $this->dropTable('inner_industrial_effect');
    }
}
