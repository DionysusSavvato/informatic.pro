<?php

use yii\db\Migration;

/**
 * Class m180505_143707_add_foreign_key_information_change_localization_table
 */
class m180505_143707_add_foreign_key_information_change_localization_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_information_change_localization__function',
            'information_change_localization', 'function_id',
            'industrial_function', 'entity_id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey(
            'fk_information_change_localization__function',
            'information_change_localization'
        );
    }
}
