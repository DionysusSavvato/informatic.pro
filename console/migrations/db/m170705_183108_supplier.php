<?php

use yii\db\Migration;

class m170705_183108_supplier extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%supplier}}',
            [
                'entity_id' => $this->primaryKey(11)->unsigned(),
                'name' => $this->string(255)->notNull(),
                'abbreviation' => $this->string(20)->notNull(),
            ], $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%supplier}}');
    }
}
