<?php

use yii\db\Migration;

class m170715_115802_add_foreign_key_to_parameter_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_parameter_entity_id',
            '{{%parameter}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_parameter_entity_id', '{{%parameter}}');
    }
}
