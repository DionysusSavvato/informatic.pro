<?php

use yii\db\Migration;

class m170715_115748_add_foreign_key_to_need_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_need_entity_id',
            '{{%need}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_need_entity_id', '{{%need}}');
    }
}
