<?php

use yii\db\Migration;

class m170705_183102_parameter_value extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%parameter_value}}',
            [
                'value_id' => $this->primaryKey(11)->unsigned(),
                'param_id' => $this->integer(11)->unsigned()->notNull(),
                'value' => $this->string(255)->notNull(),
                'date' => $this->datetime()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
            ], $tableOptions
        );
        $this->createIndex('FK_parameter_value_parameter_parameter_id', '{{%parameter_value}}', ['param_id'], false);

    }

    public function safeDown()
    {
        $this->dropIndex('FK_parameter_value_parameter_parameter_id', '{{%parameter_value}}');
        $this->dropTable('{{%parameter_value}}');
    }
}
