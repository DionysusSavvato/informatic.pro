<?php

use yii\db\Migration;

class m180318_160836_add_foreign_key_to_message_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_message__situation',
            'message', 'situation_id',
            'situation', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_message__situation',
            'message'
        );
    }
}
