<?php

use yii\db\Migration;

/**
 * Class m170715_115427_add_foreign_key_to_consumer_table
 */
class m170715_115427_add_foreign_key_to_consumer_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_consumer_entity_id',
            '{{%consumer}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_consumer_entity_id', '{{%consumer}}');
    }
}
