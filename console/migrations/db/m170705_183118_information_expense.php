<?php

use yii\db\Migration;

class m170705_183118_information_expense extends Migration
{
    private const TABLE_NAME = 'information_expense';

    public function safeUp(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'sum' => $this->float()->unsigned()->notNull(),
            'date' => $this->date()->notNull(),
        ]);
    }

    public function safeDown(): void
    {
        $this->dropTable(
            self::TABLE_NAME
        );
    }
}
