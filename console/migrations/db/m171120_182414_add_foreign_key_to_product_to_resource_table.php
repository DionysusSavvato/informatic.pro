<?php

use yii\db\Migration;

class m171120_182414_add_foreign_key_to_product_to_resource_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_product_to_resource__resource',
            'product_to_resource', 'resource_id',
            'resource', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey('fk_product_to_resource__resource', 'product_to_resource');
    }
}
