<?php

use yii\db\Migration;

class m170705_183051_industrial_function extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%industrial_function}}',
            [
                'entity_id' => $this->primaryKey(11)->unsigned(),
                'name' => $this->string(255)->notNull(),
                'abbreviation' => $this->string(20)->notNull(),
                'parent_id' => $this->integer(11)->unsigned()->null()->defaultValue(null),
                'manager_id' => $this->integer()->unsigned()->null(),
            ], $tableOptions
        );
        $this->createIndex(
            'IDX_function_function_entity_id',
            '{{%industrial_function}}',
            ['parent_id'],
            false);

    }

    public function safeDown(): void
    {
        $this->dropIndex('IDX_function_function_entity_id', '{{%industrial_function}}');
        $this->dropTable('{{%industrial_function}}');
    }
}
