<?php

use common\models\ScaleType;
use yii\db\Migration;

class m170716_103704_fill_scale_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(
            '{{%scale_type}}',
            ['scale_type_id', 'name'],
            [
                [
                    'scale_type_id' => ScaleType::TYPE_CLASS,
                    'name' => 'Классификационная шкала',
                ],
                [
                    'scale_type_id' => ScaleType::TYPE_RANK,
                    'name' => 'Ранговая шкала',
                ],
                [
                    'scale_type_id' => ScaleType::TYPE_RELATIVE,
                    'name' => 'Относительная шкала',
                ],
                [
                    'scale_type_id' => ScaleType::TYPE_ABSOLUTE,
                    'name' => 'Абсолютная шкала',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%scale}}', [
            'type_id' => [
                ScaleType::TYPE_CLASS,
                ScaleType::TYPE_RANK,
                ScaleType::TYPE_RELATIVE,
                ScaleType::TYPE_ABSOLUTE,
            ],
        ]);
        $this->delete('{{%scale_type}}', [
            'scale_type_id' => [
                ScaleType::TYPE_CLASS,
                ScaleType::TYPE_RANK,
                ScaleType::TYPE_RELATIVE,
                ScaleType::TYPE_ABSOLUTE,
            ],
        ]);
    }
}
