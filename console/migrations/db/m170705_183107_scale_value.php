<?php

use yii\db\Migration;

class m170705_183107_scale_value extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%scale_value}}',
            [
                'scale_value_id' => $this->primaryKey(11)->unsigned(),
                'scale_id' => $this->integer(11)->unsigned()->notNull(),
                'name' => $this->string(255)->notNull(),
                'rank' => $this->integer(11)->null()->defaultValue(null),
            ], $tableOptions
        );
        $this->createIndex('FK_scale_values_scale_scale_id', '{{%scale_value}}', ['scale_id'],
            false);

    }

    public function safeDown()
    {
        $this->dropIndex('FK_scale_values_scale_scale_id', '{{%scale_value}}');
        $this->dropTable('{{%scale_value}}');
    }
}
