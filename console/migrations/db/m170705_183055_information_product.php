<?php

use yii\db\Migration;

class m170705_183055_information_product extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%information_product}}',
            [
                'entity_id' => $this->primaryKey(11)->unsigned(),
                'name' => $this->string(255)->notNull(),
                'abbreviation' => $this->string(20)->notNull(),
                'function_id' => $this->integer(11)->unsigned()->notNull(),
            ], $tableOptions
        );
        $this->createIndex('IDX_information_product_information_function_entity_id', '{{%information_product}}',
            ['function_id'], false);

    }

    public function safeDown(): void
    {
        $this->dropIndex('IDX_information_product_information_function_entity_id', '{{%information_product}}');
        $this->dropTable('{{%information_product}}');
    }
}
