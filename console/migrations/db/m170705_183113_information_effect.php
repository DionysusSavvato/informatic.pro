<?php

use yii\db\Migration;

class m170705_183113_information_effect extends Migration
{
    public function safeUp(): void
    {
        $this->createTable(
            'information_effect',
            [
                'id' => $this->primaryKey()->unsigned(),
                'information_product_to_information_need_id' => $this->integer()->unsigned()->notNull(),
                'information_effect_type_id' => $this->integer()->unsigned()->notNull(),
                'value' => $this->float()->notNull(),
                'date' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            ]
        );
    }

    public function safeDown(): void
    {
        $this->dropTable('information_effect');
    }
}
