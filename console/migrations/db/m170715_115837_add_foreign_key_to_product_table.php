<?php

use yii\db\Migration;

class m170715_115837_add_foreign_key_to_product_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_product_function_id',
            '{{%product}}', 'function_id',
            '{{%industrial_function}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_function_id', '{{%product}}');
    }
}
