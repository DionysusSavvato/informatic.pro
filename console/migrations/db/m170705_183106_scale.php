<?php

use yii\db\Migration;

class m170705_183106_scale extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%scale}}',
            [
                'scale_id' => $this->primaryKey(11)->unsigned(),
                'type_id' => $this->integer()->unsigned()->notNull(),
                'name' => $this->string(255)->notNull(),
            ], $tableOptions
        );

    }

    public function safeDown(): void
    {
        $this->dropTable('{{%scale}}');
    }
}
