<?php

use yii\db\Migration;

class m170705_183120_message extends Migration
{
    private const TABLE_NAME = 'message';

    public function safeUp(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'situation_id' => $this->integer()->unsigned()->notNull(),
            'industrial_function_id' => $this->integer()->unsigned()->notNull(),
            'place_id' => $this->integer()->unsigned()->null(),
            'date' => $this->dateTime()->notNull(),
        ]);
    }

    public function safeDown(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
