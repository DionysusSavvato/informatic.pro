<?php

use yii\db\Migration;

class m170715_115649_add_foreign_key_to_information_product_to_information_need_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_inf_product_to_inf_need_inf_product_id',
            '{{%information_product_to_information_need}}', 'information_product_id',
            '{{%information_product}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_inf_product_to_inf_need_inf_product_id', '{{%information_product_to_information_need}}');
    }
}
