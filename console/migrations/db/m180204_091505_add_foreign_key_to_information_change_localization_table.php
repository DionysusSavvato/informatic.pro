<?php

use yii\db\Migration;

class m180204_091505_add_foreign_key_to_information_change_localization_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_information_change_localization__entity',
            'information_change_localization', 'entity_id',
            'entity', 'entity_id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_information_change_localization__entity',
            'information_change_localization'
        );
    }
}
