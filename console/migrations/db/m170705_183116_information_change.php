<?php

use yii\db\Migration;

class m170705_183116_information_change extends Migration
{
    public function safeUp(): void
    {
        $this->createTable(
            'information_change',
            [
                'id' => $this->primaryKey()->unsigned(),
                'title' => $this->string()->notNull(),
                'description' => $this->text(),
                'date' => $this->date()->notNull(),
                'flag_model_changes_needed' => $this->boolean()->unsigned()->notNull()->defaultValue(false),
            ]
        );
    }

    public function safeDown(): void
    {
        $this->dropTable('information_change');
    }
}
