<?php

use yii\db\Migration;

class m170715_115907_add_foreign_key_to_product_to_need_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_product_to_need_need_id',
            '{{%product_to_need}}', 'need_id',
            '{{%need}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_to_need_need_id', '{{%product_to_need}}');
    }
}
