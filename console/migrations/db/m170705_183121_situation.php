<?php

use yii\db\Migration;

class m170705_183121_situation extends Migration
{
    private const TABLE_NAME = 'situation';

    public function safeUp(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string()->notNull(),
            'recomendation' => $this->text()->notNull(),
        ]);
    }

    public function safeDown(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
