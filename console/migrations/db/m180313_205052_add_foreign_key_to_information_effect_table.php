<?php

use yii\db\Migration;

class m180313_205052_add_foreign_key_to_information_effect_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_information_effect__information_effect_type',
            'information_effect', 'information_effect_type_id',
            'information_effect_type', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_information_effect__information_effect_type',
            'information_effect'
        );
    }
}
