<?php

use yii\db\Migration;

class m170716_053439_add_foreign_key_to_scale_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_scale_type_id',
            '{{%scale}}', 'type_id',
            '{{%scale_type}}', 'scale_type_id',
            'RESTRICT', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_scale_type_id', '{{%scale}}');
    }
}
