<?php

use yii\db\Migration;

class m171119_214604_add_foreign_key_to_inforamtion_effect_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_information_effect__inf_need_to_inf_product',
            'information_effect', 'information_product_to_information_need_id',
            'information_product_to_information_need', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey(
            'fk_information_effect__inf_need_to_inf_product',
            'information_effect'
        );
    }
}
