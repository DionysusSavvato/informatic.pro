<?php

use common\models\Situation;
use yii\db\Migration;

class m180318_163914_fill_data_for_situation_table extends Migration
{
    private const TABLE_NAME = 'situation';

    public function safeUp(): void
    {
        $this->insert(self::TABLE_NAME, [
            'id' => Situation::INFORMATION_IS_NOT_TIMELINESS,
            'title' => 'Несвоевременное информационное обеспечение',
            'recomendation' => 'Необходимо повысить своевременность информационного обеспечения',
        ]);

        $this->insert(self::TABLE_NAME, [
            'id' => Situation::INSTORMAION_IS_NOT_COMPLETENESS,
            'title' => 'Недостаточная полнота информационного обеспечения',
            'recomendation' => 'Необходимо доработать содержание информационного обеспечения',
        ]);
    }

    public function safeDown(): void
    {
        $this->delete(self::TABLE_NAME, [
            'id' => [
                Situation::INFORMATION_IS_NOT_TIMELINESS,
                Situation::INSTORMAION_IS_NOT_COMPLETENESS,
            ],
        ]);
    }
}
