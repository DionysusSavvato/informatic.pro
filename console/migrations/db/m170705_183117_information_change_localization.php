<?php

use yii\db\Migration;

class m170705_183117_information_change_localization extends Migration
{
    private const TABLE_NAME = 'information_change_localization';
    private const INDEX_NAME = 'u_idx_$information_change_id$entity_id';

    public function safeUp(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'information_change_id' => $this->integer()->unsigned()->notNull(),
            'entity_id' => $this->integer()->unsigned()->notNull(),
            'function_id' => $this->integer()->unsigned()->notNull(),
        ]);
        $this->createIndex(
            self::INDEX_NAME,
            self::TABLE_NAME,
            [
                'information_change_id',
                'entity_id',
            ],
            true
        );
    }

    public function safeDown(): void
    {
        $this->dropIndex(
            self::INDEX_NAME,
            self::TABLE_NAME
        );
        $this->dropTable(self::TABLE_NAME);
    }
}
