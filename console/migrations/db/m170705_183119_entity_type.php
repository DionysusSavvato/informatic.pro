<?php

use common\models\EntityType;
use yii\db\Migration;

class m170705_183119_entity_type extends Migration
{
    private const TABLE_NAME = 'entity_type';

    private const TYPES = [
        EntityType::TYPE_INDUSTRIAL_FUNCTION => 'Производственная функция',
        EntityType::TYPE_MATERIAL => 'Оборотная единица (ресурс или продукт)',
        EntityType::TYPE_CONSUMER => 'Потребитель',
        EntityType::TYPE_SUPPLIER => 'Поставщик',
        EntityType::TYPE_NEED => 'Потребность',
        EntityType::TYPE_MANAGER => 'Ответственное лицо',
        EntityType::TYPE_INFORMATION_NEED => 'Информационная потребность',
        EntityType::TYPE_INFORMATION_PRODUCT => 'Информационный продукт',
        EntityType::TYPE_INFORMATION_FUNCTION => 'Информационная функция',
        EntityType::TYPE_INFORMATION_RESOURCE => 'Информационный ресурс',
    ];

    public function safeUp(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull()
        ]);

        foreach (self::TYPES as $typeID => $typeName) {
            $this->insert(self::TABLE_NAME, [
                'id' => $typeID,
                'name' => $typeName
            ]);
        }
    }

    public function safeDown(): void
    {
        $this->dropTable(
            self::TABLE_NAME
        );
    }
}
