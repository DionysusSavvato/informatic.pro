<?php

use yii\db\Migration;

class m171106_060835_add_foreign_key_to_industrial_function_table extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey('fk_function_manager_id',
            '{{%industrial_function}}', 'manager_id',
            '{{%manager}}', 'entity_id',
            'SET NULL', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_function_manager_id', '{{%industrial_function}}');

    }
}
