<?php

use yii\db\Migration;

class m180318_160851_add_foreign_key_to_message_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_message__industrial_function',
            'message', 'industrial_function_id',
            'industrial_function', 'entity_id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_message__industrial_function',
            'message'
        );
    }
}
