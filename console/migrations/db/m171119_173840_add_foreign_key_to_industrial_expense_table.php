<?php

use yii\db\Migration;

class m171119_173840_add_foreign_key_to_industrial_expense_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_industrial_expense__resource',
            'industrial_expense', 'resource_id',
            'resource', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey('fk_industrial_expense__resource', 'industrial_expense');
    }
}
