<?php

use yii\db\Migration;

class m170705_183053_information_need extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%information_need}}',
            [
                'entity_id' => $this->primaryKey(11)->unsigned(),
                'name' => $this->string(255)->notNull(),
                'abbreviation' => $this->string(20)->notNull(),
                'manager_id' => $this->integer()->unsigned()->notNull(),
            ], $tableOptions
        );

    }

    public function safeDown(): void
    {
        $this->dropTable('{{%information_need}}');
    }
}
