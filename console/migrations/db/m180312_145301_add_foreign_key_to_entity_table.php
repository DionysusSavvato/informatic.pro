<?php

use yii\db\Migration;

class m180312_145301_add_foreign_key_to_entity_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_entity__entity_type',
            'entity', 'entity_type_id',
            'entity_type', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_entity__entity_type',
            'entity'
        );
    }
}
