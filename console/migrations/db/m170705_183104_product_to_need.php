<?php

use yii\db\Migration;

class m170705_183104_product_to_need extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_to_need}}',
            [
                'id' => $this->primaryKey()->unsigned(),
                'product_id' => $this->integer(11)->unsigned()->notNull(),
                'need_id' => $this->integer(11)->unsigned()->notNull(),
                'effect_name' => $this->string()->null(),
            ], $tableOptions
        );
        $this->createIndex(
            'ui_need_id__product_id',
            '{{%product_to_need}}',
            ['need_id', 'product_id'],
            true);

    }

    public function safeDown()
    {
        $this->dropIndex('ui_need_id__product_id', '{{%product_to_need}}');
        $this->dropTable('{{%product_to_need}}');
    }
}
