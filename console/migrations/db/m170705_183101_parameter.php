<?php

use yii\db\Migration;

class m170705_183101_parameter extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%parameter}}',
            [
                'parameter_id' => $this->primaryKey(11)->unsigned(),
                'name' => $this->string(255)->notNull(),
                'scale_id' => $this->integer(11)->unsigned()->notNull(),
                'entity_id' => $this->integer(11)->unsigned()->notNull(),
            ], $tableOptions
        );
        $this->createIndex('FK_parameter_scale_scale_id', '{{%parameter}}', ['scale_id'], false);
        $this->createIndex('FK_parameter_entity_entity_id', '{{%parameter}}', ['entity_id'], false);

    }

    public function safeDown()
    {
        $this->dropIndex('FK_parameter_scale_scale_id', '{{%parameter}}');
        $this->dropIndex('FK_parameter_entity_entity_id', '{{%parameter}}');
        $this->dropTable('{{%parameter}}');
    }
}
