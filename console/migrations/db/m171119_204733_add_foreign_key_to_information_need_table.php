<?php

use yii\db\Migration;

class m171119_204733_add_foreign_key_to_information_need_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_information_need__manager',
            'information_need', 'manager_id',
            'manager', 'entity_id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey('fk_information_need__manager', 'information_need');
    }
}
