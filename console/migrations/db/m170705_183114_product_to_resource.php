<?php

use yii\db\Migration;

class m170705_183114_product_to_resource extends Migration
{
    public function safeUp(): void
    {
        $this->createTable(
            'product_to_resource',
            [
                'id' => $this->primaryKey()->unsigned(),
                'product_id' => $this->integer()->unsigned()->notNull(),
                'resource_id' => $this->integer()->unsigned()->notNull(),
                'effect_name' => $this->string()->null(),
            ]
        );
        $this->createIndex(
            'u_idx_$product_id$resource_id',
            'product_to_resource',
            [
                'product_id',
                'resource_id',
            ],
            true);
    }

    public function safeDown(): void
    {
        $this->dropIndex('u_idx_$product_id$resource_id', 'product_to_resource');
        $this->dropTable('product_to_resource');
    }
}
