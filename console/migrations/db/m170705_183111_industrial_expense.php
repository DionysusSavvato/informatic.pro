<?php

use yii\db\Migration;

/**
 * Handles the creation of table `industrial_expense`.
 */
class m170705_183111_industrial_expense extends Migration
{
    /**
     * @inheritdoc
     */
    public function up(): void
    {
        $this->createTable('industrial_expense', [
            'id' => $this->primaryKey()->unsigned(),
            'resource_id' => $this->integer()->unsigned()->notNull(),
            'price' => $this->float(2)->notNull(),
            'count' => $this->float()->unsigned()->notNull(),
            'date' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down(): void
    {
        $this->dropTable('industrial_expense');
    }
}
