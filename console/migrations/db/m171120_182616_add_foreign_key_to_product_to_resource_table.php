<?php

use yii\db\Migration;

class m171120_182616_add_foreign_key_to_product_to_resource_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_product_to_resource__product',
            'product_to_resource', 'product_id',
            'product', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown(): void
    {
        $this->dropForeignKey('fk_product_to_resource__product', 'product_to_resource');
    }
}
