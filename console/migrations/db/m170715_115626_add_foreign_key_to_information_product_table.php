<?php

use yii\db\Migration;

class m170715_115626_add_foreign_key_to_information_product_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_information_product_entity_id',
            '{{%information_product}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_information_product_entity_id', '{{%information_product}}');
    }
}
