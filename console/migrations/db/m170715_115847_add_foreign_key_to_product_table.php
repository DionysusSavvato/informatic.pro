<?php

use yii\db\Migration;

class m170715_115847_add_foreign_key_to_product_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_product_material_id',
            '{{%product}}', 'material_id',
            '{{%material}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_material_id', '{{%product}}');
    }
}
