<?php

use yii\db\Migration;

class m170715_120009_add_foreign_key_to_supplier_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_supplier_to_material_material_id',
            '{{%supplier_to_material}}', 'material_id',
            '{{%material}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_supplier_to_material_material_id', '{{%supplier_to_material}}');
    }
}
