<?php

use yii\db\Migration;

/**
 * Class m170715_115604_add_foreign_key_to_information_need_table
 */
class m170715_115604_add_foreign_key_to_information_need_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_information_need_entity_id',
            '{{%information_need}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_information_need_entity_id', '{{%information_need}}');
    }


}
