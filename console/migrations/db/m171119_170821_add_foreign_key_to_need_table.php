<?php

use yii\db\Migration;

class m171119_170821_add_foreign_key_to_need_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_need__consumer',
            '{{%need}}', 'consumer_id',
            '{{%consumer}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_need__consumer', '{{%need}}');
    }
}
