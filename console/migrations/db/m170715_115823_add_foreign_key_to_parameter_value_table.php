<?php

use yii\db\Migration;

class m170715_115823_add_foreign_key_to_parameter_value_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_parameter_value_param_id',
            '{{%parameter_value}}', 'param_id',
            '{{%parameter}}', 'parameter_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_parameter_value_param_id', '{{%parameter_value}}');
    }
}
