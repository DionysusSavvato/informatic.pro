<?php

use yii\db\Migration;

class m170715_115707_add_foreign_key_to_information_resource_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_information_resource_function_id',
            '{{%information_resource}}', 'function_id',
            '{{%information_function}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_information_resource_function_id', '{{%information_resource}}');
    }
}
