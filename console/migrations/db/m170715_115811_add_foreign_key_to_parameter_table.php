<?php

use yii\db\Migration;

class m170715_115811_add_foreign_key_to_parameter_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_parameter_scale_id',
            '{{%parameter}}', 'scale_id',
            '{{%scale}}', 'scale_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_parameter_scale_id', '{{%parameter}}');
    }
}
