<?php

use yii\db\Migration;

class m170715_115919_add_foreign_key_to_resource_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_resource_function_id',
            '{{%resource}}', 'function_id',
            '{{%industrial_function}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_resource_function_id', '{{%resource}}');
    }
}
