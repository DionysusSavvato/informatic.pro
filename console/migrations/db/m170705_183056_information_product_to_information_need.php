<?php

use yii\db\Migration;

class m170705_183056_information_product_to_information_need extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%information_product_to_information_need}}',
            [
                'id' => $this->primaryKey()->unsigned(),
                'information_product_id' => $this->integer(11)->unsigned()->notNull(),
                'information_need_id' => $this->integer(11)->unsigned()->notNull(),
                'effect_threshold_value' => $this->float()->notNull(),
            ], $tableOptions
        );
        $this->createIndex(
            'ui_information_product_id__information_need_id',
            '{{%information_product_to_information_need}}',
            ['information_need_id', 'information_product_id'],
            true
        );

    }

    public function safeDown(): void
    {
        $this->dropIndex('ui_information_product_id__information_need_id',
            '{{%information_product_to_information_need}}');
        $this->dropTable('{{%information_product_to_information_need}}');
    }
}
