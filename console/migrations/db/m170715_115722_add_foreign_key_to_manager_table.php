<?php

use yii\db\Migration;

class m170715_115722_add_foreign_key_to_manager_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_manager_entity_id',
            '{{%manager}}', 'entity_id',
            '{{%entity}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_manager_entity_id', '{{%manager}}');
    }
}
