<?php

use yii\db\Migration;

class m170715_120028_add_foreign_key_to_supplier_to_material_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_supplier_to_material_supplier_id',
            '{{%supplier_to_material}}', 'supplier_id',
            '{{%supplier}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_supplier_to_material_supplier_id', '{{%supplier_to_material}}');
    }
}
