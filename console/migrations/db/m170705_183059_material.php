<?php

use yii\db\Migration;

class m170705_183059_material extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp(): void
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%material}}',
            [
                'entity_id' => $this->primaryKey(11)->unsigned(),
                'name' => $this->string(255)->notNull(),
            ], $tableOptions
        );

    }

    public function safeDown(): void
    {
        $this->dropTable('{{%material}}');
    }
}
