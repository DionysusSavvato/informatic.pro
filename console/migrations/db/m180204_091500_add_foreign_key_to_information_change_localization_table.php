<?php

use yii\db\Migration;

class m180204_091500_add_foreign_key_to_information_change_localization_table extends Migration
{
    public function safeUp(): void
    {
        $this->addForeignKey(
            'fk_information_change_localization__information_change',
            'information_change_localization', 'information_change_id',
            'information_change', 'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_information_change_localization__information_change',
            'information_change_localization'
        );
    }
}
