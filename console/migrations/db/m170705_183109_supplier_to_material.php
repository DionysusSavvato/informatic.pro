<?php

use yii\db\Migration;

class m170705_183109_supplier_to_material extends Migration
{
    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%supplier_to_material}}',
            [
                'supplier_id' => $this->integer(11)->unsigned()->notNull(),
                'material_id' => $this->integer(11)->unsigned()->notNull(),
            ], $tableOptions
        );
        $this->createIndex('FK_supplier_to_material_supplier_entity_id', '{{%supplier_to_material}}', ['supplier_id'], false);
        $this->addPrimaryKey('pk_on_supplier_to_material', '{{%supplier_to_material}}', ['supplier_id', 'material_id']);

    }

    public function safeDown()
    {
        $this->dropPrimaryKey('pk_on_supplier_to_material', '{{%supplier_to_material}}');
        $this->dropIndex('FK_supplier_to_material_supplier_entity_id', '{{%supplier_to_material}}');
        $this->dropTable('{{%supplier_to_material}}');
    }
}
