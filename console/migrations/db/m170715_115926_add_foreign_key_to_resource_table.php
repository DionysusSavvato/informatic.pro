<?php

use yii\db\Migration;

class m170715_115926_add_foreign_key_to_resource_table extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_resource_material_id',
            '{{%resource}}', 'material_id',
            '{{%material}}', 'entity_id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_resource_material_id', '{{%resource}}');
    }
}
