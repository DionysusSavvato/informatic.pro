<?php

use yii\console\controllers\MigrateController;
use yii\gii\Module;
use yii\log\FileTarget;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

return [
    'id' => 'app-console',
    'language' => 'en-EN',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => MigrateController::class,
            'migrationPath' => '@console/migrations/db',
        ],
        'migrate-demo' => [
            'class' => MigrateController::class,
            'migrationPath' => '@console/migrations/demo',
        ],
    ],
    'modules' => [
        'gii' => Module::class,
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
